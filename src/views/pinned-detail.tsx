import { BreadcrumbItem, BreadcrumbLink, Flex } from '@chakra-ui/core';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { Breadcrumbs } from '../components/layout/breadcrumbs';
import { Header } from '../components/layout/header';
import { WithSidebar } from '../components/layout/with-sidebar';
import { PinnedItemDisplay } from '../components/pinned/pinned-item-display';
import { getCurrentUserDetails } from '../store/actions/auth';
import { getSelectedPinnedItem } from '../store/selectors';

export const PinnedDetailView = () => {
  const dispatch = useDispatch();
  const item = useSelector(getSelectedPinnedItem);

  useEffect(() => {
    if (item) {
      document.title = `${item.title}`;
    }
  }, [item]);

  useEffect(() => {
    dispatch(getCurrentUserDetails());
  }, [dispatch]);

  return (
    <Flex flex={1} flexDirection="column" h="100vh">
      <Header />
      <WithSidebar>
        <Breadcrumbs>
          <BreadcrumbItem>
            <BreadcrumbLink as={Link as any} {...{ to: '/pinned' }}>
              Pinned
            </BreadcrumbLink>
          </BreadcrumbItem>

          <BreadcrumbItem>
            <BreadcrumbLink>Detail</BreadcrumbLink>
          </BreadcrumbItem>
        </Breadcrumbs>

        <PinnedItemDisplay />
      </WithSidebar>
    </Flex>
  );
};
