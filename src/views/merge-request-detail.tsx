import { BreadcrumbItem, BreadcrumbLink, Flex } from '@chakra-ui/core';
import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { Breadcrumbs } from '../components/layout/breadcrumbs';
import { Header } from '../components/layout/header';
import { WithSidebar } from '../components/layout/with-sidebar';
import { MergeRequestDisplay } from '../components/merge-requests/merge-request-detail';
import { getSelectedMergeRequest } from '../store/selectors';

export const MergeRequestDetailView = () => {
  const mergeRequest = useSelector(getSelectedMergeRequest);

  useEffect(() => {
    if (mergeRequest) {
      document.title = `${mergeRequest.title}`;
    }
  }, [mergeRequest]);

  return (
    <Flex flex={1} flexDirection="column" h="100vh">
      <Header />
      <WithSidebar>
        <Breadcrumbs>
          <BreadcrumbItem>
            <BreadcrumbLink as={Link as any} {...{ to: '/merge-requests' }}>
              Merge Requests
            </BreadcrumbLink>
          </BreadcrumbItem>

          <BreadcrumbItem>
            <BreadcrumbLink>Detail</BreadcrumbLink>
          </BreadcrumbItem>
        </Breadcrumbs>

        <MergeRequestDisplay />
      </WithSidebar>
    </Flex>
  );
};
