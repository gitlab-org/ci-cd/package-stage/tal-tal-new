import { BreadcrumbItem, BreadcrumbLink, Button, Flex } from '@chakra-ui/core';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { EmptyState } from '../components/empty-state';
import { IssuesList } from '../components/issues/issues-list';
import { Breadcrumbs } from '../components/layout/breadcrumbs';
import { Header } from '../components/layout/header';
import { WithSidebar } from '../components/layout/with-sidebar';
import { getCurrentUserDetails } from '../store/actions/auth';
import { importIssues } from '../store/actions/issues';
import {
  getImportLoading,
  getIssuesListWithMilestones,
  getSelectedProject,
} from '../store/selectors';

export const MainView = () => {
  const dispatch = useDispatch();
  const issues = useSelector(getIssuesListWithMilestones);
  const isLoading = useSelector(getImportLoading);
  const selectedProject = useSelector(getSelectedProject);

  useEffect(() => {
    document.title = `${selectedProject.name} Issues`;
  }, [selectedProject.name]);

  useEffect(() => {
    dispatch(getCurrentUserDetails());
  }, [dispatch]);

  const onImportIssuesClick = () => {
    dispatch(importIssues());
  };

  return (
    <Flex flex={1} flexDirection="column" h="100vh" className="page">
      <Header />
      <WithSidebar>
        <Breadcrumbs>
          <BreadcrumbItem>
            <BreadcrumbLink href="/">Issues</BreadcrumbLink>
          </BreadcrumbItem>
        </Breadcrumbs>

        {issues === null && (
          <EmptyState
            title="No Issues Found"
            text="No issues exist yet. Try importing some using the button below."
            backgroundColor="background.light"
          >
            <Button mt={6} variantColor="green" isLoading={isLoading} onClick={onImportIssuesClick}>
              Import Issues
            </Button>
          </EmptyState>
        )}

        {issues && <IssuesList milestonesMenu={issues} />}
      </WithSidebar>
    </Flex>
  );
};
