import { BreadcrumbItem, BreadcrumbLink, Flex } from '@chakra-ui/core';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { Breadcrumbs } from '../components/layout/breadcrumbs';
import { Header } from '../components/layout/header';
import { WithSidebar } from '../components/layout/with-sidebar';
import { MilestoneDisplay } from '../components/milestones/milestone-display';
import { getStatsForSelectedMilestone } from '../store/actions/milestones';
import { getSelectedMilestone } from '../store/selectors';

export const MilestoneDetailView = () => {
  const dispatch = useDispatch();
  const selectedMilestone = useSelector(getSelectedMilestone);

  useEffect(() => {
    if (selectedMilestone) {
      document.title = `Milestone ${selectedMilestone.id}`;
    }
  }, [selectedMilestone]);

  useEffect(() => {
    if (selectedMilestone) {
      dispatch(getStatsForSelectedMilestone(selectedMilestone.id));
    }
  }, [dispatch, selectedMilestone]);

  if (!selectedMilestone) {
    return null;
  }

  return (
    <Flex flex={1} flexDirection="column" h="100vh">
      <Header />
      <WithSidebar>
        <Breadcrumbs>
          <BreadcrumbItem>
            <BreadcrumbLink as={Link as any} {...{ to: '/milestones' }}>
              Milestones
            </BreadcrumbLink>
          </BreadcrumbItem>

          <BreadcrumbItem>
            <BreadcrumbLink>Detail</BreadcrumbLink>
          </BreadcrumbItem>
        </Breadcrumbs>

        <MilestoneDisplay milestone={selectedMilestone} />
      </WithSidebar>
    </Flex>
  );
};
