import { BreadcrumbItem, BreadcrumbLink, Button, Flex } from '@chakra-ui/core';
import React, { useContext, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { AppContextComponent } from '../app-context';
import { EmptyState } from '../components/empty-state';
import { Breadcrumbs } from '../components/layout/breadcrumbs';
import { Header } from '../components/layout/header';
import { WithSidebar } from '../components/layout/with-sidebar';
import { PinnedItemsList } from '../components/pinned/pinned-items-list';
import { Issue } from '../models';
import { getPinnedIssues } from '../store/selectors';

export const PinnedView = () => {
  const { dispatch: contextDispatch } = useContext(AppContextComponent);

  const issues: Issue[] = useSelector(getPinnedIssues);

  useEffect(() => {
    document.title = `Pinned Issues`;
  }, []);

  const onNewPinClick = () => {
    contextDispatch({
      type: 'toggle-new-pinned-modal',
      payload: { isOpen: true },
    });
  };

  return (
    <Flex flex={1} flexDirection="column" h="100vh" className="page">
      <Header />
      <WithSidebar>
        <Breadcrumbs>
          <BreadcrumbItem>
            <BreadcrumbLink href="/pinned">Pinned</BreadcrumbLink>
          </BreadcrumbItem>
        </Breadcrumbs>
        {issues.length === 0 && (
          <EmptyState
            title="No Pinned Items Yet"
            text="You can pin items to keep track of and view later. Start adding yours by clicking the new pin button below."
            backgroundColor="background.light"
            illustration="pinned"
          >
            <Button mt={6} variantColor="green" onClick={onNewPinClick}>
              New pin
            </Button>
          </EmptyState>
        )}

        {issues.length > 0 && <PinnedItemsList />}
      </WithSidebar>
    </Flex>
  );
};
