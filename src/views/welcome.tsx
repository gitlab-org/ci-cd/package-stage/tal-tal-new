import { Button, Flex, Text } from '@chakra-ui/core';
import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { ReactComponent as WelcomeIllustration } from '../assets/svgs/welcome.svg';
import { startLogin } from '../store/actions/auth';

export const WelcomeView: React.FC = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    document.title = 'Welcome to Tal Tal';
  }, []);

  const loginClick = () => {
    dispatch(startLogin());
  };

  return (
    <Flex
      w="100vw"
      h="100vh"
      backgroundColor="background.light"
      direction="column"
      justifyContent="center"
      alignItems="center"
    >
      <Flex justifyContent="center">
        <WelcomeIllustration height="30vh" width="25vw" />
      </Flex>

      <Flex direction="column" alignItems="center">
        <Text fontSize="3xl" fontWeight="bold" textAlign="center" mb={4}>
          Tal Tal
        </Text>

        <Text textAlign="center" mb={6}>
          An async issue management tool.
        </Text>

        <Button onClick={loginClick} variantColor="green" size="sm" maxW="200px">
          Login with GitLab
        </Button>
      </Flex>
    </Flex>
  );
};
