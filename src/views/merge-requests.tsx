import { BreadcrumbItem, BreadcrumbLink, Button, Flex } from '@chakra-ui/core';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { EmptyState } from '../components/empty-state';
import { Breadcrumbs } from '../components/layout/breadcrumbs';
import { Header } from '../components/layout/header';
import { WithSidebar } from '../components/layout/with-sidebar';
import { MergeRequestsList } from '../components/merge-requests/merge-requests-list';
import { fetchMergeRequestsForProject } from '../store/actions/merge-requests';
import { getMergeRequestLoading, getMergeRequests, getSelectedProject } from '../store/selectors';

export const MergeRequestsView = () => {
  const dispatch = useDispatch();
  const selectedProject = useSelector(getSelectedProject);
  const isLoading = useSelector(getMergeRequestLoading);

  const mergeRequests = useSelector(getMergeRequests);
  const showMergeRequests = Boolean(Object.keys(mergeRequests).length);

  useEffect(() => {
    document.title = `${selectedProject.name} Merge Requests`;
  }, [selectedProject.name]);

  const fetchMergeRequests = () => {
    dispatch(fetchMergeRequestsForProject(selectedProject.id));
  };

  const text = isLoading
    ? 'Fetching your merge requests right now.'
    : 'Click the button to fetch your merge requests for this project.';

  return (
    <Flex flex={1} flexDirection="column" h="100vh" className="page">
      <Header />
      <WithSidebar>
        <Breadcrumbs>
          <BreadcrumbItem>
            <BreadcrumbLink href="/merge-requests">Merge Requests</BreadcrumbLink>
          </BreadcrumbItem>
        </Breadcrumbs>
        {!showMergeRequests && (
          <EmptyState
            title="No Merge Requests Yet"
            text={text}
            backgroundColor="background.light"
            illustration="merge-request"
          >
            <Button mt={6} variantColor="green" onClick={fetchMergeRequests} isLoading={isLoading}>
              Fetch now
            </Button>
          </EmptyState>
        )}

        {showMergeRequests && <MergeRequestsList mergeRequestsStore={mergeRequests} />}
      </WithSidebar>
    </Flex>
  );
};
