import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { authActions, oauth } from '../store/actions/auth';

export const CallbackView: React.FC = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  React.useEffect(() => {
    const processLogin = async () => {
      const clientToken = await oauth.code.getToken(window.location.href)
      const accessToken = clientToken.accessToken


      if (accessToken) {
        dispatch(authActions.setAccessToken(accessToken));
        history.push('/');
      } else {
        history.push('welcome');
      }
    }
    processLogin()
  }, [dispatch, history])
  return null
};
