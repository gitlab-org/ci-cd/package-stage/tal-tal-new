import {
  BreadcrumbItem,
  BreadcrumbLink,
  Divider,
  Flex,
  Link,
  List,
  ListItem,
  Stack,
  Text,
} from '@chakra-ui/core';
import React, { useEffect } from 'react';
import { Link as RouteLink } from 'react-router-dom';
import { ReactComponent as About } from '../assets/svgs/about.svg';
import { Breadcrumbs } from '../components/layout/breadcrumbs';
import { Header } from '../components/layout/header';
import { WithSidebar } from '../components/layout/with-sidebar';

export const AboutView = () => {
  useEffect(() => {
    document.title = 'About Tal Tal';
  }, []);

  return (
    <Flex flex={1} flexDirection="column" h="100vh" className="page">
      <Header />
      <WithSidebar>
        <Breadcrumbs>
          <BreadcrumbItem>
            <BreadcrumbLink href="about">About</BreadcrumbLink>
          </BreadcrumbItem>
        </Breadcrumbs>

        <Flex justifyContent="center" flex={1} backgroundColor="background.light" p={4}>
          <Flex alignSelf="center" flexDirection="column" w="60vw" pb={10}>
            <Flex justifyContent="center" flex={1}>
              <About height="25vh" width="20vw" />
            </Flex>

            <Text fontSize="2xl" fontWeight="bold">
              About Tal Tal
            </Text>

            <Text color="gray.700" fontSize="sm">
              Version: {process.env.REACT_APP_VERSION}
            </Text>

            <Text mt={4}>
              Tal Tal was a small tool I had built to manage my package issues using the async
              workflow{' '}
              <Link
                href="https://about.gitlab.com/handbook/engineering/development/ci-cd/package/#async-daily-standups"
                isExternal
              >
                as described here
              </Link>
              . After a little while of using Tal Tal in its original form, I decided to attempt to
              create a "re-imaging" of what the tool could be with some more usability and layout
              enhancements. This is the re-imagined version of Tal Tal, which boasts a number of new
              features, including:
            </Text>

            <List styleType="disc" mt={4} mx={4}>
              <ListItem>Importing of issues instead of manually adding them</ListItem>
              <ListItem>
                Improved workflow adjustments so that you can update MRs as well as issues
              </ListItem>
              <ListItem>Verification / Production workflow hints</ListItem>
              <ListItem>
                Improved issue display and context, including merge request counts and lists
              </ListItem>
              <ListItem>A basic search</ListItem>
              <ListItem>A basic sidebar explorer</ListItem>
            </List>

            <Divider my={4} borderColor="gray.400" />

            <Text fontWeight="bold">How does it work?</Text>

            <Text mt={4}>
              Pretty much the same as before. This is a SPA that uses the GitLab API to access any
              of your data and uses OAuth for the authentication. There is no server or server side
              proxy so no data is stored or sent anywhere. All state (such as issues, notes, etc) is
              stored locally in an index db. To remove all data, simply log out.
            </Text>

            <Divider my={4} borderColor="gray.400" />

            <Text fontWeight="bold">How do I use this?</Text>

            <Text mt={2}>
              <Link as={RouteLink as any} {...{ to: 'help' }}>
                See the help section.
              </Link>
            </Text>

            <Divider my={4} borderColor="gray.400" />

            <Text fontWeight="bold">Useful Links</Text>

            <Stack spacing={2} mt={2}>
              <Text>
                <strong>Repository:</strong>{' '}
                <Link href="https://gitlab.com/nkipling/tal-tal-new" isExternal>
                  https://gitlab.com/nkipling/tal-tal-new
                </Link>
              </Text>

              <Text>
                <strong>Issues:</strong>{' '}
                <Link href="https://gitlab.com/nkipling/tal-tal-new/-/issues/new" isExternal>
                  Report a bug / issue
                </Link>
              </Text>
            </Stack>
          </Flex>
        </Flex>
      </WithSidebar>
    </Flex>
  );
};
