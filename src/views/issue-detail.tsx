import { BreadcrumbItem, BreadcrumbLink, Flex } from '@chakra-ui/core';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { IssueDisplay } from '../components/issues/issue-display';
import { Breadcrumbs } from '../components/layout/breadcrumbs';
import { Header } from '../components/layout/header';
import { WithSidebar } from '../components/layout/with-sidebar';
import { getCurrentUserDetails } from '../store/actions/auth';
import { getSelectedIssue } from '../store/selectors';

export const IssueDetailView = () => {
  const dispatch = useDispatch();
  const issue = useSelector(getSelectedIssue);

  useEffect(() => {
    if (issue) {
      document.title = `${issue.title}`;
    }
  }, [issue]);

  useEffect(() => {
    dispatch(getCurrentUserDetails());
  }, [dispatch]);

  return (
    <Flex flex={1} flexDirection="column" h="100vh">
      <Header />
      <WithSidebar>
        <Breadcrumbs>
          <BreadcrumbItem>
            <BreadcrumbLink as={Link as any} {...{ to: '/' }}>
              Issues
            </BreadcrumbLink>
          </BreadcrumbItem>

          <BreadcrumbItem>
            <BreadcrumbLink>Detail</BreadcrumbLink>
          </BreadcrumbItem>
        </Breadcrumbs>

        <IssueDisplay />
      </WithSidebar>
    </Flex>
  );
};
