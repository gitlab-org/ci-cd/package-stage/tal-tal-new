import { BreadcrumbItem, BreadcrumbLink, Divider, Flex, Text } from '@chakra-ui/core';
import React, { useEffect } from 'react';
import { ReactComponent as Help } from '../assets/svgs/help.svg';
import { Breadcrumbs } from '../components/layout/breadcrumbs';
import { Header } from '../components/layout/header';
import { WithSidebar } from '../components/layout/with-sidebar';

export const HelpView = () => {
  useEffect(() => {
    document.title = 'Tal Tal Help';
  }, []);

  return (
    <Flex flex={1} flexDirection="column" h="100vh" className="page">
      <Header />
      <WithSidebar>
        <Breadcrumbs>
          <BreadcrumbItem>
            <BreadcrumbLink href="help">Help</BreadcrumbLink>
          </BreadcrumbItem>
        </Breadcrumbs>

        <Flex justifyContent="center" flex={1} backgroundColor="background.light" p={4}>
          <Flex alignSelf="center" flexDirection="column" w="60vw" pb={10}>
            <Flex justifyContent="center" flex={1}>
              <Help height="25vh" width="20vw" />
            </Flex>

            <Text fontSize="2xl" fontWeight="bold">
              Tal Tal Help
            </Text>

            <Text mt={4}>
              If you're used to the original Tal Tal, then there a few little changes that are worth
              being aware of.
            </Text>

            <Divider my={4} borderColor="gray.400" />

            <Text fontWeight="bold">How do I start?</Text>
            <Text mt={2}>
              This version of Tal Tal will import your issues automatically, there is no need to
              manually add them. To do this, click the 'Import Issues' button on the empty list
              page.
            </Text>
            <Text mt={2}>
              The issue import is a request heavy process, so it might take some time to gather all
              the details for each of your issues. It will only fetch issues for the projects you
              register for (GitLab being the default), so if you work outside of GitLab, you will
              need to add your additional projects.
            </Text>
            <Text mt={2}>
              Once your issues have been imported, they can be refreshed at any time by hovering
              over the issues button in the explorer at the top left and clicking the refresh
              button.
            </Text>

            <Divider my={4} borderColor="gray.400" />

            <Text fontWeight="bold">How do I add other projects?</Text>
            <Text mt={2}>
              Click the '+' icon at the top left next to the project selector. This will allow you
              to enter the project ID you wish to add. Once added, an automatic import will trigger,
              updating your issues.
            </Text>
            <Text mt={2}>
              When you have more than one project, you can use the project selector dropdown at the
              top left to switch between projects.
            </Text>

            <Divider my={4} borderColor="gray.400" />

            <Text fontWeight="bold">How do I add an update?</Text>
            <Text mt={2}>
              From either the issues list or the issue detail page, click the 'Add Update' button.
              This should open a new page with the editor for adding your update.
            </Text>
            <Text mt={2}>
              This editor will be prefilled with the async update format. If the issue has some
              previous async update entries, then the last update will be used as the pre-fill so
              you can modify.
            </Text>
            <Text mt={2}>
              Finally, on this new page, you can view all previous updates applied to the issue.
            </Text>
            <Text mt={2}>
              <strong>Note:</strong> in order for your update to be registered as an async update,
              it will need to include one of the following strings: <code>async issue update</code>,{' '}
              <code>async update</code> or <code>issue update</code>.
            </Text>

            <Divider my={4} borderColor="gray.400" />

            <Text fontWeight="bold">What does skipping an update do?</Text>
            <Text mt={2}>
              It just applies a flag to the issue locally, indicating that it is skipped for today.
              It does not affect the issue on GitLab in any way.
            </Text>

            <Divider my={4} borderColor="gray.400" />

            <Text fontWeight="bold">How do I update the workflow of an issue?</Text>
            <Text mt={2}>
              Some issues will have the workflow button displayed. It will be blue if the issue has
              a standard workflow or red if there is none applied. Clicking this will open a modal
              allowing you to change the workflow of the issue to any other workflow.
            </Text>
            <Text mt={2}>
              New in this version of Tal Tal is the ability to select any related merge requests for
              the issue and apply the same workflow update to the selected merge requests. You can
              view the current workflow status of each merge request on the right hand side.
            </Text>

            <Divider my={4} borderColor="gray.400" />

            <Text fontWeight="bold">What can I do with merge requests?</Text>
            <Text mt={2}>
              Not much. You can view a list of merge requests for each of your issues, which will be
              organised by workflow. This will include all merge requests related to the issue and
              not ones just authored by yourself. From the merge request list, you can directly
              update the workflow of the MR by clicking the workflow button.
            </Text>

            <Divider my={4} borderColor="gray.400" />

            <Text fontWeight="bold">My issues are out of date, what do I do?</Text>
            <Text mt={2}>
              You can refresh your issues by clicking the refresh icon found next to the issues
              button in the explorer. You will need to hover over this for the button to appear.
            </Text>

            <Text mt={2}>
              If there has been some time before an import, Tal Tal will prompt you with a
              recommended import button at the top left.
            </Text>

            <Divider my={4} borderColor="gray.400" />

            <Text fontWeight="bold">What about these production / verification buttons?</Text>
            <Text mt={2}>
              Tal Tal will also attempt to guess if your issue is ready to be moved to either the
              verification or production workflows. This isn't 100% accurate as it cannot predict
              your plans to create new merge requests, but it will look at the status of all related
              merge requests and suggest that an issue be updated if the criteria is met.
            </Text>

            <Text mt={2}>
              The <code>Verification?</code> or <code>Production?</code> button will appear if the
              criteria is met. Clicking this will open a popup menu asking if you want to set the
              applicable workflow. Doing so via this method will only update the workflow of the
              issue itself, not any related merge requests.
            </Text>

            <Divider my={4} borderColor="gray.400" />

            <Text fontWeight="bold">What's this new fancy search box all about?</Text>
            <Text mt={2}>
              You can search for issues by typing in the search box. This will do a search against
              the title or issue id. You can quickly focus the search box by pressing the{' '}
              <code>s</code> or <code>p</code> keys (and unfocused with <code>escape</code>).
            </Text>

            <Text mt={2}>
              The search input / results list needs a little love, so don't be too harsh on it.
            </Text>

            <Divider my={4} borderColor="gray.400" />

            <Text fontWeight="bold">What is "pinned" all about?</Text>
            <Text mt={2}>
              You can pin issues or epics (hopefully more in the future) to be saved for whatever
              you want to do with them later. It's usually used to pin issues that you're not
              assigned to but want to remember and keep track of.
            </Text>

            <Text mt={2}>
              To add a new pinned item, hover over the pinned menu item on the left and click the +
              icon. This will open a new modal where you can specify the id of the issue or epic you
              wish to pin.
            </Text>

            <Text mt={2}>
              To correctly pin the item, you must first specify a project using the dropdown (or add
              a new one via the input on the right) and then a category too. You can create new
              categories by using the input on the right of the selector. Note: you must press the
              'add' button for this to take effect.
            </Text>

            <Text mt={2}>
              Not much can be done with pinned items right now, they're just acting as a personal
              list. You can remove a pinned item by clicking into the detail and then clicking the
              trash can icon at the top right.
            </Text>

            <Divider my={4} borderColor="gray.400" />

            <Text fontWeight="bold">Why can't I do x?</Text>
            <Text mt={2}>
              Probably because I haven't thought of it yet. Feel free to float your ideas and if I
              get time, I'll try to add it!
            </Text>
          </Flex>
        </Flex>
      </WithSidebar>
    </Flex>
  );
};
