import { BreadcrumbItem, BreadcrumbLink, Button, Flex } from '@chakra-ui/core';
import React, { useContext, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { AppContextComponent } from '../app-context';
import { EmptyState } from '../components/empty-state';
import { Breadcrumbs } from '../components/layout/breadcrumbs';
import { Header } from '../components/layout/header';
import { WithSidebar } from '../components/layout/with-sidebar';
import { MilestoneList } from '../components/milestones/milestone-list';
import { getSelectedProject, getTrackedMilestones } from '../store/selectors';

export const MilestonesView = () => {
  const { dispatch: contextDispatch } = useContext(AppContextComponent);

  const selectedProject = useSelector(getSelectedProject);
  const milestones = useSelector(getTrackedMilestones);

  useEffect(() => {
    document.title = `${selectedProject.name} Milestones`;
  }, [selectedProject.name]);

  const onAddMilestoneClick = () => {
    contextDispatch({
      type: 'toggle-milestone-modal',
      payload: { isOpen: true },
    });
  };

  return (
    <Flex flex={1} flexDirection="column" h="100vh" className="page">
      <Header />
      <WithSidebar>
        <Breadcrumbs>
          <BreadcrumbItem>
            <BreadcrumbLink href="/milestones">Milestones</BreadcrumbLink>
          </BreadcrumbItem>
        </Breadcrumbs>
        {milestones.length === 0 && (
          <EmptyState
            title="No Milestones Yet"
            text="You can add milestones to track here. This allows you to track all merge requests made in any particular milestone. You can also add and record notes, useful for async retros."
            backgroundColor="background.light"
            illustration="milestones"
          >
            <Button mt={6} variantColor="green" onClick={onAddMilestoneClick}>
              Add milestone
            </Button>
          </EmptyState>
        )}

        {milestones.length > 0 && <MilestoneList />}
      </WithSidebar>
    </Flex>
  );
};
