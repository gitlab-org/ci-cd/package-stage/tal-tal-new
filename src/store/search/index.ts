import * as CryptoJs from 'crypto-js';
import { Builder, Index } from 'lunr';
import { Issue, SearchDoc } from '../../models';

let searchIndex: Index;
let knownHash: string;

export const initBuilder = () => {
  const builder = new Builder();

  builder.ref('entityId');

  // These are the fields that each entity is indexed against.
  // A higher boost indicates that a match in this field is more important.
  builder.field('id', { boost: 3 });
  builder.field('title', { boost: 2 });
  builder.field('content', { boost: 1 });

  return builder;
};

function getCollectionHash(collection: string[]): string {
  return CryptoJs.MD5(collection.toString()).toString();
}

export const buildSearchIndex = (issues: Issue[]) => {
  return () => {
    const hash = getCollectionHash([...issues.map(x => x.id.toString())]);

    if (hash !== knownHash) {
      const builder = initBuilder();
      knownHash = hash;

      // We convert issues to a SearchDoc for indexing. Here we could choose to
      // add more fields to support more complex searching. E.g. we could allow
      // searching through merge requests to find issues too.
      const issueDocs: SearchDoc[] = issues.map(x => ({
        entityId: x.id,
        id: x.iid,
        title: x.title,
        content: x.description,
      }));

      // We could add more entities to search through here.
      // For example, merge requests might be good.

      issueDocs.forEach(x => builder.add(x));

      searchIndex = builder.build();

      // TODO: remove this after debugging
      // (window as any).searchIndex = searchIndex;
    }
  };
};

/**
 * I cannot begin to express how much I loathe Javascript / React / Frontend.
 * This function is an embodyment of my hatred and a big fuck you to this
 * world and all the horseshit that comes with it. I'm done. I'm going to go
 * write some Elixir.
 */
export const search = (searchStr: string, allIssues: Issue[]): Issue[] => {
  const res = searchIndex.search(searchStr);

  if (res.length) {
    // NOPE FUCK YOU, YOU CAN'T USE THE STORE OR ANYTHING LIKE IT HERE, WTF ARE
    // YOU THINKING?!?!?!?
    // const store = getStore();
    // const state: any = store.getState();
    // const issues = state.issues.store;

    const results = res
      .reduce((acc: number[], cur) => {
        return [...acc, parseInt(cur.ref)];
      }, [])
      .map(x => allIssues.find(i => i.id === x) as Issue);

    return results;
  }

  return [];
};
