import { createReducer } from 'typesafe-actions';
import { capitalize } from '../../helpers';
import { Issue, Menu, MenuItem, ProjectMenus, ProjectsStore } from '../../models';
import { Icons } from '../../theme/icons';
import { menuActions, MenuActions } from '../actions/menu';

export interface MenuState {
  isOpen: boolean;
  projects: ProjectsStore;
  getProjectLoading: boolean;
  selectedProject: number;
  menusForProjects: ProjectMenus;
}

const gitlabProject = { name: 'GitLab', id: 278964, selected: true, groupId: 9970 };

const createMenuForProject = (id: number, globalMenu: any = null): ProjectMenus => ({
  [id]: {
    issues: {
      id: 'issues',
      label: 'Issues',
      icon: Icons.ISSUES,
      isOpen: true,
      count: 0,
      type: 'folder',
      tree: {},
    },
    mergeRequests: {
      id: 'mergeRequests',
      label: 'Merge Requests',
      icon: Icons.MERGE_REQUEST,
      isOpen: false,
      count: 0,
      type: 'folder',
      tree: {},
    },
    milestones: {
      id: 'milestones',
      label: 'Milestones',
      icon: Icons.CLOCK,
      isOpen: false,
      count: 0,
      type: 'folder',
      tree: {},
    },
  },
  global: globalMenu
    ? globalMenu
    : {
        pinned: {
          id: 'pinned',
          label: 'Pinned',
          icon: Icons.THUMBTACK,
          isOpen: false,
          count: 0,
          type: 'folder',
          tree: {
            uncategorised: {
              id: 'uncategorised',
              label: 'Uncategorised',
              isOpen: false,
              count: 0,
              type: 'folder',
              tree: {},
            },
          },
        },
      },
});

export const initialMenuState = (): MenuState => ({
  isOpen: true,
  projects: {
    278964: gitlabProject,
  },
  getProjectLoading: false,
  selectedProject: gitlabProject.id,
  menusForProjects: createMenuForProject(gitlabProject.id),
});

export default createReducer<MenuState, MenuActions>(initialMenuState())
  .handleAction(menuActions.toggleMenu, (state, action) => ({
    ...state,
    isOpen: action.payload,
  }))
  .handleAction(menuActions.toggleMenuItem, (state, action) => {
    let menuItem = null;

    if (action.payload.id.includes('pinned')) {
      menuItem = getMenuItemInTree(action.payload.id, state.menusForProjects.global);
    } else {
      menuItem = getMenuItemInTree(
        action.payload.id,
        state.menusForProjects[state.selectedProject],
      );
    }

    if (menuItem) {
      menuItem.isOpen = action.payload.state;
    }

    return {
      ...state,
    };
  })
  .handleAction(menuActions.buildIssuesMenu, (state, action) => {
    let menusForProjects = createMenuForProject(gitlabProject.id, state.menusForProjects.global);

    menusForProjects[gitlabProject.id].mergeRequests = getExistingMergeRequests(
      gitlabProject.id,
      state,
      menusForProjects[gitlabProject.id].mergeRequests,
    );

    menusForProjects[gitlabProject.id].milestones = getExistingMilestones(
      gitlabProject.id,
      state,
      menusForProjects[gitlabProject.id].milestones,
    );

    if (
      state.menusForProjects[gitlabProject.id] &&
      state.menusForProjects[gitlabProject.id].mergeRequests
    ) {
      menusForProjects[gitlabProject.id].mergeRequests =
        state.menusForProjects[gitlabProject.id].mergeRequests;
    }

    action.payload.forEach((x) => {
      const { title = 'None' } = x.milestone || {};
      const id = title.toLowerCase();
      const projectId = x.project_id;

      if (!menusForProjects[projectId]) {
        menusForProjects = {
          ...menusForProjects,
          ...createMenuForProject(projectId),
        };

        menusForProjects[projectId].mergeRequests = getExistingMergeRequests(
          projectId,
          state,
          menusForProjects[projectId].mergeRequests,
        );

        menusForProjects[gitlabProject.id].milestones = getExistingMilestones(
          gitlabProject.id,
          state,
          menusForProjects[gitlabProject.id].milestones,
        );
      }

      const issues = menusForProjects[projectId].issues;
      ++(issues.count as number);
      const issuesTree = issues.tree || {};

      if (issuesTree && issuesTree[id]) {
        (issuesTree[id].tree as Menu)[x.id.toString()] = createMenuItem(x);
      } else {
        const isOpen = (state.menusForProjects[projectId]?.issues.tree || {})[id]?.isOpen || false;
        // const isOpen = false;

        issuesTree[id.toString()] = {
          id,
          label: title,
          isOpen: isOpen,
          count: 0,
          type: 'folder',
          tree: {
            [x.id]: createMenuItem(x),
          },
        };
      }
    });

    return {
      ...state,
      menusForProjects,
    };
  })
  .handleAction(menuActions.buildMergeRequestsMenu, (state, action) => {
    const mrMenu = state.menusForProjects[action.payload.projectId];
    mrMenu.mergeRequests.tree = {};

    for (let key of Object.keys(action.payload.map)) {
      const mapEntry = action.payload.map[key];

      (mrMenu.mergeRequests.tree as Menu)[key] = {
        id: key,
        label: capitalize(key),
        isOpen: false,
        count: mapEntry.length,
        type: 'folder',
        tree: mapEntry
          .map((x) => ({
            id: x.id.toString(),
            label: x.title,
            isOpen: false,
            count: 0,
            type: 'item',
            itemId: x.id,
          }))
          .reduce((acc, cur) => ({ ...acc, [cur.id]: cur }), {}),
      };
    }

    state.menusForProjects[action.payload.projectId] = mrMenu;

    return {
      ...state,
      menusForProjects: state.menusForProjects,
    };
  })
  .handleAction(menuActions.buildMilestonesMenu, (state, action) => {
    const milestonesMenu = state.menusForProjects[action.payload.projectId];
    milestonesMenu.milestones.tree = {};

    for (let key of action.payload.milestones) {
      (milestonesMenu.milestones.tree as Menu)[key] = {
        id: key,
        label: capitalize(key),
        isOpen: false,
        count: 0,
        type: 'item',
        tree: {},
      };
    }

    state.menusForProjects[action.payload.projectId] = milestonesMenu;

    return {
      ...state,
      menusForProjects: state.menusForProjects,
    };
  })
  .handleAction(menuActions.removeMilestoneMenuItem, (state, action) => {
    const milestonesMenu = state.menusForProjects[state.selectedProject].milestones;

    if (Object.keys(milestonesMenu).length > 0) {
      delete (milestonesMenu.tree || {})[action.payload];
    }

    return {
      ...state,
    };
  })
  .handleAction(menuActions.setGetProjectLoading, (state, action) => ({
    ...state,
    getProjectLoading: action.payload,
  }))
  .handleAction(menuActions.addProject, (state, action) => ({
    ...state,
    projects: {
      ...state.projects,
      [action.payload.id]: action.payload,
    },
  }))
  .handleAction(menuActions.setSelectedProject, (state, action) => ({
    ...state,
    selectedProject: action.payload,
  }))
  .handleAction(menuActions.addPinnedMenuItem, (state, action) => {
    const { category, id, issue } = action.payload;
    const categoryId = category.trim().toLowerCase();
    const folder = (state.menusForProjects.global.pinned.tree as Menu)[categoryId];

    if (folder) {
      (folder.tree as Menu)[id] = createMenuItem(issue);
    } else {
      (state.menusForProjects.global.pinned.tree as Menu)[categoryId] = {
        id: categoryId,
        label: category,
        isOpen: true,
        count: 0,
        type: 'folder',
        tree: {
          [id]: createMenuItem(issue),
        },
      };
    }

    return {
      ...state,
    };
  })
  .handleAction(menuActions.removePinnedMenuItem, (state, action) => {
    const pinnedFolders = state.menusForProjects.global.pinned.tree as Menu;

    for (let i of Object.keys(pinnedFolders)) {
      const menu = pinnedFolders[i].tree || {};
      const item = menu[action.payload];

      if (item) {
        delete menu[action.payload];

        if (i !== 'uncategorised' && Object.keys(menu).length === 0) {
          delete pinnedFolders[i];
        }
      }
    }

    return {
      ...state,
    };
  })
  .handleAction(menuActions.reset, (state, action) => initialMenuState());

const getMenuItemInTree = (key: string, tree: Menu): MenuItem | null => {
  const [parentKey, ...rest] = key.split('-');

  if (rest.length > 0) {
    const nextTree = tree[parentKey].tree;

    if (nextTree) {
      return getMenuItemInTree(rest.join('-'), nextTree);
    }

    return null;
  }

  return tree[parentKey];
};

const createMenuItem = (issue: Issue): MenuItem => ({
  id: issue.id.toString(),
  label: issue.title,
  isOpen: false,
  count: 0,
  type: 'item',
  itemId: issue.id,
});

const getExistingMergeRequests = (projectId: number, state: MenuState, newMenu: MenuItem) => {
  if (state.menusForProjects[projectId] && state.menusForProjects[projectId].mergeRequests) {
    return state.menusForProjects[projectId].mergeRequests;
  }

  return newMenu;
};

const getExistingMilestones = (projectId: number, state: MenuState, newMenu: MenuItem) => {
  if (state.menusForProjects[projectId] && state.menusForProjects[projectId].milestones) {
    return state.menusForProjects[projectId].milestones;
  }

  return newMenu;
};
