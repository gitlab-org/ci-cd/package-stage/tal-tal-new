import { createReducer } from 'typesafe-actions';
import { MergeRequest, MergeRequestMap } from '../../models';
import { mergeRequestActions, MergeRequestActions } from '../actions/merge-requests';
import { organiseMergeRequests } from './helpers';

type Store = {
  [key: number]: MergeRequest;
};

type MRMap = {
  user: MergeRequest[];
  action: MergeRequest[];
};

export interface MergeRequestState {
  isLoading: boolean;
  store: Store;
  map: MergeRequestMap;
  userId: number;
  selectedMergeRequest: number;
}

const initialState: MergeRequestState = {
  isLoading: false,
  store: {},
  map: {},
  userId: 0,
  selectedMergeRequest: 0,
};

export default createReducer<MergeRequestState, MergeRequestActions>(initialState)
  .handleAction(mergeRequestActions.setIsLoading, (state, action) => ({
    ...state,
    isLoading: action.payload,
  }))
  .handleAction(mergeRequestActions.setMergeRequests, (state, action) => {
    const { projectId, userId } = action.payload;

    const store = action.payload.mergeRequests.reduce(
      (acc, cur) => ({ ...acc, [cur.id]: { ...cur, type: 'merge-request' } }),
      {} as any,
    );

    const map = buildMRMap(store, projectId, userId);

    return {
      ...state,
      store: {
        ...state.store,
        ...store,
      },
      map: {
        ...state.map,
        ...map,
      },
      userId: action.payload.userId,
    };
  })
  .handleAction(mergeRequestActions.updateMergeRequest, (state, action) => {
    const { store, userId } = state;
    store[action.payload.id] = {
      ...action.payload,
      type: 'merge-request',
    };

    const map = buildMRMap(store, action.payload.project_id, userId);

    return {
      ...state,
      store,
      map: {
        ...state.map,
        ...map,
      },
    };
  })
  .handleAction(mergeRequestActions.setSelectedMergeRequest, (state, action) => ({
    ...state,
    selectedMergeRequest: action.payload,
  }))
  .handleAction(mergeRequestActions.reset, (state, action) => initialState);

const buildMRMap = (store: Store, projectId: number, userId: number) => {
  let map: MRMap = {
    user: [],
    action: [],
  };

  const flatStore: MergeRequest[] = [];

  for (let i in store) {
    if (store[i].project_id === projectId) {
      flatStore.push(store[i]);
    }
  }

  map = flatStore.reduce((acc, cur) => {
    if (cur.author.id === userId) {
      map.user.push(cur);
    } else {
      map.action.push(cur);
    }

    return acc;
  }, map);

  const { mergeRequestMap } = organiseMergeRequests(map.user, {});

  return {
    [projectId]: {
      'action required': map.action,
      ...mergeRequestMap,
    },
  };
};
