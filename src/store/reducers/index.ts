import { combineReducers } from 'redux';
import auth, { AuthState } from './auth';
import issues, { IssuesState } from './issues';
import menu, { MenuState } from './menu';
import mergeRequests, { MergeRequestState } from './merge-requests';
import milestones, { MilestoneState } from './milestones';
import pinned, { PinnedState } from './pinned';

export interface AppState {
  auth: AuthState;
  menu: MenuState;
  issues: IssuesState;
  pinned: PinnedState;
  mergeRequests: MergeRequestState;
  milestones: MilestoneState;
}

export default combineReducers({ auth, menu, issues, pinned, mergeRequests, milestones });
