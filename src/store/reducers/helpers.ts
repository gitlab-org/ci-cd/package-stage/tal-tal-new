import { highestPriorityWorkflow } from '../../helpers';
import { MergeRequest } from '../../models';

export const organiseMergeRequests = <T>(mergeRequests: MergeRequest[] = [], entity: T) => {
  const mergeRequestMap: any = {};

  if (mergeRequests.length) {
    mergeRequests.forEach((x) => {
      const workflow = highestPriorityWorkflow(x.labels || []);
      x.workflow = workflow;

      if (mergeRequestMap[workflow]) {
        mergeRequestMap[workflow].push(x);
      } else {
        mergeRequestMap[workflow] = [x];
      }
    });
  }

  return {
    ...entity,
    mergeRequests,
    mergeRequestMap,
  };
};
