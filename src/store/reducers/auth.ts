import { createReducer } from 'typesafe-actions';
import { GitLabUser } from '../../models';
import { authActions, AuthActions } from '../actions/auth';

export interface AuthState {
  accessToken: string | null;
  user: GitLabUser | null;
}

const initialState: AuthState = {
  accessToken: null,
  user: null,
};

export default createReducer<AuthState, AuthActions>(initialState)
  .handleAction(authActions.setAccessToken, (state, action) => ({
    ...state,
    accessToken: action.payload,
  }))
  .handleAction(authActions.setUser, (state, action) => ({
    ...state,
    user: action.payload,
  }))
  .handleAction(authActions.reset, (state, action) => initialState);
