import { createReducer } from 'typesafe-actions';
import { PinnedItemStore } from '../../models';
import { pinnedActions, PinnedActions } from '../actions/pinned';

export interface PinnedState {
  store: PinnedItemStore;
  itemLoading: boolean;
  selectedItem: number | null;
}

export const initialState = (): PinnedState => ({
  store: {},
  itemLoading: false,
  selectedItem: null,
});

export default createReducer<PinnedState, PinnedActions>(initialState())
  .handleAction(pinnedActions.addPinnedItem, (state, action) => ({
    ...state,
    store: {
      ...state.store,
      [action.payload.id]: action.payload.data,
    },
  }))
  .handleAction(pinnedActions.setItemLoading, (state, action) => ({
    ...state,
    itemLoading: action.payload,
  }))
  .handleAction(pinnedActions.setSelectedItem, (state, action) => ({
    ...state,
    selectedItem: action.payload,
  }))
  .handleAction(pinnedActions.removePinnedItem, (state, action) => {
    delete state.store[action.payload];

    return {
      ...state,
    };
  })
  .handleAction(pinnedActions.reset, (state, action) => initialState());
