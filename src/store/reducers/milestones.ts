import { createReducer } from 'typesafe-actions';
import { TrackedMilestone, TrackMilestoneMap } from '../../models';
import { milestoneActions, MilestoneActions } from '../actions/milestones';

export interface MilestoneState {
  isLoading: boolean;
  store: TrackMilestoneMap;
  selectedMilestone: string | null;
}

const initialState: MilestoneState = {
  isLoading: false,
  store: {},
  selectedMilestone: null,
};

export default createReducer<MilestoneState, MilestoneActions>(initialState)
  .handleAction(milestoneActions.toggleMilestonesLoading, (state, action) => ({
    ...state,
    isLoading: action.payload,
  }))
  .handleAction(milestoneActions.addNewMilestone, (state, action) => ({
    ...state,
    store: {
      ...state.store,
      [action.payload]: createNewMilestone(action.payload),
    },
  }))
  .handleAction(milestoneActions.removeMilestone, (state, action) => {
    const { store } = state;
    delete store[action.payload];

    return {
      ...state,
      store,
    };
  })
  .handleAction(milestoneActions.setSelectedMilestone, (state, action) => ({
    ...state,
    selectedMilestone: action.payload,
  }))
  .handleAction(milestoneActions.addDataToMilestone, (state, action) => {
    const { milestoneId, mergeRequests } = action.payload;
    const { store } = state;
    const milestone = store[milestoneId];

    const { milestone: milestoneFromMR } = mergeRequests[0];

    if (milestoneFromMR) {
      milestone.startDate = milestoneFromMR.start_date || '';
      milestone.endDate = milestoneFromMR.due_date || '';
    }

    milestone.mergeRequests = mergeRequests;
    milestone.open = mergeRequests.filter((x) => !x.closed_at && !x.merged_at).length;
    milestone.merged = mergeRequests.filter((x) => x.merged_at).length;

    return {
      ...state,
      store: {
        ...store,
      },
    };
  })
  .handleAction(milestoneActions.addOrEditNoteToMilestone, (state, action) => {
    const { milestoneId, content, noteType, noteIndex } = action.payload;

    const { store } = state;
    const milestone = store[milestoneId];
    const notes = noteType === 'retro' ? milestone.retroNotes : milestone.weeklyNotes;

    if (noteIndex !== undefined) {
      notes[noteIndex] = content;
    } else {
      notes.push(content);
    }

    return {
      ...state,
      store: {
        ...store,
      },
    };
  })
  .handleAction(milestoneActions.removeNoteFromMilestone, (state, action) => {
    const { milestoneId, noteType, noteIndex } = action.payload;

    const { store } = state;
    const milestone = store[milestoneId];
    const notes = noteType === 'retro' ? milestone.retroNotes : milestone.weeklyNotes;

    if (noteIndex !== undefined) {
      notes.splice(noteIndex, 1);
    }

    return {
      ...state,
      store: {
        ...store,
      },
    };
  })
  .handleAction(milestoneActions.reset, (state, action) => initialState);

const createNewMilestone = (id: string): TrackedMilestone => ({
  id,
  mergeRequests: [],
  startDate: '',
  endDate: '',
  open: 0,
  merged: 0,
  avgPerWeek: 0,
  retroNotes: [],
  weeklyNotes: [],
});
