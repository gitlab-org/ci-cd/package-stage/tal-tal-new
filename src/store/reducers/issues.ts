import moment from 'moment';
import { createReducer } from 'typesafe-actions';
import { highestPriorityWorkflow, isInVerification, possiblyProductionReady } from '../../helpers';
import { Issue, IssueStore, Milestone, MilestonesProjectMenu } from '../../models';
import { IssueActions, issueActions } from '../actions/issues';
import { organiseMergeRequests } from './helpers';

export interface IssuesState {
  importLoading: boolean;
  updateLoading: boolean;
  store: IssueStore;
  milestones: MilestonesProjectMenu;
  selectedIssue: number | null;
  skippedIssues: {
    [key: number]: number;
  };
  lastUpdated: number;
}

const initialState: IssuesState = {
  importLoading: false,
  updateLoading: false,
  store: {},
  milestones: {},
  selectedIssue: null,
  skippedIssues: {},
  lastUpdated: 0,
};

export default createReducer<IssuesState, IssueActions>(initialState)
  .handleAction(issueActions.setImportLoading, (state, action) => ({
    ...state,
    importLoading: action.payload,
  }))
  .handleAction(issueActions.setUpdateLoading, (state, action) => ({
    ...state,
    updateLoading: action.payload,
  }))
  .handleAction(issueActions.setIssues, (state, action) => {
    const milestones: MilestonesProjectMenu = {};
    const store: IssueStore = {};

    action.payload.forEach((x) => {
      createOrAddToMilestone(milestones, x.milestone, x);
      // TODO: Processing is getting more and more complex, with nested loops and all sorts.
      // We should consider how to simplify this and only process certain parts when needed.
      // We'll skip this for now as the numbers in these loops should be very small and so
      // performance won't likely be a factor. The code could be tidied though.

      // Also, where is my |> operator :(
      x.workflow = highestPriorityWorkflow(x.labels);
      x.type = 'issue';
      const withMrs = organiseMergeRequests(x.mergeRequests, x);
      const withAsync = extractAsyncUpdates(withMrs);
      withAsync.possiblyReadyForProduction = possiblyProductionReady(withAsync);
      withAsync.possiblyVerificationReady = isInVerification(withAsync);
      store[x.id] = withAsync;
    });

    return {
      ...state,
      store,
      milestones,
      lastUpdated: new Date().valueOf(),
    };
  })
  .handleAction(issueActions.setSelectedIssue, (state, action) => ({
    ...state,
    selectedIssue: action.payload,
  }))
  .handleAction(issueActions.skipIssueUpdate, (state, action) => {
    const issue = state.store[action.payload];
    const { skippedIssues = {} } = state;

    if (issue) {
      skippedIssues[issue.id] = new Date().valueOf();
    }

    return {
      ...state,
      skippedIssues,
    };
  })
  .handleAction(issueActions.updateIssue, (state, action) => {
    const withMrs = organiseMergeRequests(action.payload.mergeRequests, action.payload);
    const withAsync = extractAsyncUpdates(withMrs);

    const issue = {
      ...withAsync,
      possiblyReadyForProduction: possiblyProductionReady(withAsync),
      possiblyVerificationReady: isInVerification(withAsync),
      workflow: highestPriorityWorkflow(withAsync.labels),
    };

    return {
      ...state,
      store: {
        ...state.store,
        [issue.id]: issue,
      },
    };
  })
  .handleAction(issueActions.reset, (state, action) => initialState);

/**
 * This is a lovely little function that is delightfully readable and ever so
 * easily debuggable. It creates our milestone menu with milestones nested
 * inside project id's.
 * @param milestones
 * @param milestoneId
 * @param issue
 */
const createOrAddToMilestone = (
  milestones: MilestonesProjectMenu,
  milestone: Milestone | null,
  issue: Issue,
) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { title: milestoneId = 'None', start_date: startDate, due_date: dueDate } = milestone || {};

  if (milestones[issue.project_id]) {
    if (milestones[issue.project_id][milestoneId]) {
      milestones[issue.project_id][milestoneId].issues.push(issue.id);
    } else {
      milestones[issue.project_id][milestoneId] = {
        issues: [issue.id],
        startDate,
        dueDate,
      };
    }
  } else {
    milestones[issue.project_id] = {
      [milestoneId]: {
        issues: [issue.id],
        startDate,
        dueDate,
      },
    };
  }

  milestones[issue.project_id][milestoneId].count =
    milestones[issue.project_id][milestoneId].issues.length;
};

const extractAsyncUpdates = ({ notes = [], ...issue }: Issue) => {
  const today = moment();
  const possible = ['async issue update', 'async update', 'issue update'];
  const asyncUpdates = [];

  for (let i = 0; i < notes.length; i++) {
    const note = notes[i];

    for (let p of possible) {
      if (note.body.toLowerCase().includes(p)) {
        if (!issue.updateCompleted) {
          const date = moment(note.created_at);

          if (today.isSame(date, 'day')) {
            issue.updateCompleted = true;
          }
        }

        asyncUpdates.push(note);
        break;
      }
    }
  }

  return {
    ...issue,
    asyncUpdates: asyncUpdates.reverse(),
    notes,
  };
};
