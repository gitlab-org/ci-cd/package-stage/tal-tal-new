import ClientOAuth2 from 'client-oauth2';
import { ThunkDispatch } from 'redux-thunk';
import { Action, ActionType, createAction } from 'typesafe-actions';
import history from '../../history';
import { GitLabUser } from '../../models';
import axios from '../axios';
import { AppState } from '../reducers';
import { issueActions } from './issues';
import { menuActions } from './menu';
import { mergeRequestActions } from './merge-requests';
import { pinnedActions } from './pinned';

// Use OAuth to authenticate with GitLab
// TODO: Allow for custom GitLab instances and not just .com?
export const oauth = new ClientOAuth2({
  clientId: process.env.REACT_APP_CLIENT_ID,
  authorizationUri: 'https://gitlab.com/oauth/authorize',
  accessTokenUri: 'https://gitlab.com/oauth/token',
  redirectUri: process.env.REACT_APP_REDIRECT_URI,
  scopes: ['api', 'read_user', 'profile', 'openid'],
});

// === BASIC ACTIONS === //

const setAccessToken = createAction('SET_ACCESS_TOKEN')<any | null>();
const setUser = createAction('SET_USER')<GitLabUser>();
const reset = createAction('AUTH_RESET')<void>();

export const authActions = {
  setAccessToken,
  setUser,
  reset,
};

export type AuthActions = ActionType<typeof authActions>;

// === ASYNC ACTIONS === //

export const startLogin = () => {
  return async () => (window.location.href = oauth.code.getUri());
};


export const getCurrentUserDetails = () => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>) => {
    try {
      const { data } = await axios.get('/user');
      dispatch(setUser(data));
    } catch (error) {
      dispatch(setAccessToken(''));
      history.push('/');
    }
  };
};

export const signOut = () => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>) => {
    dispatch(issueActions.reset());
    dispatch(menuActions.reset());
    dispatch(pinnedActions.reset());
    dispatch(mergeRequestActions.reset());
    dispatch(reset());

    history.push('/');
  };
};
