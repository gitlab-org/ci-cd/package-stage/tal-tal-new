import iziToast from 'izitoast';
import { ThunkDispatch } from 'redux-thunk';
import { Action, ActionType, createAction } from 'typesafe-actions';
import { MergeRequest } from '../../models';
import { fetchAllThroughPagination } from '../axios';
import { AppState } from '../reducers';

// === BASIC ACTIONS === //

type AddDataPayload = {
  milestoneId: string;
  mergeRequests: MergeRequest[];
};

type NotePayload = {
  milestoneId: string;
  content: string;
  noteType: 'retro' | 'weekly';
  noteIndex?: number;
};

const toggleMilestonesLoading = createAction('TOGGLE_MILESTONES_LOADING')<boolean>();
const addNewMilestone = createAction('ADD_MILESTONE')<string>();
const removeMilestone = createAction('REMOVE_MILESTONE')<string>();
const setSelectedMilestone = createAction('SET_SELECTED_MILESTONE')<string | null>();
const addDataToMilestone = createAction('ADD_DATA_TO_MILESTONE')<AddDataPayload>();
const addOrEditNoteToMilestone = createAction('ADD_OR_EDIT_MILESTONE_NOTE')<NotePayload>();
const removeNoteFromMilestone = createAction('REMOVE_NOTE_FROM_MILESTONE')<NotePayload>();
const reset = createAction('MILESTONES_RESET')<void>();

export const milestoneActions = {
  toggleMilestonesLoading,
  addNewMilestone,
  removeMilestone,
  setSelectedMilestone,
  addDataToMilestone,
  addOrEditNoteToMilestone,
  removeNoteFromMilestone,
  reset,
};

export type MilestoneActions = ActionType<typeof milestoneActions>;

// === ASYNC ACTIONS === //

export const getStatsForSelectedMilestone = (milestoneId: string) => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>, getState: Function) => {
    dispatch(toggleMilestonesLoading(true));

    // TODO: This only gets MRs that were created by the current user.
    // Should we also consider getting assigned and merge the resulting lists?

    const state: AppState = getState();
    // const { id: userId = 0 } = state.auth.user || {};
    const { selectedProject: projectId } = state.menu || {};

    try {
      const mergeRequests = await fetchAllThroughPagination<MergeRequest>(
        `/projects/${projectId}/merge_requests`,
        {
          scope: 'created_by_me',
          // assignee_id: userId,
          milestone: milestoneId,
        },
      );

      dispatch(addDataToMilestone({ milestoneId, mergeRequests }));
    } catch (ex) {
      console.log(ex);

      iziToast.error({
        message: 'Unable to fetch milestone information.',
      });
    } finally {
      dispatch(toggleMilestonesLoading(false));
    }
  };
};
