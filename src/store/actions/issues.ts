import iziToast from 'izitoast';
import { ThunkDispatch } from 'redux-thunk';
import { Action, ActionType, createAction } from 'typesafe-actions';
import { Issue, IssueNote, MergeRequest, Scopes } from '../../models';
import axios, { fetchAllThroughPagination } from '../axios';
import { AppState } from '../reducers';
import { buildSearchIndex } from '../search';
import { menuActions } from './menu';
import { mergeRequestActions } from './merge-requests';

// === BASIC ACTIONS === //

const setIssues = createAction('SET_ISSUES')<Issue[]>();
const setImportLoading = createAction('SET_IMPORT_LOADING')<boolean>();
const setUpdateLoading = createAction('SET_UPDATE_LOADING')<boolean>();
const setSelectedIssue = createAction('SET_SELECTED_ISSUE')<number | null>();
const skipIssueUpdate = createAction('SKIP_UPDATE')<number>();
const updateIssue = createAction('UPDATE_ISSUE')<Issue>();
const reset = createAction('RESET_ISSUES')<void>();

export const issueActions = {
  setIssues,
  setImportLoading,
  setUpdateLoading,
  setSelectedIssue,
  skipIssueUpdate,
  updateIssue,
  reset,
};

export type IssueActions = ActionType<typeof issueActions>;

// === ASYNC ACTIONS === //

export const importIssues = (successMessage?: string) => {
  const errMsg = 'There was an error importing the issues. Please try again.';

  return async (dispatch: ThunkDispatch<AppState, void, Action>, getState: Function) => {
    dispatch(setImportLoading(true));
    const state = getState();
    const { projects = {} } = state.menu;
    const projectIds = Object.keys(projects);

    try {
      const issues = await fetchAllThroughPagination<Issue>('/issues', {
        state: 'opened',
        scope: 'assigned_to_me',
      });

      // We only care about issues in projects that the user is subscribed to
      const filteredIssues = issues.filter((x) => projectIds.includes(x.project_id.toString()));

      const issueDetailToFetch = [];

      for (let i of filteredIssues) {
        issueDetailToFetch.push(getIssueDetail(i));
      }

      Promise.all(issueDetailToFetch)
        .then((updatedIssues) => {
          dispatch(setIssues(updatedIssues));
          dispatch(menuActions.buildIssuesMenu(updatedIssues));
          dispatch(buildSearchIndex(updatedIssues));

          iziToast.success({
            message: successMessage || 'Your issues have been imported successfully.',
          });
        })
        .catch((ex) => {
          console.error(ex);

          iziToast.error({
            message: errMsg,
          });
        })
        .finally(() => {
          dispatch(setImportLoading(false));
        });
    } catch (ex) {
      console.log(ex);

      dispatch(setImportLoading(false));

      iziToast.error({
        message: errMsg,
      });
    }
  };
};

export const addAsyncUpdate = (issueId: number, updateContent: string) => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>, getState: Function) => {
    const state = getState();
    const issue = state.issues.store[issueId];

    if (issue) {
      try {
        dispatch(setUpdateLoading(true));

        await axios.post(`/projects/${issue.project_id}/issues/${issue.iid}/notes`, null, {
          params: { body: updateContent },
        });

        const refreshedIssue = await refreshIssue(issue.project_id, issue.iid);
        const withDetail = await getIssueDetail(refreshedIssue);

        dispatch(updateIssue(withDetail));

        iziToast.success({
          message: 'Async update successfully sent.',
        });
      } catch (ex) {
        console.error(ex);

        iziToast.error({
          message: 'There was an error sending the update.',
        });
      } finally {
        dispatch(setUpdateLoading(false));
      }
    }
  };
};

export const updateWorkflow = (issue: Issue, newWorkflow: string, mergeRequests: number[] = []) => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>, getState: Function) => {
    const issueUrl = `/projects/${issue.project_id}/issues/${issue.iid}`;

    try {
      dispatch(setUpdateLoading(true));

      const {
        data: { labels },
      } = await axios.get<Issue>(issueUrl);
      const workflowIndex = labels.findIndex((x) => x.includes(Scopes.WORKFLOW));

      if (workflowIndex > -1) {
        labels.splice(workflowIndex, 1);
      }

      labels.push(`${Scopes.WORKFLOW}${newWorkflow}`);

      const { data } = await axios.put(issueUrl, {
        labels,
      });

      const mrUpdates: Promise<MergeRequest>[] = [];

      mergeRequests.forEach((x) => {
        const index = issue.mergeRequests?.findIndex((m) => m.id === x) ?? null;

        if (index !== null && index > -1) {
          const [mergeRequest] = issue.mergeRequests?.splice(index, 1) as MergeRequest[];
          mrUpdates.push(updateWorkflowForMergeRequest(mergeRequest, newWorkflow));
        }
      });

      Promise.all(mrUpdates)
        .then((updatedMergeRequests) => {
          dispatch(
            updateIssue({
              ...issue,
              ...data,
              mergeRequests: [...(issue.mergeRequests || []), ...updatedMergeRequests],
            }),
          );
          dispatch(setUpdateLoading(false));

          iziToast.success({
            message: 'Workflow labels have been updated.',
          });
        })
        .catch((ex) => {
          console.error(ex);

          dispatch(setUpdateLoading(false));

          iziToast.error({
            message: 'There was an error sending the update.',
          });
        });
    } catch (ex) {
      console.error(ex);

      dispatch(setUpdateLoading(false));

      iziToast.error({
        message: 'There was an error sending the update.',
      });
    }
  };
};

export const updateMergeRequestWorkflow = (
  mergeRequest: MergeRequest,
  newWorkflow: string,
  issueId: number,
) => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>, getState: Function) => {
    try {
      dispatch(setUpdateLoading(true));

      const state: AppState = getState();
      const issue = state.issues.store[issueId];

      if (issue) {
        const index = issue.mergeRequests?.findIndex((m) => m.id === mergeRequest.id) ?? null;

        if (index !== null && index > -1) {
          const [existingMergeRequest] = issue.mergeRequests?.splice(index, 1) as MergeRequest[];
          const newMr = await updateWorkflowForMergeRequest(existingMergeRequest, newWorkflow);

          dispatch(
            updateIssue({
              ...issue,
              mergeRequests: [...(issue.mergeRequests || []), newMr],
            }),
          );

          iziToast.success({
            message: 'Workflow labels have been updated.',
          });
        }
      } else {
        const { store = {} } = state.mergeRequests || {};
        const existingMergeRequest = store[mergeRequest.id];

        if (existingMergeRequest) {
          const newMr = await updateWorkflowForMergeRequest(existingMergeRequest, newWorkflow);

          dispatch(mergeRequestActions.updateMergeRequest(newMr));

          dispatch(
            menuActions.buildMergeRequestsMenu({
              projectId: newMr.project_id,
              map: getState().mergeRequests.map[newMr.project_id],
            }),
          );

          iziToast.success({
            message: 'Workflow labels have been updated.',
          });
        }
      }
    } catch (ex) {
      console.error(ex);

      iziToast.error({
        message: 'There was an error updating the workflow.',
      });
    } finally {
      dispatch(setUpdateLoading(false));
    }
  };
};

// === Helpers === //

const refreshIssue = async (projectId: number, issueId: number) => {
  const { data } = await axios.get(`/projects/${projectId}/issues/${issueId}`);

  return data;
};

const getIssueDetail = async (issue: Issue) => {
  /**
   * TODO: Consider how this can be improved, I don't like this for a number of reasons:
   *
   * 1. We're making _too many requests_. How can this be reduced?
   * 2. These sets of requests are sequential.
   * 3. If we need _even more data_, we can't continue like this.
   * 4. Refreshing data is very expensive.
   * 5. We not currently caching anything - how could we even do this?
   */

  const notesUrl = `/projects/${issue.project_id}/issues/${issue.iid}/notes`;
  const mergeRequestsUrl = `/projects/${issue.project_id}/issues/${issue.iid}/related_merge_requests`;

  const notes = await fetchAllThroughPagination<IssueNote>(notesUrl, {
    order_by: 'updated_at',
  });

  const mergeRequests = await fetchAllThroughPagination<MergeRequest>(mergeRequestsUrl);

  issue.notes = notes;
  issue.mergeRequests = mergeRequests;

  return issue;
};

const updateWorkflowForMergeRequest = async (
  mergeRequest: MergeRequest,
  newWorkflow: string,
): Promise<MergeRequest> => {
  const url = `/projects/${mergeRequest.project_id}/merge_requests/${mergeRequest.iid}`;

  const {
    data: { labels },
  } = await axios.get<MergeRequest>(url);

  const workflowIndex = labels.findIndex((x) => x.includes(Scopes.WORKFLOW));

  if (workflowIndex > -1) {
    labels.splice(workflowIndex, 1);
  }

  labels.push(`${Scopes.WORKFLOW}${newWorkflow}`);

  const { data } = await axios.put(url, {
    labels,
  });

  return data;
};
