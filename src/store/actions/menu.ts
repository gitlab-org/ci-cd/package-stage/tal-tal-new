import iziToast from 'izitoast';
import { ThunkDispatch } from 'redux-thunk';
import { Action, ActionType, createAction } from 'typesafe-actions';
import { Issue, IssueProject, ProjectMergeRequestsMap } from '../../models';
import axios from '../axios';
import { AppState } from '../reducers';
import { importIssues } from './issues';

// === BASIC ACTIONS === //

type TogglePayload = {
  id: string;
  state: boolean;
};

type AddPinnedItemPayload = {
  category: string;
  id: number;
  issue: Issue;
};

type BuildMRMenuPayload = {
  projectId: number;
  map: ProjectMergeRequestsMap;
};

type BuildMilestonesPayload = {
  projectId: number;
  milestones: string[];
};

const toggleMenu = createAction('TOGGLE_MENU')<boolean>();
const toggleMenuItem = createAction('TOGGLE_MENU_ITEM')<TogglePayload>();
const buildIssuesMenu = createAction('BUILD_ISSUES_MENU')<Issue[]>();
const buildMergeRequestsMenu = createAction('BUILD_MERGE_REQUESTS_MENU')<BuildMRMenuPayload>();
const buildMilestonesMenu = createAction('BUILD_MILESTONES_MENU')<BuildMilestonesPayload>();
const removeMilestoneMenuItem = createAction('REMOVE_MILESTONE_ITEM')<string>();
const setGetProjectLoading = createAction('SET_GET_PROJECT_LOADING')<boolean>();
const addProject = createAction('ADD_PROJECT')<IssueProject>();
const setSelectedProject = createAction('SET_SELECTED_PROJECT')<number>();
const addPinnedMenuItem = createAction('ADD_PINNED_MENU_ITEM')<AddPinnedItemPayload>();
const removePinnedMenuItem = createAction('REMOVE_PINNED_MENU_ITEM')<number>();
const reset = createAction('RESET_MENU')<void>();

export const menuActions = {
  toggleMenu,
  toggleMenuItem,
  buildIssuesMenu,
  buildMergeRequestsMenu,
  buildMilestonesMenu,
  removeMilestoneMenuItem,
  setGetProjectLoading,
  addProject,
  setSelectedProject,
  addPinnedMenuItem,
  removePinnedMenuItem,
  reset,
};

export type MenuActions = ActionType<typeof menuActions>;

export const addNewProject = (projectId: number, postImport: boolean = true) => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>, getState: Function) => {
    const state = getState();
    const project = state.menu.projects[projectId];

    if (!project) {
      try {
        dispatch(setGetProjectLoading(true));

        const { data } = await axios.get(`/projects/${projectId}`);

        dispatch(
          addProject({
            name: data.name,
            id: data.id,
            selected: false,
            groupId: data.namespace?.id,
          }),
        );

        if (postImport) {
          // After adding a project, kick off a import to get all the issues
          dispatch(importIssues('Project added and issues imported.'));
        }
      } catch (ex) {
        console.error(ex);

        iziToast.error({
          message: 'There was an error fetching the project.',
        });
      } finally {
        dispatch(setGetProjectLoading(false));
      }
    }
  };
};
