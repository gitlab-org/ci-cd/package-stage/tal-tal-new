import iziToast from 'izitoast';
import { ThunkDispatch } from 'redux-thunk';
import { Action, ActionType, createAction } from 'typesafe-actions';
import { Issue, IssueProject, PinnedItem, PinnedItemType } from '../../models';
import axios from '../axios';
import { AppState } from '../reducers';
import { menuActions } from './menu';

type ItemType = {
  type: PinnedItemType;
  id: number;
};

// === BASIC ACTIONS === //

type AddPinnedItemPayload = {
  id: number;
  data: Issue;
};

const addPinnedItem = createAction('ADD_PINNED_ITEM')<AddPinnedItemPayload>();
const setItemLoading = createAction('SET_ITEM_LOADING')<boolean>();
const setSelectedItem = createAction('SET_SELECTED_ITEM')<number | null>();
const removePinnedItem = createAction('REMOVE_PINNED_ITEM')<number>();
const reset = createAction('RESET_PINNED_ITEMS')<void>();

export const pinnedActions = {
  addPinnedItem,
  setItemLoading,
  setSelectedItem,
  removePinnedItem,
  reset,
};

export type PinnedActions = ActionType<typeof pinnedActions>;

// === ASYNC ACTIONS === //

export const addNewPinnedItem = (itemId: string, projectId: number, category: string) => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>, getState: Function) => {
    dispatch(setItemLoading(true));

    const state: AppState = getState();
    const items: PinnedItem[] = [];
    const { type, id } = getItemType(itemId);

    if (!isNaN(id) && !items.find((x) => x.id === id)) {
      try {
        const project = state.menu.projects[projectId];

        if (project) {
          const fetchFunction = getFetchFunction(type);
          const item = await fetchFunction(project, id);

          // Set that this is a pinned item and therefore is treated differently
          item.isPinned = true;

          dispatch(addPinnedItem({ id: item.id, data: item }));
          dispatch(menuActions.addPinnedMenuItem({ category, id: item.id, issue: item }));

          iziToast.success({
            message: 'Successfully pinned issue.',
          });
        }
      } catch (ex) {
        iziToast.error({
          message: 'There was an error with the request.',
        });
      } finally {
        dispatch(setItemLoading(false));
      }
    }
  };
};

// === Helpers === //

const getItemType = (id: string): ItemType => {
  const charAZero = id.trim().charAt(0);
  const rest = parseInt(id.trim().substring(1));

  switch (charAZero) {
    case '&':
      return {
        type: PinnedItemType.EPIC,
        id: rest,
      };

    case '#':
      return {
        type: PinnedItemType.ISSUE,
        id: rest,
      };

    default:
      return {
        type: PinnedItemType.ISSUE,
        id: parseInt(id.trim()),
      };
  }
};

const getFetchFunction = (type: PinnedItemType): ((project: IssueProject, id: number) => any) => {
  switch (type) {
    case PinnedItemType.EPIC:
      return getEpicDetail;

    default:
      return getIssueDetail;
  }
};

const getIssueDetail = async (project: IssueProject, issueIid: number) => {
  const { data } = await axios.get<Issue>(`/projects/${project.id}/issues/${issueIid}`);

  return data;
};

const getEpicDetail = async (project: IssueProject, epicId: number) => {
  const { data } = await axios.get(`/groups/${project.groupId}/epics/${epicId}`);

  return {
    ...data,
    isEpic: true,
  };
};
