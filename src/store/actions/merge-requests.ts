import parse from 'cc-parse';
import iziToast from 'izitoast';
import { ThunkDispatch } from 'redux-thunk';
import { Action, ActionType, createAction } from 'typesafe-actions';
import { MergeRequest, MergeRequestNote, MergeRequestReviewItem } from '../../models';
import { fetchAllThroughPagination } from '../axios';
import { AppState } from '../reducers';
import { menuActions } from './menu';

// === BASIC ACTIONS === //

type MergeRequestsPayload = {
  userId: number;
  projectId: number;
  mergeRequests: MergeRequest[];
};

const setIsLoading = createAction('SET_MERGE_REQUEST_LOADING')<boolean>();
const setMergeRequests = createAction('SET_MERGE_REQUESTS')<MergeRequestsPayload>();
const updateMergeRequest = createAction('UPDATE_MERGE_REQUEST')<MergeRequest>();
const setSelectedMergeRequest = createAction('SET_SELECTED_MERGE_REQUEST')<number>();
const reset = createAction('RESET_MERGE_REQUESTS')<void>();

export const mergeRequestActions = {
  setIsLoading,
  setMergeRequests,
  updateMergeRequest,
  setSelectedMergeRequest,
  reset,
};

export type MergeRequestActions = ActionType<typeof mergeRequestActions>;

export const fetchMergeRequestsForProject = (projectId: number) => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>, getState: Function) => {
    dispatch(setIsLoading(true));

    const state: AppState = getState();
    const { id = 0 } = state.auth.user || {};

    try {
      const mergeRequests = await fetchAllThroughPagination<MergeRequest>(
        `/projects/${projectId}/merge_requests`,
        {
          state: 'opened',
          scope: 'assigned_to_me',
          assignee_id: id,
        },
      );

      dispatch(setMergeRequests({ userId: id, projectId, mergeRequests }));
      dispatch(
        menuActions.buildMergeRequestsMenu({
          projectId,
          map: getState().mergeRequests.map[projectId],
        }),
      );
    } catch (ex) {
      console.log(ex);

      iziToast.error({
        message: 'Unable to fetch merge requests.',
      });
    } finally {
      dispatch(setIsLoading(false));
    }
  };
};

export const fetchMergeRequestComments = (mergeRequest: MergeRequest) => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>, getState: Function) => {
    try {
      const allNotes = await fetchAllThroughPagination<MergeRequestNote>(
        `/projects/${mergeRequest.project_id}/merge_requests/${mergeRequest.iid}/notes`,
      );

      const reviewItems: MergeRequestReviewItem[] = [];

      // Filter out any notes that are system or non discussion related
      allNotes
        .filter((x) => x.type)
        .forEach((x) => {
          const result = parse(x.body);

          if (result) {
            reviewItems.push({
              author: x.author,
              resolved: x.resolved,
              parseResult: result,
              webUrl: `${mergeRequest.web_url}#note_${x.id}`,
            });
          }
        });

      dispatch(
        updateMergeRequest({
          ...mergeRequest,
          reviewItems,
        }),
      );
    } catch (ex) {
      console.error(ex);
    }
  };
};
