import moment from 'moment';
import { AppState } from './reducers';

// === Auth === //

export const getAccessToken = (store: AppState) => {
  return store.auth.accessToken;
};

export const getCurrentUser = (store: AppState) => {
  return store.auth.user;
};

// === Issues === //

export const getImportLoading = (store: AppState) => {
  return (store.issues.importLoading || store.mergeRequests?.isLoading) ?? false;
};

export const getIssuesListWithMilestones = (store: AppState) => {
  const { selectedProject } = store.menu;
  const { milestones } = store.issues;
  const projectMilestonesMenu = milestones[selectedProject] || {};

  if (Object.keys(projectMilestonesMenu).length) {
    return milestones[selectedProject];
  }

  return null;
};

export const getIssueById = (store: AppState, id: number) => {
  const { store: issuesStore = {} } = store.issues;
  const { store: pinnedStore = {} } = store.pinned;

  return issuesStore[id] || pinnedStore[id];
};

export const getSelectedIssue = (store: AppState) => {
  const { selectedIssue, store: issuesStore } = store.issues;

  if (selectedIssue) {
    return issuesStore[selectedIssue];
  }

  return null;
};

export const isIssueSkipped = (store: AppState, issueId: number) => {
  if (!store.issues.skippedIssues) {
    return false;
  }

  const skippedTs = store.issues.skippedIssues[issueId];

  if (skippedTs) {
    return moment(skippedTs).isSame(moment(), 'date');
  }

  return false;
};

export const isUpdateLoading = (store: AppState) => {
  return store.issues.updateLoading || false;
};

export const getRecommendUpdate = (store: AppState) => {
  const { lastUpdated } = store.issues;

  if (lastUpdated) {
    if (moment(lastUpdated).isBefore(moment(), 'date')) {
      return true;
    }
  }

  return false;
};

export const getLastUpdated = (store: AppState) => {
  return store.issues.lastUpdated;
};

export const getAllIssues = (store: AppState) => {
  const { store: allIssues = {} } = store.issues;

  return Object.keys(allIssues).map((x) => allIssues[x as any]);
};

// === Menu === //

export const isMenuOpen = (store: AppState) => {
  return store.menu.isOpen ?? true;
};

export const getMenu = (store: AppState) => {
  const { selectedProject } = store.menu;

  if (selectedProject) {
    return {
      ...store.menu.menusForProjects[selectedProject],
      ...store.menu.menusForProjects.global,
    };
  }

  return {};
};

export const getProjects = (store: AppState) => {
  const { projects } = store.menu;

  return Object.keys(projects).map((x) => projects[parseInt(x)]);
};

export const getSelectedProject = (store: AppState) => {
  const { selectedProject, projects } = store.menu;

  return projects[selectedProject];
};

export const getProjectsLoading = (store: AppState) => {
  return store.menu.getProjectLoading || false;
};

// === Milestones === //

export const getMilestonesLoading = (store: AppState) => {
  return store.milestones?.isLoading || false;
};

// export const getCurrentMilestone = (store: AppState) => {
//   // eslint-disable-next-line @typescript-eslint/no-unused-vars
//   const today = moment();
//   const { milestones } = store.issues;
//   const currentProjectMenu = milestones[store.menu.selectedProject];

//   const keys = Object.keys(currentProjectMenu);

//   for (let k of keys) {
//     const milestone = currentProjectMenu[k];

//     if (milestone.startDate && milestone.dueDate) {
//       const start = moment(milestone.startDate);
//       const end = moment(milestone.dueDate);

//       if (today.isBetween(start, end)) {
//         return milestone;
//       }
//     }
//   }

//   return null;
// };

export const getTrackedMilestones = (store: AppState) => {
  const { store: milestones = {} } = store.milestones || {};

  return Object.keys(milestones);
};

export const getSelectedMilestone = (store: AppState) => {
  const { store: milestones = {}, selectedMilestone } = store.milestones || {};

  if (selectedMilestone) {
    return milestones[selectedMilestone];
  }

  return null;
};

export const getPreviousMilestone = (store: AppState) => {
  const { store: milestones = {}, selectedMilestone = null } = store.milestones || {};

  if (selectedMilestone) {
    const keys = Object.keys(milestones);
    const index = keys.findIndex((x) => x === selectedMilestone);
    const prevIndex = index - 1;

    // Check if we have any previous milestones to compare to
    if (keys.length > 1 && prevIndex > -1) {
      return milestones[keys[prevIndex]];
    }
  }

  return null;
};

export const getMergedMRsForMilestone = (store: AppState) => {
  const { store: milestones = {}, selectedMilestone = null } = store.milestones || {};

  if (selectedMilestone) {
    return milestones[selectedMilestone]?.mergeRequests.filter((x) => x.merged_at) || [];
  }

  return [];
};

// === Pinned === //

export const getPinnedItemLoading = (state: AppState) => {
  return state.pinned?.itemLoading || false;
};

export const getPinnedItem = (state: AppState, id: number) => {
  const { store = {} } = state.pinned || {};

  return store[id];
};

export const getPinnedIssues = (state: AppState) => {
  const { store = {} } = state.pinned || {};

  return Object.keys(store).map((x) => store[x as any]);
};

export const getPinnedListMenu = (state: AppState) => {
  const { global = null } = state.menu.menusForProjects;

  if (global) {
    const { tree = {} } = global.pinned;

    return Object.keys(tree)
      .sort()
      .map((x) => ({
        label: tree[x].label,
        count: Object.keys(tree[x].tree || {}).length,
        items: Object.keys(tree[x].tree || {}).map((i) => i),
      }));
  }

  return [];
};

export const getSelectedPinnedItem = (state: AppState) => {
  const { selectedItem, store = {} } = state.pinned || {};

  if (selectedItem) {
    return store[selectedItem];
  }

  return null;
};

// === Merge Requests === //

export const getMergeRequestLoading = (state: AppState) => {
  return state.mergeRequests?.isLoading || false;
};

export const getMergeRequests = (state: AppState) => {
  const { selectedProject } = state.menu;

  return state.mergeRequests?.map[selectedProject] || {};
};

export const getMergeRequestById = (state: AppState, mergeRequestId: number) => {
  return state.mergeRequests?.store[mergeRequestId];
};

export const getSelectedMergeRequest = (state: AppState) => {
  const { selectedMergeRequest, store = {} } = state.mergeRequests || {};

  return store[selectedMergeRequest];
};
