import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://gitlab.com/api/v4/',
});

(window as any).ax = instance;

type Pagination = {
  'x-next-page': string | number;
  'x-page': string | number;
  'x-total-pages': string | number;
};

// Default amount of entries to fetch
const PER_PAGE = 100;

/**
 * A helper function that will perform the supplied GET request and then perform
 * follow up requests for any pagination, resulting in all the available data
 * being returned.
 * @param request The URL of the API request
 * @param params Additional params for the request
 */
export const fetchAllThroughPagination = async <T>(request: string, params: any = {}) => {
  let result: T[] = [];

  const { data, headers } = await instance.get(request, {
    params: { per_page: PER_PAGE, ...params },
  });

  if (data && data.length && headers) {
    // Store first page of results
    result = data;

    let {
      'x-page': currPage,
      'x-next-page': start,
      'x-total-pages': total,
    } = headers as Pagination;
    currPage = parseInt(currPage as string);
    start = parseInt(start as string);
    total = parseInt(total as string);

    if (isNaN(currPage) || isNaN(start) || isNaN(total)) {
      return result;
    }

    if (currPage < total) {
      const promisesCollection = [];

      for (let i = currPage; i < total + 1; i++) {
        promisesCollection.push(makeRequest<T>(request, i, params));
      }

      return await (await Promise.all(promisesCollection)).flat();
    }
  }

  return result;
};

/**
 * A small helper function that wraps the request.
 * @param request The request
 * @param i The page to request
 * @param params Any additional params
 */
const makeRequest = async <T>(request: string, i: number, params: any = {}) => {
  const { data } = await instance.get<T>(request, {
    params: { per_page: PER_PAGE, page: i, ...params },
  });

  return data;
};

export default instance;
