import React from 'react';
import { Issue, MergeRequest } from './models';

/**
 * I've not used context for a long time but holy moly this looks ridiculous for what I want to do.
 * I'm NOT sold on this or context at _all_ right now. The reason I'm not using Redux for this is
 * because I don't want this state to be persisted over reloads - I thought context would be a
 * simple way to achieve this but this looks too much.
 *
 * // TODO: Figure out how we can use Redux for this and not care about persistance or something.
 */

interface AppContextState {
  params: {
    tabIndex: number;
    isUpdating: boolean;
  };
  workflowModal: {
    isOpen: boolean;
    issue: Issue | null;
    mergeRequest: MergeRequest | null;
    issueId: number | null;
  };
  milestoneModal: {
    isOpen: boolean;
  };
  pinnedModal: {
    isOpen: boolean;
  };
}

export const initialState: AppContextState = {
  params: {
    tabIndex: 0,
    isUpdating: false,
  },
  workflowModal: {
    isOpen: false,
    issue: null,
    mergeRequest: null,
    issueId: null,
  },
  milestoneModal: {
    isOpen: false,
  },
  pinnedModal: {
    isOpen: false,
  },
};

const reducer = (state: AppContextState, action: any) => {
  switch (action.type) {
    case 'reset':
      return initialState;
    case 'set-params':
      return { ...state, params: { ...action.payload } };
    case 'toggle-workflow-modal':
      return { ...state, workflowModal: { ...action.payload } };
    case 'toggle-milestone-modal':
      return { ...state, milestoneModal: { ...action.payload } };
    case 'toggle-new-pinned-modal':
      return { ...state, pinnedModal: { ...action.payload } };
    default:
      return state;
  }
};

export const AppContextComponent = React.createContext<{ state: AppContextState; dispatch?: any }>({
  state: initialState,
});

export const AppContext: React.FC = (props) => {
  const [state, dispatch] = React.useReducer(reducer, initialState);
  const value = { state, dispatch };

  return (
    <AppContextComponent.Provider value={value}>{props.children}</AppContextComponent.Provider>
  );
};
