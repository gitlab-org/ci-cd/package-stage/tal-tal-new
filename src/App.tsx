import { CSSReset, ThemeProvider } from '@chakra-ui/core';
import React, { useEffect } from 'react';
import { Provider, useDispatch, useSelector } from 'react-redux';
import { Route, Router, Switch } from 'react-router-dom';
import { PersistGate } from 'redux-persist/integration/react';
import { AppContext } from './app-context';
import { AddMilestoneModal } from './components/modals/add-milestone';
import { PinnedModal } from './components/modals/pinned-issue';
import { WorkflowModal } from './components/modals/workflow-modal';
import { PrivateRoute } from './components/private-route';
import history from './history';
import { persistor, store } from './store';
import axios from './store/axios';
import { buildSearchIndex } from './store/search';
import { getAccessToken, getAllIssues } from './store/selectors';
import { makoTheme } from './theme';
import { AboutView } from './views/about';
import { CallbackView } from './views/callback';
import { HelpView } from './views/help';
import { IssueDetailView } from './views/issue-detail';
import { MainView } from './views/main';
import { MergeRequestDetailView } from './views/merge-request-detail';
import { MergeRequestsView } from './views/merge-requests';
import { MilestoneDetailView } from './views/milestone-detail';
import { MilestonesView } from './views/milestones';
import { PinnedView } from './views/pinned';
import { PinnedDetailView } from './views/pinned-detail';
import { WelcomeView } from './views/welcome';

const InnerApp = () => {
  const dispatch = useDispatch();

  const allIssues = useSelector(getAllIssues);
  const accessToken = useSelector(getAccessToken);
  axios.defaults.headers.common['Authorization'] = `Bearer ${accessToken || ''}`;

  useEffect(() => dispatch(buildSearchIndex(allIssues)), [allIssues, dispatch]);

  return (
    <ThemeProvider theme={makoTheme}>
      <CSSReset />
      <Router history={history}>
        <Switch>
          <PrivateRoute exact path="/">
            <Route component={MainView} />
          </PrivateRoute>

          <PrivateRoute exact path="/issue">
            <Route component={IssueDetailView} />
          </PrivateRoute>

          <PrivateRoute exact path="/merge-requests">
            <Route component={MergeRequestsView} />
          </PrivateRoute>

          <PrivateRoute exact path="/merge-requests/detail">
            <Route component={MergeRequestDetailView} />
          </PrivateRoute>

          <PrivateRoute exact path="/milestones">
            <Route component={MilestonesView} />
          </PrivateRoute>

          <PrivateRoute exact path="/milestones/detail">
            <Route component={MilestoneDetailView} />
          </PrivateRoute>

          <PrivateRoute exact path="/pinned">
            <Route component={PinnedView} />
          </PrivateRoute>

          <PrivateRoute exact path="/pinned/item">
            <Route component={PinnedDetailView} />
          </PrivateRoute>

          <PrivateRoute exact path="/about">
            <Route component={AboutView} />
          </PrivateRoute>

          <PrivateRoute exact path="/help">
            <Route component={HelpView} />
          </PrivateRoute>

          <Route exact path="/welcome" component={WelcomeView} />
          <Route exact path="/callback" component={CallbackView} />
        </Switch>

        <WorkflowModal />
        <PinnedModal />
        <AddMilestoneModal />
      </Router>
    </ThemeProvider>
  );
};

export const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <AppContext>
          <InnerApp />
        </AppContext>
      </PersistGate>
    </Provider>
  );
};
