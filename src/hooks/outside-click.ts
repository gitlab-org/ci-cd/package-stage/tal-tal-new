import { useEffect } from 'react';

/**
 * Hook that alerts clicks outside of the passed ref
 */
export function useOutsideClick(
  ref: React.RefObject<HTMLInputElement>,
  callback: (event: any) => void,
  includeEscape: boolean = true,
) {
  /**
   * Alert if clicked on outside of element
   */
  function handleClickOutside(event: any) {
    if (ref.current && !ref.current.contains(event.target)) {
      callback(event);
    }
  }

  function handleEscapePress(event: KeyboardEvent) {
    if (event.key === 'Escape') {
      callback(event);
    }
  }

  useEffect(() => {
    // Bind the event listener
    document.addEventListener('mousedown', handleClickOutside);

    if (includeEscape) {
      document.addEventListener('keydown', handleEscapePress);
    }

    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener('mousedown', handleClickOutside);

      if (includeEscape) {
        document.removeEventListener('keydown', handleClickOutside);
      }
    };
  });
}
