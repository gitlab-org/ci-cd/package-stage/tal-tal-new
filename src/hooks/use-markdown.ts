import * as Showdown from 'showdown';

type Opts = {
  imageUrl?: string;
};

export function useMarkdownToHtml(textToParse: string, opts: Opts = {}) {
  const converter = new Showdown.Converter({
    tables: true,
    simplifiedAutoLink: true,
    strikethrough: true,
    tasklists: true,
    openLinksInNewWindow: true,
    emoji: true,
  });

  if (opts.imageUrl) {
    // TODO: review and actually figure out how these extensions work.
    // Why is the filter needed for the listener to work?
    // Can we remove the listenter and use just the filter?
    const gitLabImages: Showdown.ShowdownExtension = {
      type: 'output',
      filter(text, converter, options) {
        // No idea why this is needed
        return text;
      },
      listeners: {
        'images.after'(evtName, text, converter, options, globals) {
          if (text.includes('<img') && text.includes('src="/')) {
            return text.replace('<img src="/', `<img src="${opts.imageUrl}`);
          }

          return text;
        },
      },
    };

    converter.addExtension(gitLabImages);
  }

  return converter.makeHtml(textToParse);
}
