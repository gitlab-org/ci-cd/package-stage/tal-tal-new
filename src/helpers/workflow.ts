import { Issue, MergeRequest, Scopes, Workflow, workflowPriority } from '../models';

type Conditions = {
  merged: number;
  production: number;
  productionOrNone: number;
};

const hasWorkflow = (labels: string[], workflow: Workflow) => {
  return Boolean(labels.find((x) => x === `${Scopes.WORKFLOW}${workflow}`));
};

/**
 * Returns the highest priority assigned workflow label. If no priority is found,
 * it returns the first workflow label.
 * @param workflowLabels A list of workflow labels
 */
export const highestPriorityWorkflow = (labels: string[]): Workflow => {
  const workflowLabels = labels
    // Find all workflow labels
    .filter((x) => x.toLowerCase().includes(Scopes.WORKFLOW))
    // Remove workflow:: string and trim
    .map((x) => x.replace(Scopes.WORKFLOW, '').trim());

  for (let p of workflowPriority) {
    for (let l of workflowLabels) {
      if (p.trim().toLowerCase() === l.trim().toLowerCase()) {
        return l as Workflow;
      }
    }
  }

  // None found matching priority, return the first or default
  return (workflowLabels[0] as Workflow) || Workflow.NONE;
};

/**
 * Attempts to determine if an issue is ready to be marked as in production.
 * It follows an opinionated set of checks to determine if this is true:
 *   1. If there are no merge requests, the issue cannot be ready
 *   2. If all merge requests are marked as production, then this can be too
 *   3. The merge request check excludes MRs with no workflow (to account for later MRs such as release posts, etc)
 *   4. OR if all merge requests are merged, regardless of workflow
 * @param issue Issue to check
 */
export const possiblyProductionReady = (issue: Issue) => {
  if (hasWorkflow(issue.labels, Workflow.PRODUCTION)) {
    return false;
  }

  // Issue can't be ready if there are no MR's
  if (issue.mergeRequests?.length) {
    const conditions = buildConditions(issue.mergeRequests);

    const mrCount = issue.mergeRequests.length;

    if (conditions.production === mrCount) {
      return true;
    }

    if (
      conditions.merged === mrCount ||
      conditions.production + conditions.productionOrNone === mrCount
    ) {
      return true;
    }

    if (conditions.productionOrNone === mrCount) {
      return true;
    }
  }

  return false;
};

export const isInVerification = (issue: Issue) => {
  if (
    hasWorkflow(issue.labels, Workflow.VERIFICATION) ||
    hasWorkflow(issue.labels, Workflow.PRODUCTION)
  ) {
    return false;
  }

  if (issue.mergeRequests?.length) {
    const conditions = buildConditions(issue.mergeRequests);

    const mrCount = issue.mergeRequests.length;

    if (conditions.merged + conditions.production - conditions.productionOrNone === mrCount) {
      return true;
    }
  }

  return false;
};

const buildConditions = (mergeRequests: MergeRequest[]): Conditions => {
  const conditions: any = {
    merged: 0,
    production: 0,
    productionOrNone: 0,
  };

  // Only do one loop / lookup
  mergeRequests.forEach((x) => {
    if (x.merged_at) {
      ++conditions.merged;
    }

    if (x.workflow === Workflow.PRODUCTION) {
      ++conditions.production;
    }

    if (x.workflow === Workflow.NONE) {
      ++conditions.productionOrNone;
    }
  });

  return conditions;
};
