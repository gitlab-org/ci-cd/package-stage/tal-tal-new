import moment from 'moment';

export const fromNow = (ts: number | string) => {
  return moment(ts).fromNow();
};
