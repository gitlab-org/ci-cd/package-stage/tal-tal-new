import { Reference } from '../models';

export const projectUrlFromReference = (reference: Reference) => {
  const issue = reference.full.lastIndexOf('#');
  const mr = reference.full.lastIndexOf('!');

  const lastIndex = issue !== -1 ? issue : mr !== -1 ? mr : -1;
  const uri = reference.full.substring(0, lastIndex);

  return `https://gitlab.com/${uri}`;
};
