import { MergeRequest } from '../models';

export const MergeRequestStatus = {
  CLOSED: ['Closed', 'gray'],
  MERGED: ['Merged', 'purple'],
  OPEN: ['Open', 'green'],
};

export const getMergeRequestStatus = (mergeRequest: MergeRequest) => {
  const isClosed = Boolean(mergeRequest.closed_at && !mergeRequest.merged_at);
  const isMerged = Boolean(mergeRequest.merged_at);

  if (isClosed) {
    return MergeRequestStatus.CLOSED;
  }

  if (isMerged) {
    return MergeRequestStatus.MERGED;
  }

  return MergeRequestStatus.OPEN;
};

export const getReviewItemsCountAndUnresolved = ({ reviewItems }: MergeRequest) => {
  if (!reviewItems) {
    return [];
  }

  const resolved = reviewItems.filter((x) => !x.resolved).length;

  return [reviewItems.length, resolved];
};
