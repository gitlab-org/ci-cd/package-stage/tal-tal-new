export const capitalize = (s: string) => {
  return s[0].toUpperCase() + s.slice(1);
};

export const sortByDescending = (a: any, b: any, prop: string): number => {
  if (a[prop] > b[prop]) {
    return -1;
  } else if (b[prop] < a[prop]) {
    return 1;
  }

  return 0;
};
