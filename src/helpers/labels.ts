import { Issue } from '../models';

const PackagePriorityLabel = 'Package:P';

/**
 * Extracts package priority labels from an issue.
 * @param issue Issue to examine
 */
export const findPriorityLabels = (issue: Issue) => {
  if (issue.labels && issue.labels.length) {
    return issue.labels
      .filter(x => x.includes(PackagePriorityLabel))
      .map(x => x.replace(PackagePriorityLabel, 'P'))
      .sort();
  }

  return null;
};
