import iziToast from 'izitoast';
import React from 'react';
import ReactDOM from 'react-dom';
import '../node_modules/izitoast/dist/css/iziToast.min.css';
import { App } from './App';
import './assets/scss/index.scss';

ReactDOM.render(<App />, document.getElementById('root'));

iziToast.settings({
  position: 'topRight',
});
