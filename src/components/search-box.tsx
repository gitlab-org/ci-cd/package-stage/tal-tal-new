import {
  Box,
  Input,
  Popover,
  PopoverArrow,
  PopoverBody,
  PopoverContent,
  PopoverHeader,
  PopoverTrigger,
  Stack,
  Text,
} from '@chakra-ui/core';
import moment from 'moment';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { useOutsideClick } from '../hooks';
import { Issue } from '../models';
import { issueActions } from '../store/actions/issues';
import { search } from '../store/search';
import { getAllIssues } from '../store/selectors';

/**
 * // TODO: Build this properly
 *
 * This search component is utter trash. First, the Popover component from
 * Chakra leaves _a lot_ to be desired. It's just not at all suited for what I
 * want to do here.
 *
 * Also, we're only supporting issues as search results at the min, so when we
 * want to extend this, the component will break. Another reason to redo it.
 *
 * Finally, the actual search is borked because it doesn't like reading from the
 * store for some reason. This works fine in other projects but there's
 * something special about this project (of course there is, no two Javascript
 * projects can ever quite work the same) so it needs looking at.
 */

const ExludedNodes: string[] = ['input', 'select', 'option', 'textarea'];

export const SearchBox: React.FC = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const ref = useRef(null);

  const allIssues = useSelector(getAllIssues);

  let debounce: NodeJS.Timeout;
  const [isOpen, setIsOpen] = useState(false);
  const [results, setResults] = useState<Issue[]>([]);

  // TODO: Consider abstracting this into a hook or some kind of manager
  const onKeyPress = (e: KeyboardEvent) => {
    if (
      e.target &&
      !ExludedNodes.includes((e.target as HTMLElement).nodeName.toLowerCase()) &&
      (e.key === 's' || e.key === 'p')
    ) {
      const searchEntry = document.getElementById('search-entry') as HTMLInputElement;
      searchEntry?.focus();
    }

    if (e.key === 'Escape') {
      if (document.activeElement && document.activeElement.id === 'search-entry') {
        const searchEntry = document.getElementById('search-entry') as HTMLInputElement;
        searchEntry?.blur();
      }
    }
  };

  useOutsideClick(ref, () => setIsOpen(false), false);

  useEffect(() => {
    document.addEventListener('keyup', onKeyPress);

    return () => {
      document.removeEventListener('keyup', onKeyPress);
    };
  }, []);

  const onSearchInput = (e: React.FormEvent<HTMLInputElement>) => {
    clearTimeout(debounce);
    const { value } = e.currentTarget;

    if (value) {
      debounce = setTimeout(() => {
        setResults(search(value, allIssues));

        setIsOpen(true);
      }, 500);
    } else {
      setIsOpen(false);
      setResults([]);
    }
  };

  const onSearchResultClick = (id: number) => {
    dispatch(issueActions.setSelectedIssue(id));
    history.push('issue');
    setIsOpen(false);
    setResults([]);
  };

  return (
    <div ref={ref} style={{ marginRight: '0.5rem' }}>
      <Popover isOpen={isOpen} placement="bottom">
        <PopoverTrigger>
          <Input
            type="search"
            placeholder="Search"
            bg="gray.700"
            borderColor="gray.700"
            color="gray.300"
            minW="10px"
            size="sm"
            id="search-entry"
            onChange={onSearchInput}
          />
        </PopoverTrigger>
        <PopoverContent zIndex={4}>
          <PopoverArrow />
          <PopoverHeader pt={4} fontSize="sm" fontWeight="bold" border="0">
            Search Results
          </PopoverHeader>
          <PopoverBody fontSize="sm">
            <Stack>
              {results.length ? (
                results.slice(0, 5).map((x, index) => (
                  <Box
                    key={x.id}
                    borderBottomWidth={index < results.length - 1 ? '1px' : ''}
                    py={2}
                    className="issue with-hover"
                    onClick={() => onSearchResultClick(x.id)}
                  >
                    <Text fontWeight="bold">{x.title}</Text>
                    <Text color="gray.500" fontWeight="normal">
                      #{x.id} - Opened {moment(x.created_at).fromNow()} by {x.author.name}
                    </Text>
                  </Box>
                ))
              ) : (
                <Text>No results found.</Text>
              )}
            </Stack>
          </PopoverBody>
        </PopoverContent>
      </Popover>
    </div>
  );
};
