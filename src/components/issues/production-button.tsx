import {
  Button,
  ButtonGroup,
  Popover,
  PopoverArrow,
  PopoverBody,
  PopoverContent,
  PopoverFooter,
  PopoverHeader,
  PopoverTrigger,
  Text,
  useDisclosure,
} from '@chakra-ui/core';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { capitalize } from '../../helpers';
import { useOutsideClick } from '../../hooks';
import { Issue } from '../../models';
import { updateWorkflow } from '../../store/actions/issues';
import { isUpdateLoading } from '../../store/selectors';

interface Props {
  issue: Issue;
}

const getDetails = (issue: Issue) => {
  if (issue.possiblyReadyForProduction) {
    return ['Production?', 'production', 'green'];
  }

  if (issue.possiblyVerificationReady) {
    return ['Verification?', 'verification', 'purple'];
  }

  return [];
};

export const ProductionButton: React.FC<Props> = ({ issue }) => {
  const dispatch = useDispatch();

  const containerRef = useRef(null);
  const initialRef = useRef(null);

  const [updateApplied, setUpdateApplied] = useState(false);

  const [title, status, variantColor] = getDetails(issue);
  const { isOpen, onOpen, onClose } = useDisclosure();

  const isLoading = useSelector(isUpdateLoading);

  useOutsideClick(containerRef, onClose);

  useEffect(() => {
    if (updateApplied && !isLoading) {
      setUpdateApplied(false);
      onClose();
    }
  }, [isLoading, onClose, updateApplied]);

  if (!title) {
    return null;
  }

  const onUpdateClick = () => {
    setUpdateApplied(true);
    dispatch(updateWorkflow(issue, status));
  };

  return (
    <div ref={containerRef}>
      <Popover initialFocusRef={initialRef} isOpen={isOpen}>
        <PopoverTrigger>
          <Button
            leftIcon="question"
            variantColor={variantColor}
            variant="outline"
            size="xs"
            mr="2"
            onClick={onOpen}
          >
            {title}
          </Button>
        </PopoverTrigger>
        <PopoverContent zIndex={4}>
          <PopoverArrow />
          <PopoverHeader pt={4} fontSize="sm" fontWeight="bold" border="0">
            {capitalize(status)} ready?
          </PopoverHeader>
          <PopoverBody fontSize="sm">
            <Text>
              This issuse matches the conditions for possibly being {status} ready. Do you want to
              change the workflow to <strong>{status}</strong> now?
            </Text>

            <Text mt={2}>
              <strong>Note:</strong> this will only change the issue workflow.
            </Text>
          </PopoverBody>
          <PopoverFooter d="flex" alignItems="center" justifyContent="flex-end" border="0" pb={4}>
            <ButtonGroup size="xs">
              <Button variant="ghost" onClick={onClose} ref={initialRef} isDisabled={isLoading}>
                Cancel
              </Button>
              <Button variantColor="green" isLoading={isLoading} onClick={onUpdateClick}>
                Yes
              </Button>
            </ButtonGroup>
          </PopoverFooter>
        </PopoverContent>
      </Popover>
    </div>
  );
};
