import { Flex, Link, Tab, TabList, TabPanel, TabPanels, Tabs, Text } from '@chakra-ui/core';
import React, { useContext, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { AppContextComponent } from '../../app-context';
import { fromNow } from '../../helpers';
import { AppState } from '../../store/reducers';
import { getSelectedIssue, isIssueSkipped } from '../../store/selectors';
import { TabCount } from '../tab-count';
import { AsyncUpdatePanel } from './async/async-update-panel';
import { IssueActions } from './issue-actions';
import { IssueDescription } from './issue-description';
import { IssueTopButtons } from './issue-top-buttons';
import { MergeRequestList } from './merge-requests/merge-request-list';

const tabPanelProps = {
  backgroundColor: 'background.light',
  flex: 1,
  p: 4,
};

export const IssueDisplay = () => {
  const issue = useSelector(getSelectedIssue);
  const isSkipped = useSelector<AppState, boolean>((s) =>
    isIssueSkipped(s, s.issues.selectedIssue as number),
  );
  const { state: contextState, dispatch: contextDispatch } = useContext(AppContextComponent);
  const [tabIndex, setTabIndex] = useState(contextState?.params.tabIndex || 0);
  const [isUpdating, setIsUpdating] = useState(contextState?.params.isUpdating || false);

  useEffect(() => contextDispatch({ type: 'reset' }), [contextDispatch]);

  if (!issue) {
    return null;
  }

  const asyncCount = issue.asyncUpdates?.length;
  const mrCount = issue.mergeRequests?.length;

  const handleTabsChange = (index: number) => setTabIndex(index);

  const addUpdate = () => {
    setIsUpdating(true);
    handleTabsChange(1);
  };

  const mergeRequestAction = () => {
    handleTabsChange(2);
  };

  const onUpdatingChange = () => {
    setIsUpdating(false);
  };

  return (
    <Flex flex={1} direction="column">
      <Flex justifyContent="space-between" px={4}>
        <Flex direction="column">
          <Text fontSize="xl" fontWeight="bold">
            <Link href={issue.web_url} isExternal>
              {issue.title}
            </Link>
          </Text>
          <Text color="gray.500">
            #{issue.iid} Opened {fromNow(issue.created_at)} by {issue.author.name}. Last updated{' '}
            {fromNow(issue.updated_at)}.
          </Text>
        </Flex>

        <IssueTopButtons issue={issue} />
      </Flex>

      <IssueActions
        issue={issue}
        px={4}
        my={4}
        addUpdateAction={addUpdate}
        mergeRequestAction={mergeRequestAction}
        isSkipped={isSkipped}
      />

      <Flex justifyContent="space-between" flex={1}>
        <Tabs
          as={Flex}
          flex={1}
          flexDirection="column"
          index={tabIndex}
          onChange={handleTabsChange}
        >
          <TabList>
            <Tab className="issue-tabs">Detail</Tab>
            <Tab>
              Async
              <TabCount count={asyncCount} />
            </Tab>
            <Tab>
              Merge Requests
              <TabCount count={mrCount} />
            </Tab>
          </TabList>

          <TabPanels as={Flex} flex={1}>
            <TabPanel {...tabPanelProps}>
              <IssueDescription item={issue} />
            </TabPanel>
            <TabPanel {...tabPanelProps}>
              <AsyncUpdatePanel
                issue={issue}
                isUpdating={isUpdating}
                onUpdatingChange={onUpdatingChange}
              />
            </TabPanel>
            <TabPanel {...tabPanelProps}>
              <MergeRequestList issue={issue} />
            </TabPanel>
          </TabPanels>
        </Tabs>
      </Flex>
    </Flex>
  );
};
