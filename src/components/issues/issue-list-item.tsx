import { Box, Flex, IconButton, Text } from '@chakra-ui/core';
import cn from 'classnames';
import React, { useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { AppContextComponent } from '../../app-context';
import { fromNow } from '../../helpers';
import { Issue } from '../../models';
import { issueActions } from '../../store/actions/issues';
import { AppState } from '../../store/reducers';
import { getIssueById, isIssueSkipped } from '../../store/selectors';
import { Icons } from '../../theme/icons';
import { IssueActions } from './issue-actions';

interface Props {
  id: number;
}

export const IssueListItem: React.FC<Props> = ({ id }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const issue = useSelector<AppState, Issue | undefined>((s) => getIssueById(s, id));
  const isSkipped = useSelector<AppState, boolean>((s) => isIssueSkipped(s, id));
  const { dispatch: contextDispatch } = useContext(AppContextComponent);

  if (!issue) {
    return null;
  }

  const onIssueClick = () => {
    dispatch(issueActions.setSelectedIssue(issue.id));
    history.push('issue');
  };

  const addUpdate = () => {
    // Preload the selected tab and is updating to go straight to the editor
    contextDispatch({ type: 'set-params', payload: { tabIndex: 1, isUpdating: true } });
    dispatch(issueActions.setSelectedIssue(issue.id));
    history.push('issue');
  };

  const mergeRequestAction = () => {
    // Preload the selected tab and is updating to go straight to the editor
    contextDispatch({ type: 'set-params', payload: { tabIndex: 2 } });
    dispatch(issueActions.setSelectedIssue(issue.id));
    history.push('issue');
  };

  const goToIssue = (e: React.MouseEvent) => {
    e.stopPropagation();
    window.open(issue.web_url, '_blank');
  };

  return (
    <Box
      className={cn('issue with-hover', { 'is-skipped': isSkipped || issue.updateCompleted })}
      p={5}
      borderWidth="1px"
      borderRadius={4}
      borderColor="gray.400"
      mb={2}
      backgroundColor={issue.updateCompleted ? 'green.50' : 'white'}
      onClick={onIssueClick}
    >
      <Flex justifyContent="space-between">
        <Flex direction="column">
          <Text fontWeight="bold" fontSize="md">
            {issue.title}
          </Text>
          <Text color="gray.500" fontSize="sm">
            #{issue.iid} - Opened {fromNow(issue.created_at)} by{' '}
            <strong>{issue.author.name}</strong>. Last updated {fromNow(issue.updated_at)}.
          </Text>
        </Flex>

        <Flex>
          <IconButton
            aria-label="View on GitLab"
            icon={Icons.EXTERNAL_LINK}
            variant="ghost"
            size="sm"
            onClick={goToIssue}
          />
        </Flex>
      </Flex>

      <IssueActions
        issue={issue}
        hideMilestone
        mt={2}
        addUpdateAction={addUpdate}
        mergeRequestAction={mergeRequestAction}
        isSkipped={isSkipped}
      />
    </Box>
  );
};
