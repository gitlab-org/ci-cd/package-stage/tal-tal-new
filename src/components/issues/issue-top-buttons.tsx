import { Flex, IconButton } from '@chakra-ui/core';
import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { projectUrlFromReference } from '../../helpers';
import { Issue } from '../../models';
import { issueActions } from '../../store/actions/issues';
import { Icons } from '../../theme/icons';

interface Props {
  issue: Issue;
}

export const IssueTopButtons: React.FC<Props> = ({ issue }) => {
  const dispatch = useDispatch();
  const history = useHistory();

  const goToIssue = () => {
    window.open(issue.web_url, '_blank');
  };

  const onCloseClick = () => {
    dispatch(issueActions.setSelectedIssue(null));
    history.push('/');
  };

  const goToProject = () => {
    const url = projectUrlFromReference(issue.references);
    window.open(url, '_blank');
  };

  return (
    <Flex>
      <IconButton
        aria-label="View on GitLab"
        icon={Icons.EXTERNAL_LINK}
        variant="ghost"
        size="sm"
        onClick={goToIssue}
      />
      <IconButton
        aria-label="Go to Project"
        icon={Icons.HOME}
        variant="ghost"
        size="sm"
        onClick={goToProject}
      />
      <IconButton
        aria-label="Close"
        icon="small-close"
        variant="ghost"
        size="sm"
        onClick={onCloseClick}
      />
    </Flex>
  );
};
