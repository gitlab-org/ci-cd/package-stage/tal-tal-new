import { BoxProps, Button, Flex, Tag, TagIcon, TagLabel } from '@chakra-ui/core';
import React, { MouseEvent, useContext } from 'react';
import { useDispatch } from 'react-redux';
import { AppContextComponent } from '../../app-context';
import { findPriorityLabels } from '../../helpers';
import { Issue } from '../../models';
import { issueActions } from '../../store/actions/issues';
import { Icons } from '../../theme/icons';
import { WorkflowButton } from '../workflow-button';
import { ProductionButton } from './production-button';

interface Props extends BoxProps {
  issue: Issue;
  hideMilestone?: boolean;
  addUpdateAction: () => void;
  mergeRequestAction: () => void;
  isSkipped: boolean;
}

export const IssueActions: React.FC<Props> = ({
  issue,
  hideMilestone,
  addUpdateAction,
  mergeRequestAction,
  isSkipped,
  ...props
}) => {
  const dispatch = useDispatch();
  const { dispatch: contextDispatch } = useContext(AppContextComponent);
  const priorities = findPriorityLabels(issue);

  const preventPropagation = (e: React.MouseEvent) => {
    e.stopPropagation();
  };

  const addUpdateClick = (e: MouseEvent) => {
    addUpdateAction();
  };

  const mergeRequestClick = (e: MouseEvent) => {
    mergeRequestAction();
  };

  const skipUpdate = () => {
    dispatch(issueActions.skipIssueUpdate(issue.id));
  };

  const changeWorkflow = () => {
    contextDispatch({ type: 'toggle-workflow-modal', payload: { isOpen: true, issue } });
  };

  return (
    <Flex {...props} flexWrap="wrap" onClick={preventPropagation}>
      <ProductionButton issue={issue} />

      {issue.milestone && hideMilestone !== true && (
        <Tag size="sm" variantColor="blue" rounded="full" mr="2">
          <TagIcon icon={Icons.CLOCK} size="12px" />
          <TagLabel>{issue.milestone.title}</TagLabel>
        </Tag>
      )}

      {!issue.updateCompleted && !isSkipped && (
        <Button
          leftIcon="warning-2"
          size="xs"
          variantColor="red"
          variant="outline"
          mr="2"
          className="void-button"
        >
          Async update required
        </Button>
      )}

      {isSkipped && (
        <Button
          leftIcon={Icons.CLEAR}
          size="xs"
          variantColor="gray"
          variant="outline"
          mr="2"
          className="void-button"
        >
          Update skipped
        </Button>
      )}

      <Button
        leftIcon={Icons.PENCIL_SQUARE}
        variantColor={issue.updateCompleted || isSkipped ? 'gray' : 'green'}
        variant={issue.updateCompleted || isSkipped ? 'ghost' : 'solid'}
        size="xs"
        mr="2"
        onClick={addUpdateClick}
      >
        Add Update
      </Button>

      {issue.updateCompleted ? null : (
        <Button
          leftIcon={Icons.ANGLE_DOUBLE_RIGHT}
          variantColor="gray"
          size="xs"
          mr="2"
          onClick={skipUpdate}
          isDisabled={issue.updateCompleted || isSkipped}
        >
          Skip Update
        </Button>
      )}

      <WorkflowButton workflow={issue.workflow} onClick={changeWorkflow} />

      <Button
        leftIcon={Icons.MERGE_REQUEST}
        variantColor="gray"
        variant="ghost"
        size="xs"
        mr="2"
        onClick={mergeRequestClick}
      >
        {issue.mergeRequests?.length}
      </Button>

      {issue.weight ? (
        <Button
          leftIcon={Icons.WEIGHT}
          variantColor="gray"
          variant="ghost"
          size="xs"
          mr="2"
          className="void-button"
        >
          {issue.weight}
        </Button>
      ) : null}

      {priorities &&
        priorities.map((x, index) => (
          <Tag variantColor="purple" rounded="full" size="sm" key={index}>
            {x}
          </Tag>
        ))}
    </Flex>
  );
};
