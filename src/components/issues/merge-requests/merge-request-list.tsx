import { Flex, Text } from '@chakra-ui/core';
import React from 'react';
import { Issue } from '../../../models';
import { EmptyState } from '../../empty-state';
import { MergeRequestItem } from './merge-request-item';

interface Props {
  issue: Issue;
}

export const MergeRequestList: React.FC<Props> = ({ issue }) => {
  const mrMap = issue.mergeRequestMap || {};

  return (
    <Flex>
      {issue.mergeRequests && issue.mergeRequests.length ? (
        <Flex direction="column" w="100%">
          {Object.keys(mrMap).map(x => (
            <Flex direction="column" key={x}>
              <Text fontWeight="bold" mb={2} className="title">
                {x}
              </Text>
              {mrMap[x].map(x => (
                <MergeRequestItem mergeRequest={x} key={x.id} issueId={issue.id} />
              ))}
            </Flex>
          ))}
        </Flex>
      ) : null}

      {!issue.mergeRequests ||
        (issue.mergeRequests?.length === 0 && (
          <EmptyState
            title="No Merge Requests Found"
            text="No merge requests for this issue were found."
            illustration="merge-request"
          />
        ))}
    </Flex>
  );
};
