import { Avatar, AvatarGroup, Box, Flex, IconButton, Link, Text, Tooltip } from '@chakra-ui/core';
import React from 'react';
import { fromNow } from '../../../helpers';
import { MergeRequest } from '../../../models';
import { Icons } from '../../../theme/icons';
import { MergeRequestActions } from '../../merge-requests/merge-request-actions';

interface Props {
  mergeRequest: MergeRequest;
  issueId: number;
}

export const MergeRequestItem: React.FC<Props> = ({ mergeRequest, issueId }) => {
  const isMerged = Boolean(mergeRequest.merged_at);

  const copyReference = () => {
    navigator.clipboard.writeText(mergeRequest.references.full);
  };

  const goToMergeRequest = () => {
    window.open(mergeRequest.web_url, '_blank');
  };

  return (
    <Box
      p={5}
      borderWidth="1px"
      borderRadius={4}
      borderColor="gray.400"
      mb={2}
      backgroundColor={isMerged ? 'green.50' : 'white'}
    >
      <Flex justifyContent="space-between">
        <Flex mr={2}>
          <AvatarGroup size="sm" max={3}>
            {mergeRequest.assignees.map((x) => (
              <Avatar name={x.name} src={x.avatar_url} key={x.id} />
            ))}
          </AvatarGroup>
        </Flex>

        <Flex direction="column" flex={1}>
          <Text fontWeight="bold" fontSize="md">
            <Link href={mergeRequest.web_url} isExternal>
              {mergeRequest.title}
            </Link>
          </Text>
          <Text color="gray.500" fontSize="sm">
            !{mergeRequest.iid} - Created {fromNow(mergeRequest.created_at)} by{' '}
            <strong>{mergeRequest.author.name}</strong>. Last updated{' '}
            {fromNow(mergeRequest.updated_at)}.
          </Text>
        </Flex>

        <Flex>
          <Tooltip aria-label="Copy reference" label="Copy reference" placement="bottom">
            <IconButton
              aria-label="Copy reference"
              icon="copy"
              variant="ghost"
              size="sm"
              onClick={copyReference}
            />
          </Tooltip>

          <IconButton
            aria-label="View on GitLab"
            icon={Icons.EXTERNAL_LINK}
            variant="ghost"
            variantColor={isMerged ? 'green' : 'gray'}
            size="sm"
            onClick={goToMergeRequest}
          />
        </Flex>
      </Flex>

      <MergeRequestActions mergeRequest={mergeRequest} mt={2} issueId={issueId} />
    </Box>
  );
};
