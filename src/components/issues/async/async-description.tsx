import { Box } from '@chakra-ui/core';
import React from 'react';
import '../../../assets/scss/description.scss';
import { useMarkdownToHtml } from '../../../hooks';

interface Props {
  description: string;
}

export const AsyncDescription: React.FC<Props> = ({ description }) => {
  const html = useMarkdownToHtml(description);

  return (
    <Box
      fontSize="sm"
      className="issue-description"
      dangerouslySetInnerHTML={{ __html: html }}
    ></Box>
  );
};
