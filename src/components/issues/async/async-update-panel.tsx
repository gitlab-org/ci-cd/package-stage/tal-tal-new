import { Accordion, AccordionHeader, AccordionIcon, AccordionItem, AccordionPanel, Avatar, Box, Button, Flex, Text } from '@chakra-ui/core';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { asyncTemplate, fromNow } from '../../../helpers';
import { GitLabUser, Issue } from '../../../models';
import { addAsyncUpdate } from '../../../store/actions/issues';
import { getCurrentUser, isUpdateLoading } from '../../../store/selectors';
import { EmptyState } from '../../empty-state';
import { MarkdownEditor } from '../../markdown-editor';
import { AsyncDescription } from './async-description';
import { UpdatesMap } from './heat-map';

interface Props {
  issue: Issue;
  isUpdating: boolean;
  onUpdatingChange: () => void;
}

export const AsyncUpdatePanel: React.FC<Props> = ({
  issue,
  isUpdating: initialUpdating = false,
  onUpdatingChange,
}) => {
  const dispatch = useDispatch();
  const user = useSelector(getCurrentUser) as GitLabUser;
  const isSendingUpdate = useSelector(isUpdateLoading)

  const [showUpdating, setUpdating] = useState(initialUpdating);
  const [updateValue, setUpdateValue] = useState('');

  const { asyncUpdates = [] } = issue;
  const updateText = asyncUpdates.length
    ? asyncUpdates[asyncUpdates.length - 1].body
    : asyncTemplate;

  useEffect(() => {
    if (initialUpdating) {
      setUpdating(initialUpdating);
    }
  }, [initialUpdating]);

  const onSaveClick = async () => {
    await dispatch(addAsyncUpdate(issue.id, updateValue));
    onCancel();
  };

  const onCancel = () => {
    setUpdating(false);
    onUpdatingChange();
  };

  return (
    <Flex flex={1} w="100%" direction="column">
      {asyncUpdates.length > 0 && (
        <Box
          p={4}
          backgroundColor="white"
          borderWidth="1px"
          borderRadius={4}
          borderColor="gray.400"
          w="100%"
        >
          <Text fontSize="lg" fontWeight="bold" mb={4}>
            Update heatmap
          </Text>

          <Box maxH="120px">
            <UpdatesMap startFrom={issue.created_at} updates={asyncUpdates} />
          </Box>

          <Text fontSize="lg" fontWeight="bold" mb={4}>
            Previous Updates
          </Text>

          {asyncUpdates.map(x => (
            <Accordion allowMultiple key={x.id}>
              <AccordionItem>
                <AccordionHeader>
                  <Flex flex="1" textAlign="left" alignItems="center">
                    <Avatar src={x.author.avatar_url} size="sm" mr={2} />
                    <Text fontSize="sm">
                      Update from <strong>{x.author.name}</strong> {fromNow(x.created_at)}
                    </Text>
                  </Flex>
                  <AccordionIcon />
                </AccordionHeader>
                <AccordionPanel pb={4}>
                  <AsyncDescription description={x.body} />
                </AccordionPanel>
              </AccordionItem>
            </Accordion>
          ))}

          <Button
            variantColor="green"
            size="sm"
            mt={2}
            onClick={() => setUpdating(true)}
            isDisabled={showUpdating}
          >
            Add Update
          </Button>
        </Box>
      )}

      {showUpdating && (
        <Box
          p={4}
          mt={2}
          backgroundColor="white"
          borderWidth="1px"
          borderRadius={4}
          borderColor="gray.400"
          w="100%"
        >
          <Text fontSize="lg" fontWeight="bold" mb={4}>
            New Update
          </Text>

          <Flex>
            <Avatar src={user.avatar_url} mr={2} />
            <Flex direction="column" flex={1}>
              <Flex fontSize="sm" flex={1}>
                <MarkdownEditor value={updateText} onChange={s => setUpdateValue(s)} />
              </Flex>

              <Flex justifyContent="space-between" mt={2}>
                <Button size="sm" variantColor="green" onClick={onSaveClick} isLoading={isSendingUpdate}>
                  Save Update
                </Button>
                <Button size="sm" onClick={onCancel}>
                  Cancel
                </Button>
              </Flex>
            </Flex>
          </Flex>
        </Box>
      )}

      {asyncUpdates.length === 0 && !showUpdating && (
        <EmptyState
          title="No Updates Found"
          text="No async updates have been added to this issue yet."
          illustration="async-update"
        >
          <Button mt={6} variantColor="green" onClick={() => setUpdating(true)}>
            Add Update
          </Button>
        </EmptyState>
      )}
    </Flex>
  );
};
