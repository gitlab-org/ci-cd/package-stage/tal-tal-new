import moment from 'moment';
import React, { useEffect, useState } from 'react';
import CalendarHeatmap from 'react-calendar-heatmap';
import 'react-calendar-heatmap/dist/styles.css';
import { IssueNote } from '../../../models';

type Value = {
  date: string;
  count: number;
};

interface Props {
  startFrom: string;
  updates: IssueNote[];
}

export const UpdatesMap: React.FC<Props> = ({ startFrom, updates }) => {
  const startDate = moment(startFrom).toDate();
  const endDate = moment().toDate();
  const weekdayLabels = ['', 'M', '', 'W', '', 'F', ''];

  const [values, setValues] = useState<Value[]>([]);

  useEffect(() => {
    const res = updates.map(x => ({
      date: x.created_at,
      count: 1,
    }));

    setValues(res);
  }, [updates]);

  return (
    <div className="async-heatmap">
      <CalendarHeatmap
        startDate={startDate}
        endDate={endDate}
        showMonthLabels={false}
        showWeekdayLabels
        horizontal
        weekdayLabels={weekdayLabels}
        values={values}
        classForValue={(value: Value) => {
          const { count = 0 } = value || {};

          if (count > 0) {
            return 'update-complete';
          }

          return 'color-empty';
        }}
      />
    </div>
  );
};
