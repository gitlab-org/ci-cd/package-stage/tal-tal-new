import { Box } from '@chakra-ui/core';
import React from 'react';
import '../../assets/scss/description.scss';
import { projectUrlFromReference } from '../../helpers';
import { useMarkdownToHtml } from '../../hooks';
import { Issue, MergeRequest } from '../../models';

interface Props {
  item: Issue | MergeRequest;
}

export const IssueDescription: React.FC<Props> = ({ item }) => {
  const imageUrl = `${projectUrlFromReference(item.references)}/`;
  const html = useMarkdownToHtml(item.description, { imageUrl });

  return (
    <Box
      p={4}
      backgroundColor="white"
      borderWidth="1px"
      borderRadius={4}
      borderColor="gray.400"
      className="issue-description"
      dangerouslySetInnerHTML={{ __html: html }}
    />
  );
};
