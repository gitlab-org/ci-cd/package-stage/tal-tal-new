import { Flex, Tab, TabList, TabPanel, TabPanels, Tabs, Text } from '@chakra-ui/core';
import moment from 'moment';
import React from 'react';
import { useSelector } from 'react-redux';
import { MilestonesMenu } from '../../models';
import { getLastUpdated } from '../../store/selectors';
import { TabCount } from '../tab-count';
import { IssueListItem } from './issue-list-item';

interface Props {
  milestonesMenu: MilestonesMenu;
}

type Tab = {
  label: string;
  count: number;
  issues: number[];
};

const getTabs = (milestonesMenu: MilestonesMenu) => {
  return Object.keys(milestonesMenu)
    .sort()
    .map(x => ({
      label: x,
      count: milestonesMenu[x].count || 0,
      issues: milestonesMenu[x].issues || [],
    }));
};

export const IssuesList: React.FC<Props> = ({ milestonesMenu }) => {
  const lastSync = useSelector(getLastUpdated);
  const tabs = getTabs(milestonesMenu);

  return (
    <Flex flex={1}>
      <Flex flex={1} direction="column">
        <Flex direction="column" mb={4} px={4}>
          <Text fontSize="xl" fontWeight="bold">
            Your Issues
          </Text>
          {lastSync ? (
            <Text color="gray.500" fontSize="sm">
              Last sync: <strong>{moment(lastSync).fromNow()}</strong>.
            </Text>
          ) : null}
        </Flex>

        <Tabs as={Flex} flex={1} flexDirection="column">
          <TabList>
            {tabs.map((x, index) => (
              <Tab className="issue-tabs" key={index}>
                {x.label}
                <TabCount count={x.issues.length} />
              </Tab>
            ))}
          </TabList>

          <TabPanels as={Flex} flex={1} h="100%">
            {tabs.map((x, index) => (
              <TabPanel p={4} backgroundColor="background.light" flex={1} key={index}>
                {x.issues.map(i => (
                  <IssueListItem id={i} key={i} />
                ))}
              </TabPanel>
            ))}
          </TabPanels>
        </Tabs>
      </Flex>
    </Flex>
  );
};
