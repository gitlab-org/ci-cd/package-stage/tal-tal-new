import { Button } from '@chakra-ui/core';
import cn from 'classnames';
import React from 'react';
import { FaCheck, FaHourglassStart } from 'react-icons/fa';
import { Workflow } from '../models';

const getButtonOpts = (workflow: Workflow = Workflow.NONE): any[] => {
  switch (workflow) {
    case Workflow.PRODUCTION:
      return ['green', FaCheck, true];

    case Workflow.BLOCKED:
      return ['orange', FaHourglassStart];

    case Workflow.NONE:
      return ['red', 'warning-2'];

    default:
      return ['blue', FaHourglassStart];
  }
};

interface Props {
  workflow: Workflow | undefined;
  onClick?: (e: React.MouseEvent) => void;
  readOnly?: boolean;
}

export const WorkflowButton: React.FC<Props> = ({ workflow, onClick, readOnly = false }) => {
  const [variant, icon, disabled = false] = getButtonOpts(workflow);
  const isNotActive = readOnly || disabled;

  return (
    <Button
      leftIcon={icon}
      variantColor={variant}
      variant={isNotActive ? 'outline' : 'solid'}
      size="xs"
      mr="2"
      className={cn('title', { 'void-button': isNotActive })}
      onClick={onClick}
    >
      {workflow}
    </Button>
  );
};
