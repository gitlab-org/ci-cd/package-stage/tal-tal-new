import { Box, Breadcrumb, Divider, Flex, Icon } from '@chakra-ui/core';
import React from 'react';

export const Breadcrumbs: React.FC = ({ children }) => {
  return (
    <Flex direction="column">
      <Box>
        <Breadcrumb
          fontSize="sm"
          separator={<Icon name="chevron-right" />}
          px={4}
          pt={2}
          pb={0}
          justifySelf="center"
        >
          {children}
        </Breadcrumb>
      </Box>

      <Divider />
    </Flex>
  );
};
