import {
  Avatar,
  Button,
  Flex,
  Icon,
  Menu,
  MenuButton,
  MenuDivider,
  MenuItem,
  MenuList,
  Spinner,
  Text,
  useDisclosure,
} from '@chakra-ui/core';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { importIssues } from '../../store/actions/issues';
import { getCurrentUser, getImportLoading, getRecommendUpdate } from '../../store/selectors';
import { ImportModal } from '../modals/import-modal';
import { LogoutModal } from '../modals/logout';
import { ProjectSelector } from '../project-selector';
import { SearchBox } from '../search-box';

export const Header = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const user = useSelector(getCurrentUser);
  const recommendUpdate = useSelector(getRecommendUpdate);
  const isImporting = useSelector(getImportLoading);

  const { isOpen, onOpen, onClose } = useDisclosure();
  const importModal = useDisclosure();

  const refreshIssues = () => {
    dispatch(importIssues());
  };

  const openProfile = () => {
    window.open(user?.web_url, '_blank');
  };

  const goTo = (url: string) => {
    history.push(url);
  };

  return (
    <Flex bg="gray.800" padding="0.75rem" justifyContent="space-between">
      <Flex alignItems="center">
        <ProjectSelector />

        {recommendUpdate && !isImporting && (
          <Button size="xs" h="2rem" variantColor="blue" leftIcon="info" onClick={refreshIssues}>
            Refresh recommended!
          </Button>
        )}

        {isImporting && (
          <Flex>
            <Spinner speed="0.65s" color="white" mx={2} />
            <Text color="white" fontSize="sm">
              Updating...
            </Text>
          </Flex>
        )}
      </Flex>

      <Flex alignItems="center">
        <SearchBox />

        <Menu autoSelect={false}>
          <MenuButton as={Button} size="xs" h="2rem">
            {user?.name || 'Unknown'} <Icon ml={1} name="chevron-down" />
          </MenuButton>
          <MenuList placement="bottom-end">
            <MenuItem py={1} onClick={openProfile}>
              <Flex alignItems="center">
                <Avatar src={user?.avatar_url} size="sm" mr={4} />
                <Flex direction="column">
                  <Text fontSize="sm" fontWeight="bold">
                    {user?.name}
                  </Text>
                  <Text fontSize="xs">@{user?.username}</Text>
                </Flex>
              </Flex>
            </MenuItem>

            <MenuDivider />
            <MenuItem onClick={() => goTo('about')}>About</MenuItem>
            <MenuItem onClick={() => goTo('help')}>Help</MenuItem>
            <MenuDivider />
            <MenuItem onClick={onOpen}>Logout</MenuItem>
          </MenuList>
        </Menu>
      </Flex>

      <ImportModal isOpen={importModal.isOpen} onClose={importModal.onClose} />
      <LogoutModal isOpen={isOpen} onClose={onClose} />
    </Flex>
  );
};
