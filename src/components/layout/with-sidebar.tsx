import { Box, Flex } from '@chakra-ui/core';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { menuActions } from '../../store/actions/menu';
import { isMenuOpen } from '../../store/selectors';
import { ProjectSidebar } from '../sidebar/project-sidebar';

export const WithSidebar: React.FC = ({ children }) => {
  const openWidth = ['20vw', '250px'];
  const closedWidth = ['35px', '35px'];

  const dispatch = useDispatch();
  const isOpen = useSelector(isMenuOpen);
  const [[width, maxWidth], setWidths] = useState(isOpen ? openWidth : closedWidth);

  const toggle = () => {
    if (isOpen) {
      dispatch(menuActions.toggleMenu(false));
      setWidths(closedWidth);
    } else {
      dispatch(menuActions.toggleMenu(true));
      setWidths(openWidth);
    }
  };

  return (
    <Flex flex={1}>
      <Box
        w={width}
        minW={maxWidth}
        borderRight="1px"
        borderColor="gray.200"
        className="menu-container"
      >
        <ProjectSidebar onToggle={toggle} />
      </Box>
      <Flex w="80vw" flex={1} direction="column" className="page-container">
        {children}
      </Flex>
    </Flex>
  );
};
