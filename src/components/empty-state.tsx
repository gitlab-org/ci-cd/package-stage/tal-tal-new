import { BoxProps, Flex, Text } from '@chakra-ui/core';
import React from 'react';
import { ReactComponent as AsyncUpdate } from '../assets/svgs/async-update.svg';
import { ReactComponent as IssuesEmpty } from '../assets/svgs/issues-empty.svg';
import { ReactComponent as MergeRequest } from '../assets/svgs/merge-request.svg';
import { ReactComponent as Milestones } from '../assets/svgs/milestones.svg';
import { ReactComponent as Pinned } from '../assets/svgs/pinned.svg';

interface Props extends BoxProps {
  illustration?: string;
  title: string;
  text: string;
}

const getIllustration = (illustration?: string) => {
  switch (illustration) {
    case 'async-update':
      return AsyncUpdate;

    case 'merge-request':
      return MergeRequest;

    case 'milestones':
      return Milestones;

    case 'pinned':
      return Pinned;

    default:
      return IssuesEmpty;
  }
};

export const EmptyState: React.FC<Props> = ({ illustration, title, text, children, ...props }) => {
  const IllustrationComponent = getIllustration(illustration);

  return (
    <Flex justifyContent="center" alignItems="center" flex={1} direction="column" p={6} {...props}>
      <IllustrationComponent height="25vh" width="20vw" />
      <Text fontSize="xl" fontWeight="bold" mt={12}>
        {title}
      </Text>

      <Text mt={4} textAlign="center">
        {text}
      </Text>

      {children}
    </Flex>
  );
};
