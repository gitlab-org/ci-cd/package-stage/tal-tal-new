import {
  Button,
  ButtonGroup,
  FormControl,
  FormLabel,
  Icon,
  IconButton,
  Input,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Popover,
  PopoverArrow,
  PopoverBody,
  PopoverContent,
  PopoverFooter,
  PopoverHeader,
  PopoverTrigger,
  useDisclosure,
} from '@chakra-ui/core';
import React, { useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addNewProject, menuActions } from '../store/actions/menu';
import { getProjects, getProjectsLoading, getSelectedProject } from '../store/selectors';

export const ProjectSelector = () => {
  const dispatch = useDispatch();
  const initialRef = useRef(null);

  const isLoading = useSelector(getProjectsLoading);
  const projects = useSelector(getProjects);
  const selectedProject = useSelector(getSelectedProject);

  const [projectId, setProjectId] = useState('');
  const { isOpen, onOpen, onClose } = useDisclosure();

  const setSelectedProject = (id: number) => {
    dispatch(menuActions.setSelectedProject(id));
  };

  const onProjectIdInput = (e: React.FormEvent<HTMLInputElement>) => {
    setProjectId(e.currentTarget.value);
  };

  const addProject = () => {
    const projectIdInt = parseInt(projectId);

    if (!isNaN(projectIdInt)) {
      dispatch(addNewProject(projectIdInt));
    }

    onClose();
  };

  return (
    <>
      <Menu>
        <MenuButton
          as={Button}
          size="xs"
          className="no-right-radius"
          h="2rem"
          minW="80px"
          justifyContent="space-between"
        >
          {selectedProject?.name || 'Select project...'} <Icon ml={1} name="chevron-down" />
        </MenuButton>
        <MenuList placement="bottom-start">
          {projects.map(x => (
            <MenuItem key={x.id} onClick={() => setSelectedProject(x.id)}>
              {x.name}
            </MenuItem>
          ))}
        </MenuList>
      </Menu>
      <Popover initialFocusRef={initialRef} placement="bottom-end" isOpen={isOpen}>
        <PopoverTrigger>
          <IconButton
            aria-label="Add Project"
            icon="small-add"
            size="xs"
            className="no-left-radius"
            borderLeft="1px"
            mr={1}
            h="2rem"
            onClick={onOpen}
          />
        </PopoverTrigger>
        <PopoverContent zIndex={4}>
          <PopoverArrow />
          <PopoverHeader pt={4} fontSize="sm" fontWeight="bold" border="0">
            Add Project
          </PopoverHeader>
          <PopoverBody fontSize="sm">
            <FormControl>
              <FormLabel htmlFor="projectId" fontSize="sm">
                Project Id
              </FormLabel>
              <Input ref={initialRef} id="projectId" size="sm" onInput={onProjectIdInput} />
            </FormControl>
          </PopoverBody>
          <PopoverFooter d="flex" alignItems="center" justifyContent="flex-end" border="0" pb={4}>
            <ButtonGroup size="xs">
              <Button variant="ghost" onClick={onClose}>
                Cancel
              </Button>
              <Button variantColor="green" isLoading={isLoading} onClick={addProject}>
                Add
              </Button>
            </ButtonGroup>
          </PopoverFooter>
        </PopoverContent>
      </Popover>
    </>
  );
};
