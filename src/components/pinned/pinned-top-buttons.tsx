import { Flex, IconButton } from '@chakra-ui/core';
import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { projectUrlFromReference } from '../../helpers';
import { Issue } from '../../models';
import { menuActions } from '../../store/actions/menu';
import { pinnedActions } from '../../store/actions/pinned';
import { Icons } from '../../theme/icons';

interface Props {
  item: Issue;
}

export const PinnedTopButtons: React.FC<Props> = ({ item }) => {
  const dispatch = useDispatch();
  const history = useHistory();

  const goToItem = () => {
    window.open(item.web_url, '_blank');
  };

  const goToProject = () => {
    const url = projectUrlFromReference(item.references);
    window.open(url, '_blank');
  };

  const onCloseClick = () => {
    dispatch(pinnedActions.setSelectedItem(null));
    history.push('/pinned');
  };

  const onDeletePinnedItemClick = () => {
    dispatch(menuActions.removePinnedMenuItem(item.id));
    dispatch(pinnedActions.removePinnedItem(item.id));
    history.push('/pinned');
  };

  return (
    <Flex>
      <IconButton
        aria-label="View on GitLab"
        icon={Icons.EXTERNAL_LINK}
        variant="ghost"
        size="sm"
        onClick={goToItem}
      />
      <IconButton
        aria-label="Go to Project"
        icon={Icons.HOME}
        variant="ghost"
        size="sm"
        onClick={goToProject}
      />
      <IconButton
        aria-label="Close"
        icon="small-close"
        variant="ghost"
        size="sm"
        onClick={onCloseClick}
      />
      <IconButton
        aria-label="Delete"
        icon="delete"
        variant="ghost"
        variantColor="red"
        size="sm"
        onClick={onDeletePinnedItemClick}
      />
    </Flex>
  );
};
