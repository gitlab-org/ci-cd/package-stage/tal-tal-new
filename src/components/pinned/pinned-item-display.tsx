import { Flex, Link, Tab, TabList, TabPanel, TabPanels, Tabs, Text } from '@chakra-ui/core';
import React from 'react';
import { useSelector } from 'react-redux';
import { fromNow } from '../../helpers';
import { getSelectedPinnedItem } from '../../store/selectors';
import { IssueDescription } from '../issues/issue-description';
import { PinnedItemActions } from './pinned-item-actions';
import { PinnedTopButtons } from './pinned-top-buttons';

const tabPanelProps = {
  backgroundColor: 'background.light',
  flex: 1,
  p: 4,
};

export const PinnedItemDisplay = () => {
  const item = useSelector(getSelectedPinnedItem);

  if (!item) {
    return null;
  }

  return (
    <Flex flex={1} direction="column">
      <Flex justifyContent="space-between" px={4}>
        <Flex direction="column">
          <Text fontSize="xl" fontWeight="bold">
            <Link href={item.web_url} isExternal>
              {item.title}
            </Link>
          </Text>
          <Text color="gray.500">
            #{item.iid} Opened {fromNow(item.created_at)} by {item.author.name}. Last updated{' '}
            {fromNow(item.updated_at)}.
          </Text>
        </Flex>

        <PinnedTopButtons item={item} />
      </Flex>

      <PinnedItemActions item={item} px={4} my={4} />

      <Flex justifyContent="space-between" flex={1}>
        <Tabs as={Flex} flex={1} flexDirection="column">
          <TabList>
            <Tab className="issue-tabs">Detail</Tab>
          </TabList>

          <TabPanels as={Flex} flex={1}>
            <TabPanel {...tabPanelProps}>
              <IssueDescription item={item} />
            </TabPanel>
          </TabPanels>
        </Tabs>
      </Flex>
    </Flex>
  );
};
