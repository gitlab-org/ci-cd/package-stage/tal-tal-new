import { Flex, Tab, TabList, TabPanel, TabPanels, Tabs, Text } from '@chakra-ui/core';
import React from 'react';
import { useSelector } from 'react-redux';
import { getPinnedListMenu } from '../../store/selectors';
import { TabCount } from '../tab-count';
import { PinnedListItem } from './pinned-list-item';

type Tab = {
  label: string;
  count: number;
  issues: number[];
};

export const PinnedItemsList: React.FC = () => {
  const tabs: any[] = useSelector(getPinnedListMenu);

  return (
    <Flex flex={1}>
      <Flex flex={1} direction="column">
        <Flex direction="column" mb={4} px={4}>
          <Text fontSize="xl" fontWeight="bold">
            Pinned Items
          </Text>
        </Flex>

        <Tabs as={Flex} flex={1} flexDirection="column">
          <TabList>
            {tabs.map((x, index) => (
              <Tab className="issue-tabs" key={index}>
                {x.label}
                <TabCount count={x.items.length} />
              </Tab>
            ))}
          </TabList>

          <TabPanels as={Flex} flex={1} h="100%">
            {tabs.map((x, index) => (
              <TabPanel p={4} backgroundColor="background.light" flex={1} key={index}>
                {x.items.map((i: number) => (
                  <PinnedListItem id={i} key={i} />
                ))}
              </TabPanel>
            ))}
          </TabPanels>
        </Tabs>
      </Flex>
    </Flex>
  );
};
