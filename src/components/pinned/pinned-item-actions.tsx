import { BoxProps, Button, Flex, Tag, TagIcon, TagLabel } from '@chakra-ui/core';
import React from 'react';
import { findPriorityLabels } from '../../helpers';
import { Issue } from '../../models';
import { Icons } from '../../theme/icons';

interface Props extends BoxProps {
  item: Issue;
}

export const PinnedItemActions: React.FC<Props> = ({ item, ...props }) => {
  const priorities = findPriorityLabels(item);

  const preventPropagation = (e: React.MouseEvent) => {
    e.stopPropagation();
  };

  return (
    <Flex {...props} flexWrap="wrap" onClick={preventPropagation}>
      {item.milestone && (
        <Tag size="sm" variantColor="blue" rounded="full" mr="2">
          <TagIcon icon={Icons.CLOCK} size="12px" />
          <TagLabel>{item.milestone.title}</TagLabel>
        </Tag>
      )}

      {item.isEpic && (
        <Button
          leftIcon={Icons.EPIC}
          variantColor="green"
          variant="outline"
          size="xs"
          mr="2"
          className="void-button"
        >
          Epic
        </Button>
      )}

      {/* <Button
        leftIcon={Icons.MERGE_REQUEST}
        variantColor="gray"
        variant="ghost"
        size="xs"
        mr="2"
        onClick={mergeRequestClick}
      >
        {item.mergeRequests?.length}
      </Button> */}

      {item.weight ? (
        <Button
          leftIcon={Icons.WEIGHT}
          variantColor="gray"
          variant="ghost"
          size="xs"
          mr="2"
          className="void-button"
        >
          {item.weight}
        </Button>
      ) : null}

      {priorities &&
        priorities.map((x, index) => (
          <Tag variantColor="purple" rounded="full" size="sm" key={index}>
            {x}
          </Tag>
        ))}
    </Flex>
  );
};
