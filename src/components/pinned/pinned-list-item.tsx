import { Box, Flex, IconButton, Text } from '@chakra-ui/core';
import cn from 'classnames';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { fromNow } from '../../helpers';
import { Issue } from '../../models';
import { pinnedActions } from '../../store/actions/pinned';
import { AppState } from '../../store/reducers';
import { getPinnedItem } from '../../store/selectors';
import { Icons } from '../../theme/icons';
import { PinnedItemActions } from './pinned-item-actions';

interface Props {
  id: number;
}

export const PinnedListItem: React.FC<Props> = ({ id }) => {
  const dispatch = useDispatch();
  const history = useHistory();

  const item = useSelector<AppState, Issue | undefined>((s) => getPinnedItem(s, id));

  if (!item) {
    return null;
  }

  const onIssueClick = () => {
    dispatch(pinnedActions.setSelectedItem(item.id));
    history.push('pinned/item');
  };

  const goToIssue = (e: React.MouseEvent) => {
    e.stopPropagation();
    window.open(item.web_url, '_blank');
  };

  return (
    <Box
      className={cn('issue with-hover')}
      p={5}
      borderWidth="1px"
      borderRadius={4}
      borderColor="gray.400"
      mb={2}
      backgroundColor="white"
      onClick={onIssueClick}
    >
      <Flex justifyContent="space-between">
        <Flex direction="column">
          <Text fontWeight="bold" fontSize="md">
            {item.title}
          </Text>
          <Text color="gray.500" fontSize="sm">
            {item.isEpic ? '&' : '#'}
            {item.iid} - Opened {fromNow(item.created_at)} by <strong>{item.author.name}</strong>.
            Last updated {fromNow(item.updated_at)}.
          </Text>
        </Flex>

        <Flex>
          <IconButton
            aria-label="View on GitLab"
            icon={Icons.EXTERNAL_LINK}
            variant="ghost"
            size="sm"
            onClick={goToIssue}
          />
        </Flex>
      </Flex>

      <PinnedItemActions item={item} mt={2} />
    </Box>
  );
};
