import { Flex, Select, Text } from '@chakra-ui/core';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { getProjects, getSelectedProject } from '../../../store/selectors';
import { AddNewProject } from './add-new-project';

interface Props {
  onPinnedProjectChanged: (projectId: number) => void;
}

export const AddPinnedProject: React.FC<Props> = ({ onPinnedProjectChanged }) => {
  const projects = useSelector(getProjects);
  const defaultSelectedProject = useSelector(getSelectedProject);

  const [projectId, setProjectId] = useState<string>();
  const [selectedProject, setSelectedProject] = useState(defaultSelectedProject.id);

  const onAddNewProject = (e: string) => {
    setProjectId(e);

    if (projects.find((x) => x.id === parseInt(e))) {
      setSelectedProject(parseInt(e));
      onPinnedProjectChanged(selectedProject);
      setProjectId('');
    }
  };

  const onProjectSelectChange = (e: React.FormEvent<HTMLSelectElement>) => {
    onPinnedProjectChanged(parseInt(e.currentTarget.value));
    setSelectedProject(parseInt(e.currentTarget.value));
  };

  return (
    <Flex mt={4} direction="column">
      <Text fontWeight="bold" fontSize="sm">
        Select project:
      </Text>

      <Flex alignItems="center">
        <Select
          my={4}
          onChange={onProjectSelectChange}
          mr={2}
          size="sm"
          value={selectedProject}
          flexBasis="70%"
        >
          {projects.map((x) => (
            <option value={x.id} className="title" key={x.id}>
              {x.name}
            </option>
          ))}
        </Select>

        <AddNewProject
          onAddNewProject={onAddNewProject}
          defaultProjectId={projectId?.toString() || ''}
        />
      </Flex>
    </Flex>
  );
};
