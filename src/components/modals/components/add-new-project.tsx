import { Button, Flex, Input } from '@chakra-ui/core';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addNewProject } from '../../../store/actions/menu';
import { getProjects, getProjectsLoading } from '../../../store/selectors';

interface Props {
  onAddNewProject: (projectId: string) => void;
  defaultProjectId: string;
}

export const AddNewProject: React.FC<Props> = ({ onAddNewProject, defaultProjectId }) => {
  const dispatch = useDispatch();

  const projects = useSelector(getProjects);
  const isLoading = useSelector(getProjectsLoading);

  const [projectId, setProjectId] = useState('');
  const [addProjectDisabled, setAddProjectDisabled] = useState(true);

  useEffect(() => {
    onAddNewProject(projectId);
  }, [projects, projectId, onAddNewProject]);

  const onCategoryChange = (e: React.FormEvent<HTMLInputElement>) => {
    setProjectId(e.currentTarget.value);
    setAddProjectDisabled(!Boolean(e.currentTarget.value));
    onAddNewProject(e.currentTarget.value);
  };

  const addProject = () => {
    const asInt = parseInt(projectId);

    if (!isNaN(asInt) && !projects.find((x) => x.id === asInt)) {
      dispatch(addNewProject(asInt, false));
    } else {
      setProjectId('');
      setAddProjectDisabled(true);
    }
  };

  return (
    <Flex flex={1}>
      <Input
        placeholder="Project id"
        size="sm"
        mr={2}
        onChange={onCategoryChange}
        value={defaultProjectId}
      />
      <Button
        variant="solid"
        variantColor="blue"
        size="sm"
        isDisabled={addProjectDisabled}
        isLoading={isLoading}
        onClick={addProject}
      >
        Add
      </Button>
    </Flex>
  );
};
