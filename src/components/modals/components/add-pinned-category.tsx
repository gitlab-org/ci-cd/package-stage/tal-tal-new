import { Flex, Select, Text } from '@chakra-ui/core';
import React, { useState } from 'react';
import { AddNewCategory } from './add-new-category';

interface Props {
  onPinnedCategoryChanged: (category: string) => void;
}

export const AddPinnedCategory: React.FC<Props> = ({ onPinnedCategoryChanged }) => {
  const [category, setCategory] = useState<string>('');
  const [categories, setCategories] = useState<string[]>(['Uncategorised']);

  const onCategorySelectChange = (e: React.FormEvent<HTMLSelectElement>) => {
    setCategory(e.currentTarget.value);
    onPinnedCategoryChanged(e.currentTarget.value);
  };

  const onAddNewCategory = (e: string) => {
    setCategories([...categories, e]);
    setCategory(e);
    onPinnedCategoryChanged(e);
  };

  return (
    <Flex mt={4} direction="column">
      <Text fontWeight="bold" fontSize="sm">
        Select category:
      </Text>

      <Flex alignItems="center">
        <Select
          my={4}
          onChange={onCategorySelectChange}
          size="sm"
          mr={2}
          flexBasis="60%"
          value={category}
        >
          {categories.map((x, index) => (
            <option value={x} className="title" key={index}>
              {x}
            </option>
          ))}
        </Select>

        <AddNewCategory onAddNewCategory={onAddNewCategory} />
      </Flex>
    </Flex>
  );
};
