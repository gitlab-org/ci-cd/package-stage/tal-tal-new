import { Button, Flex, Input } from '@chakra-ui/core';
import React, { useState } from 'react';

interface Props {
  onAddNewCategory: (category: string) => void;
}

export const AddNewCategory: React.FC<Props> = ({ onAddNewCategory }) => {
  const [category, setCategory] = useState('');
  const [addCategoryDisabled, setAddCategoryDisabled] = useState(true);

  const onCategoryChange = (e: React.FormEvent<HTMLInputElement>) => {
    setCategory(e.currentTarget.value);
    setAddCategoryDisabled(!Boolean(e.currentTarget.value));
  };

  const addCategory = () => {
    onAddNewCategory(category);
    setCategory('');
  };

  return (
    <Flex flex={1}>
      <Input
        placeholder="New category"
        size="sm"
        mr={2}
        onChange={onCategoryChange}
        value={category}
      />
      <Button
        variant="solid"
        variantColor="blue"
        size="sm"
        isDisabled={addCategoryDisabled}
        onClick={addCategory}
      >
        Add
      </Button>
    </Flex>
  );
};
