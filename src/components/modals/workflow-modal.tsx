import {
  Button,
  Checkbox,
  Flex,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Select,
  Stack,
  Text,
} from '@chakra-ui/core';
import React, { useContext, useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppContextComponent } from '../../app-context';
import { capitalize } from '../../helpers';
import { Issue, MergeRequest, Workflow } from '../../models';
import {
  issueActions,
  updateMergeRequestWorkflow,
  updateWorkflow,
} from '../../store/actions/issues';
import { isUpdateLoading } from '../../store/selectors';
import { WorkflowButton } from '../workflow-button';

const getItemDetails = (issue: Issue | null, mergeRequest: MergeRequest | null) => {
  if (issue) {
    return [`#${issue.iid}`, issue.title, issue.workflow];
  } else if (mergeRequest) {
    return [`!${mergeRequest.iid}`, mergeRequest.title, mergeRequest.workflow];
  }

  return [];
};

export const WorkflowModal: React.FC = () => {
  const dispatch = useDispatch();
  const initialRef = useRef(null);

  const { state, dispatch: contextDispatch } = useContext(AppContextComponent);
  const { isOpen, issue, mergeRequest, issueId } = state.workflowModal;

  const isLoading = useSelector(isUpdateLoading);
  const [updateApplied, setUpdateApplied] = useState<boolean>(false);
  const [mergedRequestsWorkflowChange, setMergedRequestsWorkflowChange] = useState<number[]>([]);
  const [selectedWorkflow, setSelectedWorkflow] = useState<string | null>(null);

  const [idToDisplay, title, workflow] = getItemDetails(issue, mergeRequest);

  useEffect(() => {
    // Only force close the modal after applying an update
    if (updateApplied === true && isLoading === false) {
      contextDispatch({
        type: 'toggle-workflow-modal',
        payload: { isOpen: false, issue: null, mergeRequest: null },
      });
    }
  }, [updateApplied, isLoading, contextDispatch]);

  useEffect(() => {
    // Reset the state of the modal when opening / closing
    setUpdateApplied(false);
    setMergedRequestsWorkflowChange([]);
    setSelectedWorkflow(null);
    dispatch(issueActions.setUpdateLoading(false));
  }, [dispatch, isOpen]);

  // Only show merge requests that haven't already been merged
  const mergeRequests = (issue && issue.mergeRequests?.filter((x) => !x.merged_at)) || [];

  const onWorkflowSelectChange = (e: React.FormEvent<HTMLSelectElement>) => {
    setSelectedWorkflow((Workflow as any)[e.currentTarget.value] || null);
  };

  const onMergeRequestChange = (e: React.FormEvent<HTMLInputElement>, id: number) => {
    if (e.currentTarget.checked) {
      mergedRequestsWorkflowChange.push(id);
    } else {
      const index = mergedRequestsWorkflowChange.findIndex((x) => x === id);

      if (index > -1) {
        mergedRequestsWorkflowChange.splice(index, 1);
      }
    }
  };

  const saveWorkflowUpdates = () => {
    if (selectedWorkflow) {
      setUpdateApplied(true);

      if (issue) {
        dispatch(updateWorkflow(issue, selectedWorkflow, mergedRequestsWorkflowChange));
      } else if (mergeRequest && issueId !== null) {
        dispatch(updateMergeRequestWorkflow(mergeRequest, selectedWorkflow, issueId));
      }
    }
  };

  const closeModal = () => {
    contextDispatch({
      type: 'toggle-workflow-modal',
      payload: { isOpen: false, issue: null, mergeRequest: null },
    });
  };

  return (
    <Modal
      isOpen={isOpen}
      closeOnOverlayClick={false}
      onClose={closeModal}
      size="xl"
      initialFocusRef={initialRef}
    >
      <ModalOverlay />
      <ModalContent maxWidth="60rem">
        <ModalHeader>
          Change Workflow for {idToDisplay}
          <Text fontSize="sm" fontWeight="normal" color="gray.500">
            {title}
          </Text>
        </ModalHeader>
        <ModalBody>
          <Text>
            The current workflow is: <WorkflowButton workflow={workflow as Workflow} readOnly />
          </Text>

          <Select
            placeholder="Select new workflow"
            my={4}
            ref={initialRef}
            onChange={onWorkflowSelectChange}
          >
            {Object.keys(Workflow)
              .filter((x) => x !== 'NONE')
              .map((x) => (
                <option value={x} className="title" key={x}>
                  {/* I sure do _love_ typescript */}
                  {capitalize((Workflow as any)[x])}
                </option>
              ))}
          </Select>

          {mergeRequests.length ? (
            <>
              <Text>You can also apply the new workflow to the following merge requests:</Text>

              <Stack spacing={0} borderWidth="1px" my={4}>
                {mergeRequests.map((x, index) => (
                  <Checkbox
                    size="sm"
                    borderBottomWidth={index < mergeRequests.length - 1 ? '1px' : '0'}
                    p={2}
                    flex={1}
                    className="workflow-checkbox"
                    onChange={(e) => onMergeRequestChange(e, x.id)}
                    key={x.id}
                  >
                    <Flex justifyContent="space-between" flexWrap="wrap">
                      <Text flex={1}>{x.title}</Text>{' '}
                      <WorkflowButton workflow={x.workflow} readOnly />
                    </Flex>
                  </Checkbox>
                ))}
              </Stack>
            </>
          ) : null}
        </ModalBody>

        <ModalFooter>
          <Button variant="ghost" mr={3} onClick={closeModal} size="sm" isDisabled={isLoading}>
            Cancel
          </Button>
          <Button
            variant="solid"
            variantColor="green"
            size="sm"
            onClick={saveWorkflowUpdates}
            isDisabled={selectedWorkflow === null}
            isLoading={isLoading}
          >
            Save
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};
