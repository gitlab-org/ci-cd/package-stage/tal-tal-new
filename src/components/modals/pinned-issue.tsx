import {
  Button,
  Flex,
  FormHelperText,
  Input,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
} from '@chakra-ui/core';
import React, { useContext, useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppContextComponent } from '../../app-context';
import { addNewPinnedItem } from '../../store/actions/pinned';
import { getPinnedItemLoading, getSelectedProject } from '../../store/selectors';
import { AddPinnedCategory } from './components/add-pinned-category';
import { AddPinnedProject } from './components/add-pinned-project';

export const PinnedModal: React.FC = () => {
  const dispatch = useDispatch();
  const initialRef = useRef(null);

  const { state, dispatch: contextDispatch } = useContext(AppContextComponent);
  const { isOpen } = state.pinnedModal;

  const isLoading = useSelector(getPinnedItemLoading);
  const defaultSelectedProject = useSelector(getSelectedProject);

  const [isDisabled, setIsDisabled] = useState(true);
  const [updateApplied, setUpdateApplied] = useState<boolean>(false);
  const [selectedProject, setSelectedProject] = useState<number>(defaultSelectedProject.id);
  const [itemId, setItemId] = useState('');
  const [selectedCategory, setSelectedCategory] = useState('Uncategorised');

  const onSelectedProjectChange = (projectId: number) => {
    setSelectedProject(projectId);
  };

  const onSelectedCatetoryChange = (category: string) => {
    setSelectedCategory(category);
  };

  useEffect(() => {
    if (selectedProject && itemId && selectedCategory) {
      setIsDisabled(false);
    } else {
      setIsDisabled(true);
    }
  }, [selectedProject, itemId, selectedCategory]);

  useEffect(() => {
    // Only force close the modal after applying an update
    if (updateApplied === true && isLoading === false) {
      contextDispatch({
        type: 'toggle-new-pinned-modal',
        payload: { isOpen: false },
      });
    }
  }, [updateApplied, isLoading, contextDispatch]);

  // Reset the state of the modal when opening / closing
  useEffect(() => {
    setUpdateApplied(false);
    setItemId('');
    setSelectedProject(defaultSelectedProject.id);
    setSelectedCategory('Uncategorised');
  }, [defaultSelectedProject.id, dispatch, isOpen]);

  const pinNewIssue = () => {
    setUpdateApplied(true);
    dispatch(addNewPinnedItem(itemId, selectedProject, selectedCategory));
  };

  const closeModal = () => {
    contextDispatch({
      type: 'toggle-new-pinned-modal',
      payload: { isOpen: false },
    });
  };

  return (
    <Modal
      isOpen={isOpen}
      closeOnOverlayClick={false}
      onClose={closeModal}
      initialFocusRef={initialRef}
    >
      <ModalOverlay />
      <ModalContent maxWidth="60rem">
        <ModalHeader>Pin an item</ModalHeader>
        <ModalBody>
          <Text>Add a new pinned item below. You can enter an issue or epid id.</Text>

          <AddPinnedProject onPinnedProjectChanged={onSelectedProjectChange} />

          <Flex direction="column" mt={2}>
            <Text fontWeight="bold" fontSize="sm">
              Id:
            </Text>

            <Input
              placeholder="Enter an id"
              mt={4}
              size="sm"
              ref={initialRef}
              value={itemId}
              onChange={(e: React.FormEvent<HTMLInputElement>) => setItemId(e.currentTarget.value)}
            />

            <FormHelperText id="project-id-helper-text">
              You can prefix with an &amp; to add epics, e.g. &amp;1234
            </FormHelperText>
          </Flex>

          <AddPinnedCategory onPinnedCategoryChanged={onSelectedCatetoryChange} />
        </ModalBody>

        <ModalFooter>
          <Button variant="ghost" mr={3} onClick={closeModal} size="sm" isDisabled={isLoading}>
            Cancel
          </Button>
          <Button
            variant="solid"
            variantColor="green"
            size="sm"
            onClick={pinNewIssue}
            isDisabled={isDisabled}
            isLoading={isLoading}
          >
            Pin
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};
