import {
  Button,
  Flex,
  FormHelperText,
  Input,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
} from '@chakra-ui/core';
import React, { useContext, useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { AppContextComponent } from '../../app-context';
import { menuActions } from '../../store/actions/menu';
import { milestoneActions } from '../../store/actions/milestones';
import { getSelectedProject, getTrackedMilestones } from '../../store/selectors';

export const AddMilestoneModal: React.FC = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const initialRef = useRef(null);

  const existingMilestones = useSelector(getTrackedMilestones);
  const project = useSelector(getSelectedProject);
  const { state, dispatch: contextDispatch } = useContext(AppContextComponent);
  const { isOpen } = state.milestoneModal;

  const [isDisabled, setIsDisabled] = useState(true);
  const [milestoneToAdd, setMilestoneToAdd] = useState('');

  useEffect(() => {
    if (
      milestoneToAdd &&
      !existingMilestones.find((x) => x.toLowerCase() === milestoneToAdd.trim().toLowerCase())
    ) {
      setIsDisabled(false);
    } else {
      setIsDisabled(true);
    }
  }, [existingMilestones, milestoneToAdd]);

  const addMilestone = () => {
    const milestone = milestoneToAdd.trim();

    if (
      milestone &&
      !existingMilestones.find((x) => x.toLowerCase() === milestone.trim().toLowerCase())
    ) {
      dispatch(milestoneActions.addNewMilestone(milestone.trim()));
      dispatch(milestoneActions.setSelectedMilestone(milestone.trim()));
      dispatch(
        menuActions.buildMilestonesMenu({
          projectId: project.id,
          milestones: [...existingMilestones, milestone],
        }),
      );

      closeModal();
      history.push('/milestones/detail');
    }
  };

  const closeModal = () => {
    setMilestoneToAdd('');
    setIsDisabled(true);

    contextDispatch({
      type: 'toggle-milestone-modal',
      payload: { isOpen: false },
    });
  };

  return (
    <Modal
      isOpen={isOpen}
      closeOnOverlayClick={false}
      onClose={closeModal}
      initialFocusRef={initialRef}
    >
      <ModalOverlay />
      <ModalContent maxWidth="60rem">
        <ModalHeader>Add milestone</ModalHeader>
        <ModalBody>
          <Text>Add a new milestone to start tracking. Add the name of the milestone below:</Text>

          <Flex direction="column">
            <Input
              placeholder="Milestone name"
              mt={4}
              size="sm"
              ref={initialRef}
              value={milestoneToAdd}
              onChange={(e: React.FormEvent<HTMLInputElement>) =>
                setMilestoneToAdd(e.currentTarget.value)
              }
            />

            <FormHelperText id="project-id-helper-text">
              The name should match the name of a milestone in this project. For example,{' '}
              <code>12.10</code> or <code>13.0</code>.
            </FormHelperText>
          </Flex>
        </ModalBody>

        <ModalFooter>
          <Button variant="ghost" mr={3} onClick={closeModal} size="sm">
            Cancel
          </Button>
          <Button
            variant="solid"
            variantColor="green"
            size="sm"
            onClick={addMilestone}
            isDisabled={isDisabled}
          >
            Add
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};
