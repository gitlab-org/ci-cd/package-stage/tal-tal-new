import {
  Button,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
} from '@chakra-ui/core';
import React from 'react';
import { useDispatch } from 'react-redux';
import { signOut } from '../../store/actions/auth';

interface Props {
  isOpen: boolean;
  onClose?: (e: any) => void;
}

export const LogoutModal: React.FC<Props> = ({ isOpen, onClose }) => {
  const dispatch = useDispatch();

  const signOutClick = () => {
    dispatch(signOut());
  };

  return (
    <Modal isOpen={isOpen} closeOnOverlayClick={false} isCentered onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Logout</ModalHeader>
        <ModalBody>
          <Text>
            Are you sure you wish to logout? All of your data will be erased and you will have to
            import your issues again.
          </Text>
        </ModalBody>

        <ModalFooter>
          <Button variant="ghost" mr={3} onClick={onClose} size="sm">
            Cancel
          </Button>
          <Button variant="solid" variantColor="red" onClick={signOutClick} size="sm">
            Logout
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};
