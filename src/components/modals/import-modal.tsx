import {
  Button,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
} from '@chakra-ui/core';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { importIssues } from '../../store/actions/issues';
import { getImportLoading } from '../../store/selectors';

interface Props {
  isOpen: boolean;
  onClose?: (e: any) => void;
}

export const ImportModal: React.FC<Props> = ({ isOpen, onClose }) => {
  const dispatch = useDispatch();
  const isLoading = useSelector(getImportLoading);

  const importClick = () => {
    dispatch(importIssues());
  };

  return (
    <Modal isOpen={isOpen} closeOnOverlayClick={false} isCentered onClose={onClose} size="lg">
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Import</ModalHeader>
        <ModalBody>
          <Text>Do you want to import your issues from GitLab?</Text>
        </ModalBody>

        <ModalFooter>
          <Button variant="ghost" size="sm" mr={3} onClick={onClose} isDisabled={isLoading}>
            Cancel
          </Button>
          <Button
            variant="solid"
            variantColor="blue"
            size="sm"
            onClick={importClick}
            isLoading={isLoading}
          >
            Import
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};
