import { Box } from '@chakra-ui/core';
import React, { useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { AppContextComponent } from '../../app-context';
import { Menu, MenuItem } from '../../models';
import { importIssues } from '../../store/actions/issues';
import { fetchMergeRequestsForProject } from '../../store/actions/merge-requests';
import { getSelectedProject } from '../../store/selectors';
import { Icons } from '../../theme/icons';
import { ClearButton } from './clear-button';
import { CollapsedButton } from './collapsed-button';
import { IssueButton } from './issue-button';
import { MilestoneButton } from './milestone-button';
import { SidebarFolder } from './sidebar-folder';

interface Props {
  id: string;
  text: string;
  count?: number;
  icon: any;
  isDefaultOpen?: boolean;
  tree:
    | {
        [key: string]: MenuItem;
      }
    | undefined;
  parentOpen: boolean;
}

// TODO: This could be made recursive so the menu can support unlimited nesting.
// Right now, I'll leave it like this as I don't think we really want more than
// one level deep.
export const SideBarButton: React.FC<Props> = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { dispatch: contextDispatch } = useContext(AppContextComponent);
  const selectedProject = useSelector(getSelectedProject);

  const goToIssuesList = () => {
    history.push('/');
  };

  const goToMergeRequestsList = () => {
    history.push('/merge-requests');
  };

  const goToMilestones = () => {
    history.push('/milestones');
  };

  const goToPinned = () => {
    history.push('/pinned');
  };

  const refreshIssues = () => {
    dispatch(importIssues());
  };

  const refreshMergeRequests = () => {
    dispatch(fetchMergeRequestsForProject(selectedProject.id));
  };

  const addMilestone = () => {
    contextDispatch({
      type: 'toggle-milestone-modal',
      payload: { isOpen: true },
    });
  };

  const addPinnedItem = () => {
    contextDispatch({
      type: 'toggle-new-pinned-modal',
      payload: { isOpen: true },
    });
  };

  // TODO: Needs fixing...
  // Honestly, whole menu needs a deep evaluation. I've let it get out of control.
  const getAdditionButtons = () => {
    switch (props.id) {
      case 'issues':
        return [
          <ClearButton icon={Icons.LIST_BULLETED} key="1" onClick={goToIssuesList} />,
          <ClearButton icon="repeat" borderBottom={false} key="2" onClick={refreshIssues} />,
        ];

      case 'mergeRequests':
        return [
          <ClearButton icon={Icons.LIST_BULLETED} key="1" onClick={goToMergeRequestsList} />,
          <ClearButton icon="repeat" borderBottom={false} key="2" onClick={refreshMergeRequests} />,
        ];

      case 'milestones':
        return [
          <ClearButton icon={Icons.LIST_BULLETED} key="1" onClick={goToMilestones} />,
          <ClearButton icon="add" borderBottom={false} key="2" onClick={addMilestone} />,
        ];

      case 'pinned':
        return [
          <ClearButton icon={Icons.LIST_BULLETED} key="1" onClick={goToPinned} />,
          <ClearButton icon="add" borderBottom={false} key="2" onClick={addPinnedItem} />,
        ];

      default:
        return [];
    }
  };

  const onClickFunction = () => {
    switch (props.id) {
      case 'issues':
        return goToIssuesList;

      case 'mergeRequests':
        return goToMergeRequestsList;

      case 'pinned':
        return goToPinned;

      default:
        return () => {};
    }
  };

  return (
    <Box borderBottom="1px" borderColor="menu.border" style={{ cursor: 'pointer' }}>
      {props.parentOpen ? (
        <SidebarFolder {...props} additionalButtons={getAdditionButtons()}>
          {props.tree && props.parentOpen ? (
            <Box fontSize="sm">
              {Object.keys(props.tree)
                .sort()
                .map((x) => {
                  // :face_palm:
                  if (props.tree) {
                    const item = props.tree[x];
                    if (item.type === 'folder') {
                      return (
                        <SidebarFolder
                          key={item.id}
                          id={`${props.id}-${item.id}`}
                          text={item.label}
                          isSmall={true}
                          isDefaultOpen={item.isOpen}
                        >
                          {item.tree
                            ? Object.keys(item.tree).map((x, index) => (
                                <IssueButton item={(item.tree as Menu)[x]} key={index} />
                              ))
                            : null}
                        </SidebarFolder>
                      );
                    } else {
                      // TODO: Maybe in the future there will be other types available here
                      return <MilestoneButton milestone={x} key={x} />;
                    }
                  }

                  return null;
                })}
            </Box>
          ) : null}
        </SidebarFolder>
      ) : (
        <CollapsedButton icon={props.icon} label={props.text} onClick={onClickFunction()} />
      )}
    </Box>
  );
};
