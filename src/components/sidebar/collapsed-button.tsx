import { Box, Flex, Icon, Tooltip } from '@chakra-ui/core';
import React from 'react';

interface Props {
  icon?: any;
  label: string;
  onClick: () => void;
}

export const CollapsedButton: React.FC<Props> = ({ icon, label, onClick }) => {
  return (
    <Box
      borderBottom="1px"
      borderColor="menu.border"
      style={{ cursor: 'pointer' }}
      onClick={onClick}
    >
      <Tooltip aria-label={label} label={label} placement="right" hasArrow>
        <Flex
          alignItems="center"
          justifyContent="center"
          className="with-hover"
          color="menu.font"
          h="60px"
        >
          <Icon name={icon} mr={1} size="0.9rem" />
        </Flex>
      </Tooltip>
    </Box>
  );
};
