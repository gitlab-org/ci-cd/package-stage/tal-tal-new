import { Flex, Icon, Text, Tooltip } from '@chakra-ui/core';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Issue, MenuItem, MergeRequest } from '../../models';
import { issueActions } from '../../store/actions/issues';
import { fetchMergeRequestComments, mergeRequestActions } from '../../store/actions/merge-requests';
import { pinnedActions } from '../../store/actions/pinned';
import { AppState } from '../../store/reducers';
import { getIssueById, getMergeRequestById, isIssueSkipped } from '../../store/selectors';
import { Icons } from '../../theme/icons';

const getIssueIconColor = (
  type: string,
  updateCompleted: boolean | undefined,
  isSkipped: boolean,
  isPinned: boolean = false,
): [any, string] => {
  if (type === 'merge-request') {
    return [null, 'gray.300'];
  } else if (updateCompleted) {
    return ['check', 'menu.updateCompleted'];
  } else if (isSkipped) {
    return [Icons.CLEAR, 'gray.400'];
  } else if (isPinned) {
    return [null, 'gray.300'];
  }

  return ['warning-2', 'menu.updatedRequired'];
};

interface Props {
  item: MenuItem;
}

export const IssueButton: React.FC<Props> = ({ item }) => {
  const dispatch = useDispatch();
  const history = useHistory();

  const issue = useSelector<AppState, Issue | undefined>((s) => getIssueById(s, parseInt(item.id)));
  const mergeRequest = useSelector<AppState, MergeRequest | undefined>((s) =>
    getMergeRequestById(s, parseInt(item.id)),
  );

  const buttonItem = issue || mergeRequest || null;

  const isSkipped = useSelector<AppState, boolean>((s) => isIssueSkipped(s, parseInt(item.id)));

  if (!buttonItem) {
    return null;
  }

  const goToIssue = () => {
    if (buttonItem.type === 'merge-request') {
      dispatch(fetchMergeRequestComments(buttonItem as MergeRequest));
      dispatch(mergeRequestActions.setSelectedMergeRequest(buttonItem.id));
      history.push('/merge-requests/detail');
    } else if ((buttonItem as Issue).isPinned) {
      dispatch(pinnedActions.setSelectedItem(buttonItem.id));
      history.push('/pinned/item');
    } else {
      if (buttonItem.type === 'merge-request') {
        // TODO: Add navigation to merge request detail
      } else {
        dispatch(issueActions.setSelectedIssue(buttonItem.id));
        history.push('/issue');
      }
    }
  };

  const [icon, color] = getIssueIconColor(
    buttonItem.type,
    (buttonItem as Issue).updateCompleted,
    isSkipped,
    (buttonItem as Issue).isPinned,
  );

  return (
    <Flex
      key={item.id}
      pl={10}
      py={2}
      className="with-hover"
      alignItems="center"
      color={color}
      onClick={goToIssue}
    >
      {icon && <Icon name={icon as any} mr={1} size="0.75em" />}
      <Tooltip aria-label="Issue" hasArrow label={item.label} placement="right">
        <Text fontSize="xs" isTruncated pr={1}>
          {item.label}
        </Text>
      </Tooltip>
    </Flex>
  );
};
