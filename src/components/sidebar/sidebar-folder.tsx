import { Badge, Box, BoxProps, Flex, Icon, Text } from '@chakra-ui/core';
import cn from 'classnames';
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { menuActions } from '../../store/actions/menu';
import { Icons } from '../../theme/icons';

/**
 * // TODO: Consider re-writing this whole menu
 *
 * The inital idea started with a file explorer but as I've worked on the UI
 * it's becoming more obvious this isn't the best fit and this is closer to a
 * navigation menu.
 */

interface Props extends BoxProps {
  id: string;
  icon?: any;
  text: string;
  count?: number;
  additionalButtons?: React.ReactNode[];
  isDefaultOpen?: boolean;
  isSmall?: boolean;
}

export const SidebarFolder: React.FC<Props> = ({
  id,
  icon = Icons.FOLDER,
  text,
  count,
  additionalButtons = [],
  isDefaultOpen,
  isSmall = false,
  children,
  ...styles
}) => {
  const dispatch = useDispatch();
  const [isOpen, setIsOpen] = useState(isDefaultOpen);

  const onClick = () => {
    const state = !isOpen;
    dispatch(menuActions.toggleMenuItem({ id, state }));
    setIsOpen(state);
  };

  const sizeStyles = isSmall
    ? {
        px: 4,
        py: 2,
      }
    : { px: 2, py: 5 };

  return (
    <Box {...styles} flex={1}>
      <Flex
        justifyContent="space-between"
        className={cn('with-hover', { selected: isOpen, 'is-small': isSmall })}
        color="menu.font"
      >
        <Flex alignItems="center" {...sizeStyles} onClick={onClick} flex="1">
          <Icon name={isOpen ? 'chevron-down' : 'chevron-right'} mr={1} />
          <Icon name={icon} mr={1} size="0.9rem" />
          <Text fontSize="xs">{text}</Text>
          {count && count > 0 ? (
            <Badge variant="solid" backgroundColor="menu.pill" ml={2}>
              {count}
            </Badge>
          ) : null}
        </Flex>

        {additionalButtons.length > 0 ? (
          <Flex direction="column" className="visible-with-hover">
            {additionalButtons}
          </Flex>
        ) : null}
      </Flex>
      {isOpen && children ? children : null}
    </Box>
  );
};
