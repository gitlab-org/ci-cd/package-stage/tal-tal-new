import { IconButton } from '@chakra-ui/core';
import React from 'react';

interface Props {
  borderBottom?: boolean;
  icon: any;
  onClick?: (e: React.MouseEvent) => void;
}

export const ClearButton: React.FC<Props> = ({ icon, borderBottom, onClick }) => {
  return (
    <IconButton
      aria-label="Add Project"
      variant="outline"
      icon={icon}
      size="sm"
      h="50%"
      borderTop="0"
      borderLeft="1"
      borderRight="0"
      borderBottom={borderBottom ?? true ? '1' : '0'}
      borderColor="menu.border"
      borderRadius="0"
      onClick={onClick}
    />
  );
};
