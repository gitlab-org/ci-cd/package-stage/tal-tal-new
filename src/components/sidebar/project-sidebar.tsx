import { Box, Flex, Icon, Text } from '@chakra-ui/core';
import React from 'react';
import { useSelector } from 'react-redux';
import { getMenu, isMenuOpen } from '../../store/selectors';
import { SideBarButton } from './sidebar-button';

interface Props {
  onToggle: () => void;
}

export const ProjectSidebar: React.FC<Props> = ({ onToggle }) => {
  const isOpen = useSelector(isMenuOpen);
  const menu = useSelector(getMenu);

  return (
    <Flex
      flexDirection="column"
      h="100%"
      className="no-text-selection menu"
      backgroundColor="menu.bg"
      position="relative"
      paddingBottom="40px"
      w="inherit"
      minW="inherit"
    >
      {Object.keys(menu).map((x) => {
        const item = menu[x];

        return (
          <SideBarButton
            key={item.id}
            id={item.id}
            icon={item.icon}
            text={item.label}
            count={item.count}
            isDefaultOpen={item.isOpen}
            tree={item.tree}
            parentOpen={isOpen}
          />
        );
      })}

      <Box
        borderTop="1px"
        borderColor="menu.border"
        backgroundColor="menu.bg"
        style={{ cursor: 'pointer' }}
        onClick={onToggle}
        position="fixed"
        bottom="0"
        w="inherit"
        minW="inherit"
      >
        <Flex
          justifyContent="space-between"
          alignItems="center"
          className="with-hover"
          color="menu.font"
          h="40px"
          p={2}
        >
          <Icon name={isOpen ? 'arrow-left' : 'arrow-right'} size="0.75em" mr={isOpen ? 1 : 0} />
          {isOpen && (
            <Text fontSize="xs" color="gray.500">
              v{process.env.REACT_APP_VERSION}
            </Text>
          )}
        </Flex>
      </Box>
    </Flex>
  );
};
