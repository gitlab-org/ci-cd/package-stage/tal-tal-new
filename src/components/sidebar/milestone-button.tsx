import { Flex, Text } from '@chakra-ui/core';
import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { milestoneActions } from '../../store/actions/milestones';

interface Props {
  milestone: string;
}

export const MilestoneButton: React.FC<Props> = ({ milestone }) => {
  const dispatch = useDispatch();
  const history = useHistory();

  const goToMilestone = () => {
    dispatch(milestoneActions.setSelectedMilestone(milestone));
    history.push('/milestones/detail');
  };

  return (
    <Flex
      key={milestone}
      pl={6}
      py={2}
      className="with-hover"
      alignItems="center"
      color="gray.300"
      onClick={goToMilestone}
    >
      <Text fontSize="xs" isTruncated pr={1}>
        {milestone}
      </Text>
    </Flex>
  );
};
