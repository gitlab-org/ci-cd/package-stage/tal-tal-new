import { Flex, Icon, Link, Tab, TabList, TabPanel, TabPanels, Tabs, Text } from '@chakra-ui/core';
import React from 'react';
import { useSelector } from 'react-redux';
import { fromNow, getReviewItemsCountAndUnresolved } from '../../helpers';
import { getSelectedMergeRequest } from '../../store/selectors';
import { IssueDescription } from '../issues/issue-description';
import { TabCount } from '../tab-count';
import { MergeRequestActions } from './merge-request-actions';
import { ReviewListItem } from './review-list-item';
import { TopButtons } from './top-buttons';

const tabPanelProps = {
  backgroundColor: 'background.light',
  flex: 1,
  p: 4,
};

export const MergeRequestDisplay = () => {
  const item = useSelector(getSelectedMergeRequest);

  if (!item) {
    return null;
  }

  const [reviewItems, unresolvedReviewItems] = getReviewItemsCountAndUnresolved(item);

  return (
    <Flex flex={1} direction="column">
      <Flex justifyContent="space-between" px={4}>
        <Flex direction="column">
          <Text fontSize="xl" fontWeight="bold">
            <Link href={item.web_url} isExternal>
              {item.title}
            </Link>
          </Text>
          <Text color="gray.500">
            #{item.iid} Opened {fromNow(item.created_at)} by {item.author.name}. Last updated{' '}
            {fromNow(item.updated_at)}.
          </Text>
        </Flex>

        <TopButtons item={item} />
      </Flex>

      <MergeRequestActions mergeRequest={item} px={4} my={4} issueId={0} hideOpen />

      <Flex justifyContent="space-between" flex={1}>
        <Tabs as={Flex} flex={1} flexDirection="column">
          <TabList>
            <Tab className="issue-tabs">Detail</Tab>
            {reviewItems > 0 ? (
              <Tab className="issue-tabs">
                Review
                {unresolvedReviewItems ? (
                  <TabCount count={unresolvedReviewItems} />
                ) : (
                  <Icon name="check-circle" ml={2} size="0.75em" />
                )}
              </Tab>
            ) : null}
          </TabList>

          <TabPanels as={Flex} flex={1}>
            <TabPanel {...tabPanelProps}>
              <IssueDescription item={item} />
            </TabPanel>
            {reviewItems > 0 ? (
              <TabPanel {...tabPanelProps}>
                {item.reviewItems?.map((x, index) => (
                  <ReviewListItem item={x} key={index} />
                ))}
              </TabPanel>
            ) : null}
          </TabPanels>
        </Tabs>
      </Flex>
    </Flex>
  );
};
