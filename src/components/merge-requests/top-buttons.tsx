import { Flex, IconButton, Tooltip } from '@chakra-ui/core';
import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { projectUrlFromReference } from '../../helpers';
import { MergeRequest } from '../../models';
import { pinnedActions } from '../../store/actions/pinned';
import { Icons } from '../../theme/icons';

interface Props {
  item: MergeRequest;
}

export const TopButtons: React.FC<Props> = ({ item }) => {
  const dispatch = useDispatch();
  const history = useHistory();

  const copyReference = () => {
    navigator.clipboard.writeText(item.references.full);
  };

  const goToItem = () => {
    window.open(item.web_url, '_blank');
  };

  const goToProject = () => {
    const url = projectUrlFromReference(item.references);
    window.open(url, '_blank');
  };

  const onCloseClick = () => {
    dispatch(pinnedActions.setSelectedItem(null));
    history.push('/merge-requests');
  };

  return (
    <Flex>
      <Tooltip aria-label="Copy reference" label="Copy reference" placement="bottom">
        <IconButton
          aria-label="Copy reference"
          icon="copy"
          variant="ghost"
          size="sm"
          onClick={copyReference}
        />
      </Tooltip>

      <IconButton
        aria-label="View on GitLab"
        icon={Icons.EXTERNAL_LINK}
        variant="ghost"
        size="sm"
        onClick={goToItem}
      />
      <IconButton
        aria-label="Go to Project"
        icon={Icons.HOME}
        variant="ghost"
        size="sm"
        onClick={goToProject}
      />
      <IconButton
        aria-label="Close"
        icon="small-close"
        variant="ghost"
        size="sm"
        onClick={onCloseClick}
      />
    </Flex>
  );
};
