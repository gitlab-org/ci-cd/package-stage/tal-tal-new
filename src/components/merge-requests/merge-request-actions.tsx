import { Badge, Flex, FlexProps, Tag, TagIcon, TagLabel } from '@chakra-ui/core';
import React, { useContext } from 'react';
import { AppContextComponent } from '../../app-context';
import { capitalize, getMergeRequestStatus } from '../../helpers';
import { MergeRequest } from '../../models';
import { Icons } from '../../theme/icons';
import { WorkflowButton } from '../workflow-button';

interface Props extends FlexProps {
  mergeRequest: MergeRequest;
  hideOpen?: boolean;
  hideMilestone?: boolean;
  hideWorkflow?: boolean;
  issueId: number;
}

const getGroup = (labels: string[]) => {
  const prefix = 'group::';
  const groups = labels.filter((x) => x.includes(prefix));

  if (groups && groups.length) {
    return groups.map((x) => capitalize(x.replace(prefix, '')));
  }

  return [];
};

export const MergeRequestActions: React.FC<Props> = ({
  mergeRequest,
  hideOpen,
  hideMilestone,
  hideWorkflow = false,
  issueId,
  ...props
}) => {
  const { dispatch: contextDispatch } = useContext(AppContextComponent);
  const [status, variantColor] = getMergeRequestStatus(mergeRequest);
  const groups = getGroup(mergeRequest.labels);

  const changeWorkflow = (e: React.MouseEvent) => {
    e.stopPropagation();

    contextDispatch({
      type: 'toggle-workflow-modal',
      payload: { isOpen: true, mergeRequest, issueId },
    });
  };

  return (
    <Flex {...props}>
      {!hideOpen && (
        <Badge variantColor={variantColor} variant="solid" className="badge" mr="2">
          {status}
        </Badge>
      )}

      {groups.length
        ? groups.map((x, index) => (
            <Badge variantColor="pink" variant="subtle" className="badge" mr="2" key={index}>
              {x}
            </Badge>
          ))
        : null}

      {mergeRequest.milestone && hideMilestone !== true && (
        <Tag size="sm" variantColor="blue" rounded="full" mr="2">
          <TagIcon icon={Icons.CLOCK} size="12px" />
          <TagLabel>{mergeRequest.milestone.title}</TagLabel>
        </Tag>
      )}

      {hideWorkflow ? null : (
        <WorkflowButton workflow={mergeRequest.workflow} onClick={changeWorkflow} />
      )}
    </Flex>
  );
};
