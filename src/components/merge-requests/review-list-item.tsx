import { Avatar, Box, Flex, IconButton, Text } from '@chakra-ui/core';
import React from 'react';
import { useMarkdownToHtml } from '../../hooks';
import { MergeRequestReviewItem } from '../../models';
import { Icons } from '../../theme/icons';

interface Props {
  item: MergeRequestReviewItem;
}

export const ReviewListItem: React.FC<Props> = ({ item }) => {
  const html = useMarkdownToHtml(item.parseResult.subject);

  const goToNote = () => {
    window.open(item.webUrl, '_blank');
  };

  return (
    <Box
      p={5}
      borderWidth="1px"
      borderRadius={4}
      borderColor="gray.400"
      mb={2}
      backgroundColor={item.resolved ? 'green.50' : 'white'}
    >
      <Flex alignItems="center">
        <Avatar name={item.author.name} src={item.author.avatar_url} size="sm" mr={2} />

        <Flex direction="column">
          <Text fontWeight="bold" className="title">
            {item.parseResult.label}
          </Text>
          <Box dangerouslySetInnerHTML={{ __html: html }} />
        </Flex>

        <Flex ml="auto">
          <IconButton
            aria-label="View on GitLab"
            icon={Icons.EXTERNAL_LINK}
            variant="ghost"
            variantColor={item.resolved ? 'green' : 'gray'}
            size="sm"
            onClick={goToNote}
          />
        </Flex>
      </Flex>
    </Box>
  );
};
