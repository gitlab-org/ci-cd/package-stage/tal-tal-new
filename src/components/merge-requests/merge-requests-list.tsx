import { Flex, Tab, TabList, TabPanel, TabPanels, Tabs, Text } from '@chakra-ui/core';
import React from 'react';
import { capitalize } from '../../helpers';
import { MergeRequest, ProjectMergeRequestsMap } from '../../models';
import { TabCount } from '../tab-count';
import { MergeRequestListItem } from './merge-request-list-item';

type Tab = {
  label: string;
  count: number;
  mergeRequests: MergeRequest[];
};

const getTabs = (store: ProjectMergeRequestsMap): Tab[] => {
  return Object.keys(store)
    .sort()
    .map((x) => ({
      label: x,
      count: store[x].length || 0,
      mergeRequests: store[x] || [],
    }));
};

interface Props {
  mergeRequestsStore: ProjectMergeRequestsMap;
}

export const MergeRequestsList: React.FC<Props> = ({ mergeRequestsStore }) => {
  const tabs: Tab[] = getTabs(mergeRequestsStore);

  return (
    <Flex flex={1}>
      <Flex flex={1} direction="column">
        <Flex direction="column" mb={4} px={4}>
          <Text fontSize="xl" fontWeight="bold">
            Merge Requests
          </Text>
        </Flex>

        <Tabs as={Flex} flex={1} flexDirection="column">
          <TabList>
            {tabs.map((x, index) => (
              <Tab className="issue-tabs" key={index}>
                {capitalize(x.label)}
                <TabCount count={x.count} />
              </Tab>
            ))}
          </TabList>

          <TabPanels as={Flex} flex={1} h="100%">
            {tabs.map((x, index) => (
              <TabPanel p={4} backgroundColor="background.light" flex={1} key={index}>
                {x.mergeRequests
                  .sort((a, b) => (a.updated_at > b.updated_at ? -1 : 1))
                  .map((i: MergeRequest) => (
                    <MergeRequestListItem mergeRequestId={i.id} key={i.id} />
                  ))}
              </TabPanel>
            ))}
          </TabPanels>
        </Tabs>
      </Flex>
    </Flex>
  );
};
