import { Avatar, AvatarGroup, Box, Flex, IconButton, Text, Tooltip } from '@chakra-ui/core';
import cn from 'classnames';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { fromNow } from '../../helpers';
import { MergeRequest } from '../../models';
import { fetchMergeRequestComments, mergeRequestActions } from '../../store/actions/merge-requests';
import { AppState } from '../../store/reducers';
import { getMergeRequestById } from '../../store/selectors';
import { Icons } from '../../theme/icons';
import { MergeRequestActions } from './merge-request-actions';

interface Props {
  mergeRequestId: number;
}

export const MergeRequestListItem: React.FC<Props> = ({ mergeRequestId }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const mergeRequest = useSelector<AppState, MergeRequest | undefined>((s) =>
    getMergeRequestById(s, mergeRequestId),
  );

  if (!mergeRequest) {
    return null;
  }

  const onMergeRequestClick = () => {
    dispatch(fetchMergeRequestComments(mergeRequest));
    dispatch(mergeRequestActions.setSelectedMergeRequest(mergeRequest.id));
    history.push('merge-requests/detail');
  };

  const copyReference = (e: React.MouseEvent) => {
    e.stopPropagation();
    navigator.clipboard.writeText(mergeRequest.references.full);
  };

  const goToMergeRequest = (e: React.MouseEvent) => {
    e.stopPropagation();
    window.open(mergeRequest.web_url, '_blank');
  };

  return (
    <Box
      className={cn('issue with-hover')}
      p={5}
      borderWidth="1px"
      borderRadius={4}
      borderColor="gray.400"
      mb={2}
      backgroundColor="white"
      onClick={onMergeRequestClick}
    >
      <Flex justifyContent="space-between">
        <Flex mr={2}>
          <AvatarGroup size="sm" max={3}>
            {mergeRequest.assignees.map((x) => (
              <Avatar name={x.name} src={x.avatar_url} key={x.id} />
            ))}
          </AvatarGroup>
        </Flex>

        <Flex direction="column" flex={1}>
          <Text fontWeight="bold" fontSize="md">
            {mergeRequest.title}
          </Text>
          <Text color="gray.500" fontSize="sm">
            !{mergeRequest.iid} - Opened {fromNow(mergeRequest.created_at)} by{' '}
            <strong>{mergeRequest.author.name}</strong>. Last updated{' '}
            {fromNow(mergeRequest.updated_at)}.
          </Text>
        </Flex>

        <Flex>
          <Tooltip aria-label="Copy reference" label="Copy reference" placement="bottom">
            <IconButton
              aria-label="Copy reference"
              icon="copy"
              variant="ghost"
              size="sm"
              onClick={copyReference}
            />
          </Tooltip>

          <IconButton
            aria-label="View on GitLab"
            icon={Icons.EXTERNAL_LINK}
            variant="ghost"
            size="sm"
            onClick={goToMergeRequest}
          />
        </Flex>
      </Flex>

      <MergeRequestActions mergeRequest={mergeRequest} mt={2} issueId={0} hideOpen />
    </Box>
  );
};
