import { Box, Button, Divider, Flex, Text } from '@chakra-ui/core';
import React, { useState } from 'react';
import { TrackedMilestone } from '../../models';
import { MilestoneNote } from './milestone-note';

interface Props {
  milestone: TrackedMilestone;
}

export const MilestoneNotes: React.FC<Props> = ({ milestone }) => {
  const [addRetroNote, setAddRetroNote] = useState(false);

  const onAddRetroNoteClick = () => {
    setAddRetroNote(true);
  };

  return (
    <Flex direction="column">
      <Box
        p={4}
        backgroundColor="white"
        borderWidth="1px"
        borderRadius={4}
        borderColor="gray.400"
        w="100%"
      >
        <Text fontSize="lg" fontWeight="bold" mb={4}>
          Milestone Retro Notes
        </Text>

        <Text mb={4}>
          Use this space over the course of a milestone to record any notes that you may want to add
          to the final retro issue created.
        </Text>

        {milestone.retroNotes.length > 0
          ? milestone.retroNotes.map((x, index) => {
              return (
                <Box key={x} w="100%">
                  <MilestoneNote
                    milestoneId={milestone.id}
                    content={x}
                    type="retro"
                    noteIndex={index}
                  />
                  {index !== milestone.retroNotes.length - 1 ? <Divider /> : null}
                </Box>
              );
            })
          : null}

        <Divider />

        {addRetroNote && (
          <MilestoneNote
            milestoneId={milestone.id}
            isDefaultEditing={true}
            onCancelClick={() => setAddRetroNote(false)}
            type="retro"
          />
        )}

        <Button
          size="sm"
          variantColor="green"
          mt={2}
          isDisabled={addRetroNote}
          onClick={onAddRetroNoteClick}
        >
          Add Note
        </Button>
      </Box>

      {/* <Box
        p={4}
        backgroundColor="white"
        borderWidth="1px"
        borderRadius={4}
        borderColor="gray.400"
        w="100%"
        mt={2}
      >
        <Text fontSize="lg" fontWeight="bold" mb={4}>
          Weekly Retro Notes
        </Text>

        <Text>Consider adding any weekly retro notes for this milestone.</Text>

        <Divider />

        <Button size="sm" variantColor="green" mt={2}>
          Add Note
        </Button>
      </Box> */}
    </Flex>
  );
};
