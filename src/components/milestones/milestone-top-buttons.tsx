import { Flex, IconButton } from '@chakra-ui/core';
import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { TrackedMilestone } from '../../models';
import { menuActions } from '../../store/actions/menu';
import { milestoneActions } from '../../store/actions/milestones';

interface Props {
  milestone: TrackedMilestone;
}

export const MilestoneTopButtons: React.FC<Props> = ({ milestone }) => {
  const dispatch = useDispatch();
  const history = useHistory();

  const onCloseClick = () => {
    dispatch(milestoneActions.setSelectedMilestone(null));
    history.push('/milestones');
  };

  const onDeleteMilestoneClick = () => {
    dispatch(menuActions.removeMilestoneMenuItem(milestone.id));
    dispatch(milestoneActions.removeMilestone(milestone.id));
    history.push('/milestones');
  };

  return (
    <Flex>
      <IconButton
        aria-label="Close"
        icon="small-close"
        variant="ghost"
        size="sm"
        onClick={onCloseClick}
      />
      <IconButton
        aria-label="Delete"
        icon="delete"
        variant="ghost"
        variantColor="red"
        size="sm"
        onClick={onDeleteMilestoneClick}
      />
    </Flex>
  );
};
