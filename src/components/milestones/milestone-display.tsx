import { Flex, Tab, TabList, TabPanel, TabPanels, Tabs, Text } from '@chakra-ui/core';
import React, { useState } from 'react';
import { TrackedMilestone } from '../../models';
import { TabCount } from '../tab-count';
import { MilestoneDetail } from './milestone-detail';
import { MilestoneNotes } from './milestone-notes';
import { MilestoneTopButtons } from './milestone-top-buttons';
import { TimeDisplay } from './time-display';

const tabPanelProps = {
  backgroundColor: 'background.light',
  flex: 1,
  p: 4,
};

interface Props {
  milestone: TrackedMilestone;
}

export const MilestoneDisplay: React.FC<Props> = ({ milestone }) => {
  const [tabIndex, setTabIndex] = useState(0);

  const handleTabsChange = (index: number) => setTabIndex(index);

  return (
    <Flex flex={1} direction="column">
      <Flex justifyContent="space-between" px={4}>
        <Flex direction="column">
          <Text fontSize="xl" fontWeight="bold">
            Milestone: {milestone.id}
          </Text>
          <TimeDisplay milestone={milestone} />
        </Flex>

        <MilestoneTopButtons milestone={milestone} />
      </Flex>

      <Flex justifyContent="space-between" flex={1} mt={4}>
        <Tabs
          as={Flex}
          flex={1}
          flexDirection="column"
          index={tabIndex}
          onChange={handleTabsChange}
        >
          <TabList>
            <Tab className="issue-tabs">Detail</Tab>
            <Tab>
              Notes
              <TabCount count={0} />
            </Tab>
          </TabList>

          <TabPanels as={Flex} flex={1}>
            <TabPanel {...tabPanelProps}>
              <MilestoneDetail milestone={milestone} />
            </TabPanel>
            <TabPanel {...tabPanelProps}>
              <MilestoneNotes milestone={milestone} />
            </TabPanel>
          </TabPanels>
        </Tabs>
      </Flex>
    </Flex>
  );
};
