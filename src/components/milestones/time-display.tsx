import { Flex, Skeleton, Text } from '@chakra-ui/core';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { TrackedMilestone } from '../../models';
import { getMilestonesLoading } from '../../store/selectors';

interface Props {
  milestone: TrackedMilestone;
}

export const TimeDisplay: React.FC<Props> = ({ milestone }) => {
  const isLoading = useSelector(getMilestonesLoading);
  const isLoaded = Boolean(milestone.startDate || isLoading);

  const [finished, setFinished] = useState(false);
  const [endDate, setEndDate] = useState('');

  useEffect(() => {
    const diff = moment(milestone.endDate).diff(moment(), 'days');

    if (diff < 0) {
      setEndDate(`Finished ${Math.abs(diff)} days ago`);
      setFinished(true);
    } else if (diff === 0) {
      setEndDate(`Finishes today`);
      setFinished(true);
    } else {
      setEndDate(`Finishes in ${diff} days`);
      setFinished(false);
    }
  }, [isLoading, milestone.endDate]);

  return (
    <Skeleton isLoaded={isLoaded}>
      <Flex>
        <Text color="gray.500">Started on {moment(milestone.startDate).format('Do MMM')}.</Text>
        &nbsp;
        <Text color="gray.500" fontWeight={finished ? 'normal' : 'bold'}>
          {endDate}.
        </Text>
      </Flex>
    </Skeleton>
  );
};
