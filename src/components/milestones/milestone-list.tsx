import { Box, Flex, Text } from '@chakra-ui/core';
import cn from 'classnames';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { milestoneActions } from '../../store/actions/milestones';
import { getTrackedMilestones } from '../../store/selectors';

export const MilestoneList: React.FC = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const milestones = useSelector(getTrackedMilestones);

  const onMilestoneClick = (milestone: string) => {
    dispatch(milestoneActions.setSelectedMilestone(milestone));
    history.push('/milestones/detail');
  };

  return (
    <Flex flex={1}>
      <Flex flex={1} direction="column">
        <Flex direction="column" mb={4} px={4}>
          <Text fontSize="xl" fontWeight="bold">
            Milestones
          </Text>
        </Flex>

        <Box h="100%" backgroundColor="background.light" p={4}>
          {milestones.map((x) => (
            <Box
              className={cn('issue with-hover')}
              p={5}
              borderWidth="1px"
              borderRadius={4}
              borderColor="gray.400"
              mb={2}
              backgroundColor="white"
              key={x}
              onClick={() => onMilestoneClick(x)}
            >
              <Flex justifyContent="space-between">
                <Flex direction="column">
                  <Text fontWeight="bold" fontSize="md">
                    {x}
                  </Text>
                </Flex>
              </Flex>
            </Box>
          ))}
        </Box>
      </Flex>
    </Flex>
  );
};
