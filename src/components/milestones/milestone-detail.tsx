import { Box, StatGroup, Text } from '@chakra-ui/core';
import React from 'react';
import { useSelector } from 'react-redux';
import { sortByDescending } from '../../helpers';
import { TrackedMilestone } from '../../models';
import {
  getMergedMRsForMilestone,
  getMilestonesLoading,
  getPreviousMilestone,
} from '../../store/selectors';
import { MergedMR } from './merged-mr';
import { MilestoneStat } from './milestone-stat';

interface Props {
  milestone: TrackedMilestone;
}

export const MilestoneDetail: React.FC<Props> = ({ milestone }) => {
  const isLoading = useSelector(getMilestonesLoading);
  const prevMilestone = useSelector(getPreviousMilestone);
  const mergedMRs = useSelector(getMergedMRsForMilestone);

  return (
    <Box>
      <Text mb={2} fontWeight="bold">
        Merge requests
      </Text>

      <StatGroup>
        <MilestoneStat
          title="Merged"
          figure={milestone.merged}
          comparisonFigure={prevMilestone?.merged}
          help="So far this milestone"
          isLoading={isLoading}
          ml={0}
        />
        <MilestoneStat
          title="Open"
          figure={milestone.open}
          help="currently being worked on"
          isLoading={isLoading}
        />
        {/* <MilestoneStat
          title="Avg per week"
          figure={milestone.avgPerWeek}
          comparisonFigure={prevMilestone?.avgPerWeek}
          help="Over this milestone"
          isLoading={isLoading}
          mr={0}
        /> */}
      </StatGroup>

      <Text mt={4} mb={2} fontWeight="bold">
        Merged this milestone
      </Text>

      {mergedMRs && mergedMRs.length ? (
        mergedMRs
          .sort((a, b) => sortByDescending(a, b, 'merged_at'))
          .map((x) => <MergedMR mergeRequest={x} key={x.id} />)
      ) : (
        <Text>No merged merge requests found.</Text>
      )}
    </Box>
  );
};
