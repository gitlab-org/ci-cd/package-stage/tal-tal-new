import {
  BoxProps,
  Skeleton,
  Stat,
  StatArrow,
  StatHelpText,
  StatLabel,
  StatNumber,
} from '@chakra-ui/core';
import React from 'react';

interface Props extends BoxProps {
  title: string;
  figure: number;
  comparisonFigure?: number;
  help?: string;
  isLoading?: boolean;
}

const statProps = {
  p: 4,
  mx: 2,
  backgroundColor: 'white',
  borderWidth: '1px',
  borderRadius: 4,
  borderColor: 'gray.400',
};

export const MilestoneStat: React.FC<Props> = ({
  title,
  figure,
  comparisonFigure = 0,
  help,
  isLoading,
  ...props
}) => {
  const type =
    comparisonFigure < figure ? 'increase' : comparisonFigure === figure ? null : 'decrease';

  return (
    <Stat {...statProps} {...props} minH="120px">
      {isLoading ? (
        <>
          <Skeleton height="16px" mb="8px" w="100px"></Skeleton>
          <Skeleton height="31px" mb="8px" w="50px"></Skeleton>
          <Skeleton height="16px" mb="7px"></Skeleton>
        </>
      ) : (
        <>
          <StatLabel>{title}</StatLabel>
          <StatNumber>{figure}</StatNumber>
          <StatHelpText>
            {comparisonFigure !== 0 && type ? (
              <>
                <StatArrow type={type} />
                compared to {comparisonFigure} previously
              </>
            ) : (
              help
            )}
          </StatHelpText>
        </>
      )}
    </Stat>
  );
};
