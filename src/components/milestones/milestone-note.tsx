import { Button, ButtonGroup, Flex, IconButton, Text, Textarea } from '@chakra-ui/core';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { milestoneActions } from '../../store/actions/milestones';

interface Props {
  milestoneId: string;
  isDefaultEditing?: boolean;
  onCancelClick?: () => void;
  content?: string;
  type: 'retro' | 'weekly';
  noteIndex?: number;
}

export const MilestoneNote: React.FC<Props> = ({
  milestoneId,
  isDefaultEditing = false,
  onCancelClick,
  content = '',
  type,
  noteIndex,
}) => {
  const dispatch = useDispatch();

  const [isEditing, setIsEditing] = useState(isDefaultEditing);
  const [saveEnabled, setSaveEnabled] = useState(false);
  const [noteContent, setNoteContent] = useState(content);

  const onEditClick = () => {
    setIsEditing(!isEditing);
  };

  const onInput = (e: React.FormEvent<any>) => {
    setNoteContent(e.currentTarget.value);
  };

  const onSaveClick = () => {
    dispatch(
      milestoneActions.addOrEditNoteToMilestone({
        milestoneId,
        content: noteContent,
        noteType: type,
        noteIndex,
      }),
    );

    setIsEditing(false);
  };

  const onDeleteClick = () => {
    dispatch(
      milestoneActions.removeNoteFromMilestone({
        milestoneId,
        content: '',
        noteType: type,
        noteIndex,
      }),
    );
  };

  useEffect(() => {
    if (!isEditing && onCancelClick) {
      onCancelClick();
    }
  }, [isEditing, onCancelClick]);

  useEffect(() => {
    if (noteContent) {
      setSaveEnabled(true);
    } else {
      setSaveEnabled(false);
    }
  }, [noteContent]);

  return (
    <Flex px={4} py={2} direction={isEditing ? 'column' : 'row'} justifyContent="space-between">
      {isEditing ? (
        <>
          <Textarea defaultValue={noteContent} onInput={onInput} />
          <ButtonGroup size="xs" mt={2}>
            <Button variantColor="green" isDisabled={!saveEnabled} onClick={onSaveClick}>
              Save
            </Button>
            <Button onClick={onEditClick}>Cancel</Button>
          </ButtonGroup>
        </>
      ) : (
        <Text fontSize="sm" mr={4}>
          {noteContent}
        </Text>
      )}

      {!isEditing && (
        <ButtonGroup size="xs">
          <IconButton
            aria-label="Edit note"
            icon="edit"
            variant="outline"
            variantColor="blue"
            onClick={onEditClick}
          >
            Edit
          </IconButton>
          <IconButton
            aria-label="Remove note"
            icon="delete"
            variant="outline"
            variantColor="red"
            onClick={onDeleteClick}
          >
            Remove
          </IconButton>
        </ButtonGroup>
      )}
    </Flex>
  );
};
