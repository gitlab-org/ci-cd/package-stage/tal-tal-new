import { Box, Flex, IconButton, Link, Text, Tooltip } from '@chakra-ui/core';
import moment from 'moment';
import React from 'react';
import { fromNow } from '../../helpers';
import { MergeRequest } from '../../models';
import { Icons } from '../../theme/icons';
import { MergeRequestActions } from '../merge-requests/merge-request-actions';

interface Props {
  mergeRequest: MergeRequest;
}

export const MergedMR: React.FC<Props> = ({ mergeRequest }) => {
  const copyReference = (e: React.MouseEvent) => {
    e.stopPropagation();
    navigator.clipboard.writeText(mergeRequest.references.full);
  };

  const goToMergeRequest = (e: React.MouseEvent) => {
    e.stopPropagation();
    window.open(mergeRequest.web_url, '_blank');
  };

  return (
    <Box
      p={5}
      borderWidth="1px"
      borderRadius={4}
      borderColor="gray.400"
      mb={2}
      backgroundColor="white"
    >
      <Flex justifyContent="space-between">
        <Flex direction="column">
          <Text fontWeight="bold" fontSize="md">
            <Link href={mergeRequest.web_url} isExternal>
              {mergeRequest.title}
            </Link>
          </Text>
          <Text color="gray.500" fontSize="sm">
            !{mergeRequest.iid} - Opened {fromNow(mergeRequest.created_at)} by{' '}
            <strong>{mergeRequest.author.name}</strong>, merged on{' '}
            {moment(mergeRequest.merged_at as string).format('Do MMM YYYY')}. Last updated{' '}
            {fromNow(mergeRequest.updated_at)}.
          </Text>
        </Flex>

        <Flex>
          <Tooltip aria-label="Copy reference" label="Copy reference" placement="bottom">
            <IconButton
              aria-label="Copy reference"
              icon="copy"
              variant="ghost"
              size="sm"
              onClick={copyReference}
            />
          </Tooltip>

          <IconButton
            aria-label="View on GitLab"
            icon={Icons.EXTERNAL_LINK}
            variant="ghost"
            size="sm"
            onClick={goToMergeRequest}
          />
        </Flex>
      </Flex>

      <MergeRequestActions
        mergeRequest={mergeRequest}
        mt={2}
        issueId={0}
        hideMilestone
        hideWorkflow
      />
    </Box>
  );
};
