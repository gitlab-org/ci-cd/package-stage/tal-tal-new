import { Badge } from '@chakra-ui/core';
import React from 'react';

interface Props {
  count?: number;
}

export const TabCount: React.FC<Props> = ({ count = 0 }) => {
  if (!count) {
    return null;
  }

  return (
    <Badge ml={2} variantColor="blue">
      {count}
    </Badge>
  );
};
