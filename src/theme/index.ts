import { theme } from '@chakra-ui/core';
import { colors } from './colors';
import { customIcons } from './icons';

export const makoTheme = {
  ...theme,
  colors: {
    ...theme.colors,
    ...colors,
    border: {
      200: '#E2E8F0',
    },
    brand: {
      900: '#1a365d',
      800: '#153e75',
      700: '#2a69ac',
    },
  },
  icons: {
    ...theme.icons,
    ...customIcons,
  },
};
