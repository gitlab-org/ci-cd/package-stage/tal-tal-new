export const colors = {
  gray: {
    50: '#fafafa',
    100: '#f5f5f5',
    200: '#eeeeee',
    300: '#e0e0e0',
    400: '#bdbdbd',
    500: '#9e9e9e',
    600: '#757575',
    700: '#616161',
    800: '#424242',
    900: '#212121',
  },

  // menu: {
  //   bg: '#364860',
  //   font: '#e4eefa',
  //   border: '#2a3851',
  //   pill: '#15a5fb',
  // },

  menu: {
    bg: '#21222c',
    font: 'white',
    border: '#191a21',
    pill: '#15a5fb',
    updateCompleted: '#50fa7b',
    updatedRequired: '#ff5555',
  },

  background: {
    light: '#eff0eb',
  },
};
