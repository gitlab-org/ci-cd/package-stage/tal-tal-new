import { MergeRequest } from './merge-request';

export interface Milestone {
  id: number;
  iid: number;
  group_id: number;
  title: string;
  description: string;
  state: 'active';
  created_at: string;
  updated_at: string;
  due_date?: string;
  start_date?: string;
  web_url: string;
}

export interface TrackMilestoneMap {
  [key: string]: TrackedMilestone;
}

export interface TrackedMilestone {
  id: string;
  mergeRequests: MergeRequest[];
  startDate: string;
  endDate: string;
  open: number;
  merged: number;
  avgPerWeek: number;
  retroNotes: string[];
  weeklyNotes: string[];
}
