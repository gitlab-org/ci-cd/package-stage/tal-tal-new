import { Issue } from './issue';

export interface SearchDoc {
  entityId: number;
  title: string | string[];
  type?: string;
  content?: string | string[];
}

export interface SearchResult {
  score: number;
  entity: Issue;
  type: string;
}
