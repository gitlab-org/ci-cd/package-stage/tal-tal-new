export interface GitLabUser {
  id: number;
  name: string;
  username: string;
  state: 'active' | 'inactive';
  avatar_url: string;
  web_url: string;
}
