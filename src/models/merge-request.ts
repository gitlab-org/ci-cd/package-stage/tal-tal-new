import { ParseResult } from 'cc-parse/dist/models';
import { Workflow } from './globals';
import { Reference } from './issue';
import { Milestone } from './milestone';
import { GitLabUser } from './user';

export interface MergeRequestMap {
  [key: string]: ProjectMergeRequestsMap;
}

export interface ProjectMergeRequestsMap {
  [key: string]: MergeRequest[];
}

export interface MergeRequest {
  id: number;
  iid: number;
  project_id: number;
  title: string;
  description: string;
  state: string;
  created_at: string;
  updated_at: string;
  closed_by: string | null;
  closed_at: string | null;
  target_branch: string;
  source_branch: string;
  user_notes_count: number;
  upvotes: number;
  downvotes: number;
  author: GitLabUser;
  assignee: GitLabUser;
  assignees: GitLabUser[];
  source_project_id: number;
  target_project_id: number;
  labels: string[];
  work_in_progress: boolean;
  milestone: Milestone;
  merge_when_pipeline_succeeds: boolean;
  merge_status: string;
  sha: string;
  merged_at: string | null;
  merge_commit_sha: string | null;
  squash_commit_sha: string | null;
  discussion_locked: boolean | null;
  should_remove_source_branch: boolean | null;
  force_remove_source_branch: boolean;
  reference: string;
  web_url: string;
  references: Reference;
  time_stats: {
    time_estimate: number;
    total_time_spent: number;
    human_time_estimate: string | null;
    human_total_time_spent: string | null;
  };
  squash: boolean;
  task_completion_status: {
    count: number;
    completed_count: number;
  };
  changes_count: string;
  latest_build_started_at: string;
  latest_build_finished_at: string;
  first_deployed_to_production_at: string | null;
  pipeline: {
    id: number;
    sha: string;
    ref: string;
    status: string;
    web_url: string;
  };
  head_pipeline: {
    id: number;
    sha: string;
    ref: string;
    status: string;
    web_url: string;
    before_sha: string;
    tag: boolean;
    yaml_errors: string | null;
    user: GitLabUser;
    created_at: string;
    updated_at: string;
    started_at: string;
    finished_at: string;
    committed_at: string | null;
    duration: number;
    coverage: string;
    detailed_status: {
      icon: string;
      text: string;
      label: string;
      group: string;
      tooltip: string;
      has_details: boolean;
      details_path: string;
      illustration: string | null;
      favicon: string;
    };
  };
  diff_refs: {
    base_sha: string;
    head_sha: string;
    start_sha: string;
  };
  merge_error: string | null;
  user: {
    can_merge: boolean;
  };

  // Custom
  workflow: Workflow;
  type: string;
  reviewItems: MergeRequestReviewItem[];
}

export interface MergeRequestNote {
  attachment: null;
  author: GitLabUser;
  body: string;
  commands_changes: {};
  confidential: boolean | null;
  created_at: string;
  id: number;
  noteable_id: number;
  noteable_iid: number;
  noteable_type: 'MergeRequest';
  position: {
    base_sha: string;
    head_sha: string;
    line_range: null;
    new_line: 2;
    new_path: string;
    old_line: null;
    old_path: string;
    position_type: 'text';
    start_sha: string;
  };
  resolved: boolean;
  resolvable: boolean;
  system: boolean;
  type: 'DiffNote' | 'DiscussionNote' | null;
  updated_at: string;
}

export interface MergeRequestReviewItem {
  author: GitLabUser;
  resolved: boolean;
  parseResult: ParseResult;
  webUrl: string;
}
