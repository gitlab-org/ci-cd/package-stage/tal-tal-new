import { IssueProject } from './issue';

export interface ProjectMenus {
  [key: number]: Menu;
  global: Menu;
}

export interface Menu {
  [key: string]: MenuItem;
}

export interface MenuItem {
  id: string;
  label: string;
  icon?: any;
  isOpen: boolean;
  count: number | undefined;
  tree?: Menu;
  type: 'folder' | 'item';
  itemId?: number;
}

export interface MilestonesProjectMenu {
  [projectId: number]: MilestonesMenu;
}

export interface MilestonesMenu {
  [milestone: string]: MilestoneMenuItem;
}

export interface MilestoneMenuItem {
  issues: number[];
  count?: number;
  startDate?: string;
  dueDate?: string;
}

export interface ProjectsStore {
  [key: number]: IssueProject;
}
