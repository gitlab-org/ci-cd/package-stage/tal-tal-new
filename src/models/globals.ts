export enum Scopes {
  WORKFLOW = 'workflow::',
}

export const workflowPriority = [
  'production',
  'staging',
  'blocked',
  'in review',
  'in dev',
  'ready for development',
  'scheduling',
];

export enum Workflow {
  IN_DEV = 'in dev',
  IN_REVIEW = 'in review',
  BLOCKED = 'blocked',
  CANARY = 'canary',
  DESIGN = 'design',
  ISSUE_REVIEWED = 'issue reviewed',
  NEEDS_ISSUE_REVIEW = 'needs issue review',
  PLANNING_BREAKDOWN = 'planning breakdown',
  PROBLEM_VALIDATION = 'problem validation',
  PRODUCTION = 'production',
  READY_FOR_DEVELOPMENT = 'ready for development',
  READY_FOR_REVIEW = 'ready for review',
  SCHEDULING = 'scheduling',
  SOLUTION_VALIDATION = 'solution validation',
  STAGING = 'staging',
  START = 'start',
  VALIDATION_BACKLOG = 'validation backlog',
  VERIFICATION = 'verification',
  NONE = 'no workflow',
}
