import { Issue } from './issue';

export interface PinnedItemStore {
  [id: number]: Issue;
}

export interface PinnedItem extends Issue {
  categoryId?: string;
}

export interface PinnedMenu {
  [category: string]: PinnedMenuItem;
}

export interface PinnedMenuItem {
  issues: number[];
  count?: number;
}

export enum PinnedItemType {
  EPIC = 'epic',
  ISSUE = 'issue',
}
