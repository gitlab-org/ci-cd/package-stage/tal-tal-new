# Tal Tal

A new and improved (maybe) version.

[![Pipeline status](https://gitlab.com/nkipling/tal-tal-new/badges/master/pipeline.svg)](https://gitlab.com/nkipling/tal-tal-new/-/commits/master)

[![Netlify Status](https://api.netlify.com/api/v1/badges/dc272a56-3ebc-4b20-85e1-a2d9a77c2677/deploy-status)](https://app.netlify.com/sites/taltal-new/deploys)

Available at: https://taltal-new.netlify.com/

## About

Tal Tal is a small frontend app that helps manage your GitLab issues and keeps track of
your **async updates**. This version has new features like:

* Issue importing - no longer add/remove issues manually
* Better workflow management
* Improved issue display with more information, including merge requests
* Verification / production reminders
* A handy little search
* ...and more to come

## Screenshots

|Issues List|Adding Update|Manage Workflow|
|-----------|-------------|---------------|
|![](images/issues-list.png)|![](images/add-update.png)|![](images/update-workflow.png)|

## Want to help?

Sure. Start by cloning the repo and running `yarn install`. I suggest you also
create a `.env.development.local` file at the root directory with the following
content:

```
PORT=9990
client_id={your-oauth-client-id}
redirect_uri=http://localhost:9990/callback
```

You can grab your own OAuth client id by [following these instructions](https://docs.gitlab.com/ee/integration/oauth_provider.html#introduction-to-oauth).

Local development can then be started by running `yarn start`.